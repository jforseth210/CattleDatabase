"""
This module can be used to automatically import cattle records from a csv file.
"""
import arrow
from main import create_app, db, ProdConfig
from models.cow import Cow, get_cow_from_tag
from models.event import Event

IMPORT_FILE = ""
OWNER = ""
app = create_app(ProdConfig())
with open(IMPORT_FILE, "r", encoding="UTF-8") as file:
    cows = file.readlines()
splitCows = []
for cow in cows:
    splitCows.append(cow.replace("\n", "").split(","))

with app.app_context():
    for record in splitCows:

        cow = get_cow_from_tag(record[0])

        if cow is None:
            cow = Cow(sex="Cow", tag_number=record[0], owner=OWNER)
        db.session.add(cow)
        db.session.commit()

        # Parse sexes
        if record[4].lower() in ["s", "steer"]:
            SEX = "Steer"
        elif record[4].lower() in ["b", "bull"]:
            SEX = "Bull"
        elif record[4].lower() in ["h", "heifer"]:
            SEX = "Heifer"
        elif record[4].lower() in ["fm", "free martin", "free-martin"]:
            SEX = "Free-Martin"
        else:
            SEX = "Heifer"
        calf = Cow(
            dam_id=cow.cow_id,
            sex=SEX,
            tag_number=record[1],
            owner=OWNER,
            description=record[5],
        )
        try:
            date = arrow.get(record[2], "D-MMM-YY").format("YYYY-MM-DD")
            # date = arrow.get(record[2] + "/2012", "M/D/YYYY").format("YYYY-MM-DD")
        except arrow.parser.ParserMatchError:
            print(f"No date for {record[0]} -> {record[1]}")
        if date:
            born_event = Event(
                date=date,
                name="Born",
                description=f"{record[0]} gave birth to {record[1]}\n\n" f"{record[5]}",
                cows=[calf],
            )
            calved_event = Event(
                date=date,
                name="Calved",
                description=f"{record[0]} gave birth to {record[1]}\n\n" f"{record[5]}",
                cows=[cow],
            )
            db.session.add(born_event)
            db.session.add(calved_event)
        db.session.add(calf)
    db.session.commit()
