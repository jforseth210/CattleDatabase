"""
Deletes cows with no tag numbers and renames cows with duplicate tag_numbers

VERY DANGEROUS
"""
import arrow
from main import create_app
from models.database import db, Cow
app = create_app()
with app.app_context():
    cows = Cow.query.all()
    for cow1 in cows:
        for cow2 in cows:
            if cow1 != cow2 and cow1.tag_number == cow2.tag_number:
                DATE1 = arrow.get(cow1.get_birthdate(), "YYYY-MM-DD")
                DATE2 = arrow.get(cow2.get_birthdate(), "YYYY-MM-DD")
                if DATE2.year not in (DATE1.year + 10, DATE1.year - 10):
                    print("-------------------------")
                    print(cow1.tag_number)
                    print(cow1.get_birthdate())
                    print(cow2.get_birthdate())
                else:
                    cow1.tag_number += f"-{DATE1.format('YY')}"
                    cow2.tag_number += f"-{DATE2.format('YY')}"
                    print("---------------------------")
                    print(cow1.tag_number, cow1.get_birthdate())
                    print(cow2.tag_number, cow2.get_birthdate())
    db.session.commit()
