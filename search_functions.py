"""
Functions that help with searches and filtrations
"""
from models.database import db
from models.cow import Cow
from models.event import Event
from models.transaction import Transaction


def get_unique_values():
    """
    This is used to determine which values are shown
    as options for the filters

    For example, if there are 15 "Born" events, "Born"
    should only appear once in the filter options.
    """
    unique_values = {}

    unique_values.update(cow_unique_values())
    unique_values.update(event_unique_values())
    unique_values.update(transaction_unique_values())

    unique_values["types"] = ["Cow", "Event", "Transaction"]
    return unique_values


def cow_unique_values():
    """
    Helper method for get_unique_values.
    Gets the unique values for cows.
    """
    sexes = db.session.query(Cow.sex).distinct().all()
    sexes = [sex for (sex,) in sexes]
    owners = db.session.query(Cow.owner).distinct().all()
    owners = [owner for (owner,) in owners]

    tags = ["0**", "1**", '2**', "3**", "4**",
            "5**", "6**", "7**", "8**", "9**", "N/A"]

    sire_ids = db.session.query(Cow.sire_id).distinct().all()
    sire_ids = [sire_id for (sire_id,) in sire_ids]
    sires = []
    for sire_id in sire_ids:
        sire = Cow.query.filter_by(cow_id=sire_id).first()
        if sire:
            sires.append(sire.tag_number)

    dam_ids = db.session.query(Cow.dam_id).distinct().all()
    dam_ids = [dam_id for (dam_id,) in dam_ids]
    dams = []
    for dam_id in dam_ids:
        dam = Cow.query.filter_by(cow_id=dam_id).first()
        if dam:
            dams.append(dam.tag_number)

    if None in tags:
        tags.remove(None)
    if None in sexes:
        sexes.remove(None)
    if None in owners:
        owners.remove(None)
    if None in sires:
        sires.remove(None)
    if None in dams:
        dams.remove(None)

    # Finally, return the sets.
    return {"tags": tags, "sexes": sexes,
            "owners": owners, "dams": dams, "sires": sires}


def event_unique_values():
    """
    Helper method for get_unique_values.
    Gets the unique values for events.
    """
    names = db.session.query(Event.name).distinct().all()
    names = [name for (name,) in names]
    dates = db.session.query(Event.date).distinct().all()
    dates = [date for (date,) in dates]

    if None in names:
        names.remove(None)

    # Finally, return the sets
    return {"event_names": names, "dates": dates}


def transaction_unique_values():
    """
    Helper method for get_unique_values.
    Gets the unique values for transactions.
    """
    names = db.session.query(Transaction.name).distinct().all()
    names = [name for (name,) in names]
    tofroms = db.session.query(Transaction.tofrom).distinct().all()
    tofroms = [tofrom for (tofrom,) in tofroms]
    prices = db.session.query(Transaction.price).distinct().all()
    prices = [price for (price,) in prices]

    if None in tofroms:
        tofroms.remove(None)
    if None in names:
        names.remove(None)
    if None in prices:
        prices.remove(None)

    # Finally, return the sets
    return {"transaction_names": names, "prices": prices, "tofroms": tofroms}
