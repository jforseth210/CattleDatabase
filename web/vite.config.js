import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'
import sveltePreprocess from 'svelte-preprocess'
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [svelte({ preprocess: sveltePreprocess() })],
  build: {
    lib: {
      // Could also be a dictionary or array of multiple entry points
      entry: "./src/main.js",
      name: 'main',
      // the proper extensions will be added
      fileName: 'main',
    },
    minify: false,
    sourcemap: true,
    outDir: "../static/dist",
    assetsDir: ".",
    rollupOptions: {
      output: {
        entryFileNames: `[name].js`,
        chunkFileNames: `[name].js`,
        assetFileNames: `[name].[ext]`
      }
    }
  }
})
