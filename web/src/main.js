// @ts-nocheck
import { createInertiaApp } from "@inertiajs/svelte";
import { mount } from "svelte";

createInertiaApp({
  resolve: (name) => import(`./pages/${name}.svelte`),
  setup({ el, App, props }) {
    new mount(App, { target: el, props });
  },
});
