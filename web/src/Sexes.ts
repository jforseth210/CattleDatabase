let fertileMale: string[] = ["Bull (AI)", "Bull"];
let sterileMale: string[] = ["Steer"];

let fertileFemale: string[] = ["Cow", "Heifer", "Heifer (Replacement)"];
let sterileFemale: string[] = ["Heifer (Market)", "Free-Martin"];

export function getSexes(sex?: string) {
  if (sex === "female") {
    return [...fertileFemale, ...sterileFemale];
  } 
  if (sex === "male") {
    return [...fertileMale, ...sterileMale];
  }
  return [...fertileMale, ...sterileMale, ...fertileFemale, ...sterileFemale];
}

export function isFemale(sex: string) {
  return [...fertileFemale, ...sterileFemale].includes(sex);
}

export function isMale(sex: string) {
  return [...fertileMale, ...sterileMale].includes(sex);
}

export function isSterile(sex: string) {
  return [...sterileFemale, ...sterileMale].includes(sex);
}

export function isFertile(sex: string) {
  return [...fertileFemale, ...fertileMale].includes(sex);
}
