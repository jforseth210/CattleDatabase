// Archive a group of events
export function archiveEvents(events) {
  events.map(async (event) => {
    if (event.isArchived) {
      return;
    }
    if (event.selected) {
      const response = await fetch(
        `/events/archive?event_id=${event.event_id}&no_redirect=true`,
        {
          method: "GET",
        }
      );
      if (response.status === 308 || response.status === 200) {
        event.isArchived = true;
      }
    }
  });
}

// Unarchive a group of events
export function unarchiveEvents(events) {
  events.map(async (event) => {
    if (!event.isArchived) {
      return;
    }
    if (event.selected) {
      const response = await fetch(
        `/events/unarchive?event_id=${event.event_id}&no_redirect=true`,
        {
          method: "GET",
        }
      );
      if (response.status === 308 || response.status === 200) {
        event.isArchived = false;
      }
    }
  });
}

// Delete a group of events
export function deleteEvents(events) {
  events.map(async (event) => {
    if (event.selected) {
      const formData = new FormData();
      formData.append("event_id", event.event_id);
      formData.append("no_redirect", "true");
      const response = await fetch(`/events/delete`, {
        method: "POST",
        body: formData,
      });
      if (response.status === 308 || response.status === 200) {
        events = events.filter((currentEvent) => event != currentEvent);
      }
    }
  });
}
