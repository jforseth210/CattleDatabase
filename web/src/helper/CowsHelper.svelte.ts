import type { Cow } from "../types";

export const enum CowListType {
  ALL = "ALL",
  CURRENT_COWS = "CURRENT_COWS",
}
let cows = $state([]);
let loadingCows = $state(true);
let sortField = $state("birthdate");
let sortDirection = $state("desc");
let archivedCowsVisible = $state(false);
let active_cow_list_type = $state(CowListType.ALL);
let page = $state(0);
const loadingFirstPage = $derived(loadingCows && page == 0);

export async function clearCows() {
  page = 0;
  cows = [];
}

// Load in a group of cows
export async function loadCows(): Promise<boolean> {
  const route =
    active_cow_list_type == CowListType.ALL ? "cows" : "current_cows";
  // Set the loading flag
  loadingCows = true;
  // Get the cows
  const response = await fetch(
    `/cows/page/${page}?list_type=${active_cow_list_type.toString()}&direction=${sortDirection}&field=${sortField}&include_archived=${archivedCowsVisible}`
  );
  // Put into the cows array
  const newData = await response.json();
  cows = [...cows, ...newData];

  // Get ready to load the next page
  page = page + 1;

  // Indicate that loading is completes
  loadingCows = false;
  return newData.length > 0;
}
export function isLoading() {
  return loadingCows;
}
export function getCows() {
  return cows;
}
export function getActiveCowsListType() {
  return active_cow_list_type;
}
export function setActiveCowsListType(listType: CowListType) {
  active_cow_list_type = listType;
  loadCows();
}
export function getArchivedCowsVisible() {
  return archivedCowsVisible;
}
export function getLoadingFirstPage() {
  return loadingFirstPage;
}
export function getSortField() {
  return sortField;
}
export function getSortDirection() {
  return sortDirection;
}
export function setArchivedCowsVisible(visible: boolean) {
  archivedCowsVisible = visible;
}
export async function loadAllCows() {
  let cowsLoaded;
  do {
    cowsLoaded = await loadCows();
  } while (cowsLoaded);
}
// Whether or not to show archived cows
export async function handleSort(event) {
  let field;
  if (event.target.dataset.field) {
    field = event.target.dataset.field;
  } else {
    field = event.target.value;
  }
  if (sortField === field) {
    if (sortDirection == "desc") {
      sortDirection = "asc";
    } else {
      sortDirection = "desc";
    }
  } else {
    sortField = field;
    sortDirection = "asc";
  }
  clearCows();
  await loadCows();
}
// Transfer a group of cows
export async function transferCows(
  cowsToTransfer: Cow[],
  owner: string,
  transferEventEnabled: boolean,
  archiveCows: boolean,
  date: string,
  price: number,
  description: string
) {
  const formData = new FormData();
  formData.append("no_redirect", "true");
  formData.append("new_owner", owner);
  formData.append("transfer_event_enabled", transferEventEnabled.toString());
  formData.append("archive_cow", archiveCows.toString());
  if (transferEventEnabled) {
    formData.append("date", date);
    formData.append("price", price.toString());
    formData.append("description", description);
  }

  cowsToTransfer.forEach((cow) => {
    if (cow.selected) {
      formData.append("tag_number", cow.tag_number);
    }
  });
  const response = await fetch(`/cows/transfer_ownership`, {
    method: "POST",
    body: formData,
  });
  if (response.status === 308 || response.status === 200) {
    cowsToTransfer.forEach((cow) => {
      if (cow.selected) {
        cow.owner = owner;
        if (archiveCows) {
          cow.isArchived = true;
        }
      }
    });
  }
}
// Archive a group of cows
export function archiveCows(cowsToArchive: Cow[]) {
  cowsToArchive.forEach((cow) => {
    (async (cow) => {
      if (cow.isArchived) {
        return;
      }
      const response = await fetch(
        `/cows/archive?tag_number=${cow.tag_number}&no_redirect=true`,
        {
          method: "GET",
        }
      );
      if (response.status === 308 || response.status === 200) {
        cow.isArchived = true;
      }
    })(cow);
  });
}

// Unarchive a group of cows
export function unarchiveCows(cowsToUnarchive: Cow[]) {
  cowsToUnarchive.forEach((cow) => {
    (async (cow) => {
      if (!cow.isArchived) {
        return;
      }
      const response = await fetch(
        `/cows/unarchive?tag_number=${cow.tag_number}&no_redirect=true`,
        {
          method: "GET",
        }
      );
      if (response.status === 308 || response.status === 200) {
        cow.isArchived = false;
      }
    })(cow);
  });
}

// Delete a group of cows
export function deleteCows(cowsToDelete: Cow[]) {
  cowsToDelete.forEach((cow) => {
    (async (cow) => {
      const formData = new FormData();
      formData.append("tag_number", cow.tag_number);
      formData.append("no_redirect", "true");
      const response = await fetch(`/cows/delete`, {
        method: "POST",
        body: formData,
      });
      if (response.status === 308 || response.status === 200) {
        cows = cows.filter((currentCow) => cow != currentCow);
      }
    })(cow);
  });
}

export async function addCowsToEvent(eventId: number, selectedCows: Cow[]) {
  await fetch("/api/events/update_cows", {
    method: "POST",
    body: JSON.stringify({
      event_id: eventId,
      cows: selectedCows.map((cow) => cow["tag_number"]),
      mode: "append",
    }),
  });
}
