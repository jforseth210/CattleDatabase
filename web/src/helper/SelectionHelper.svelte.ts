type Item = {
  selected: boolean;
  visible: boolean;
};
/**
 * This class is responsible for managing multi-selecting objects
 */
export class SelectionHelper {
  // List of 'things' with selected and visibile attributes
  private items: Item[] = [];

  // Function to tell the parent component that the selection has changed
  private onSelectionChange: Function;

  // Used for shift-select
  private lastSelectedIndex = null;

  // Whether or not every item is selected
  private allSelected = false;

  /**
   * Accepts a function that will be called
   */
  public constructor(onSelectionChange: Function) {
    this.onSelectionChange = onSelectionChange;
  }

  /**
   * Toggles the selection of all items between the two indices
   * @param lower the lower bound of items to toggle
   * @param higher the upper bound of items to toggle
   */
  public selectRange(lower, higher) {
    for (let i = lower; i < higher; i++) {
      this.items[i].selected = !this.items[i].selected;
    }
    this.lastSelectedIndex = null;
    this.onSelectionChange(this.items);
  }

  /**
   * Select or deselect everything
   **/
  public masterToggle() {
    this.lastSelectedIndex = null;
    this.allSelected = !this.allSelected;
    if (this.allSelected) {
      this.items.forEach((item) => {
        if (item.visible) {
          item.selected = true;
        }
      });
    } else {
      this.items.forEach((item) => (item.selected = false));
    }
    this.onSelectionChange(this.items);
  }

  
  public handleShiftSelect(currentIndex) {
    document.getSelection()?.removeAllRanges();
    if (this.lastSelectedIndex == null) {
      this.items[currentIndex].selected = !this.items[currentIndex].selected;
      this.lastSelectedIndex = currentIndex;
      this.onSelectionChange(this.items);
      return;
    }
    let lower, higher;
    if (this.lastSelectedIndex > currentIndex) {
      lower = currentIndex;
      higher = this.lastSelectedIndex;
    } else {
      lower = this.lastSelectedIndex + 1;
      higher = currentIndex + 1;
    }
    this.selectRange(lower, higher);
    this.onSelectionChange(this.items);
  }
  public getAllSelected() {
    return this.allSelected;
  }
  public setLastSelectedIndex(index) {
    this.lastSelectedIndex = index;
  }
  public setItems(items) {
    this.items = items;
  }
}
