import type { Transaction } from "../types";

// Archive a group of transactions
export function archiveTransactions(
  transactions: { isArchived: boolean; transaction_id: number }[]
) {
  transactions.map(async (transaction) => {
    if (transaction.isArchived) {
      return;
    }
    const response = await fetch(
      `/transactions/archive?transaction_id=${transaction.transaction_id}&no_redirect=true`,
      {
        method: "GET",
      }
    );
    if (response.status === 308 || response.status === 200) {
      transaction.isArchived = true;
    }
  });
}

// Unarchive a group of transactions
export function unarchiveTransactions(transactions: Transaction[]) {
  transactions.map(async (transaction) => {
    if (!transaction.isArchived) {
      return;
    }
    const response = await fetch(
      `/transactions/unarchive?transaction_id=${transaction.transaction_id}&no_redirect=true`,
      {
        method: "GET",
      }
    );
    if (response.status === 308 || response.status === 200) {
      transaction.isArchived = false;
    }
  });
}

// Delete a group of transactions
export function deleteTransactions(transactions: Transaction[]) {
  transactions.map(async (transaction) => {
    const formData = new FormData();
    formData.append("transaction_id", transaction.transaction_id.toString());
    formData.append("no_redirect", "true");
    const response = await fetch(`/transactions/delete`, {
      method: "POST",
      body: formData,
    });
    if (response.status === 308 || response.status === 200) {
      transactions = transactions.filter(
        (currentTransaction) => transaction != currentTransaction
      );
    }
  });
}
