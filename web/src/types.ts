export type Cow = {
  tag_number: string;
  dam: Cow;
  sire: Cow;
  sex: string;
  owner: string;
  isArchived: boolean;
  cow_id: number;
  birthdate: string;
  deathdate: string;
  causeOfDeath: string;
  description: string;
  calves: Cow[];
  events: Event[];
  transactions: Transaction[];
  visible: boolean;
  selected: boolean;
};

export type Event = {
  name: string;
  date: string;
  description: string;
  isArchived: boolean;
  transactions: Transaction[];
  cows: Cow[];
  event_id: number;
  visible: boolean;
  selected: boolean;
};
export type Transaction = {
  name: string;
  price: number;
  transaction_id: number;
  isArchived: boolean;
};

export type SearchResult = {
  title: string;
  id: string | number;
};
