import { test as setup } from "@playwright/test";
import { rename } from "fs";
import path from "path";

setup("initialize", async ({ page }) => {
  rename(
    "cattle.db.testrunning",
    path.join(import.meta.dirname, "../../cattle.db"),
    () => {}
  );
  rename(
    "config.json.testrunning",
    path.join(import.meta.dirname, "../../config.json"),
    () => {}
  );
});
