import { test as setup } from "@playwright/test";
import { rename } from "fs";
import { access } from "fs/promises";
import path from "path";

const authFile = path.join(
  import.meta.dirname,
  "../playwright/.auth/user.json"
);

setup("initialize", async ({ page }) => {
  await page.goto("http://localhost:31523/install/show_server_info");
  console.log("Setting up");
  rename(
    path.join(import.meta.dirname, "../../cattle.db"),
    "cattle.db.testrunning",
    () => {}
  );
  rename(
    path.join(import.meta.dirname, "../../config.json"),
    "config.json.testrunning",
    () => {}
  );
  await page.goto("http://localhost:31523/install");

  await page.getByRole("link", { name: "Create a New Database" }).click();
  await page.getByRole("link", { name: "Create a New Configuration" }).click();
  if (await page.isVisible("input[name='username']")) {
    await page.locator('input[name="username"]').click();
    await page.locator('input[name="username"]').fill("testuser");
    await page.locator('input[name="username"]').press("Tab");
    await page.locator('input[name="password"]').fill("password");
    await page.locator('input[name="password"]').press("Tab");
    await page.locator('input[name="confirm_password"]').fill("password");
    await page.getByRole("button", { name: "Create Credentials" }).click();

    await page.getByRole("link", { name: "No" }).click();
  }
  await page.getByRole("link", { name: "Offline Mode" }).click();
  await page.getByRole("link", { name: "Go to CattleDB" }).click();

  await page.locator("#username").click();
  await page.locator("#username").fill("testuser");
  await page.locator("#username").press("Tab");
  await page.locator("#password").fill("password");
  await page.getByRole("button", { name: "Submit" }).click();

  await page.context().storageState({ path: authFile });
});
