import { test, expect } from "@playwright/test";
import { createCow, deleteCow, openCowPage, VALID_SEXES } from "./cow_helper";
test("Test change tag number", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createCow(
    page,
    "Noteworthy Cow" + testId,
    "Test User",
    VALID_SEXES.COW,
    "See, look at all of these notes!"
  );
  // Navigate
  await openCowPage(page, "Noteworthy Cow" + testId);
  await expect(page.locator("#app")).toContainText(
    "Notes: See, look at all of these notes!"
  );
  await page.getByRole("button", { name: " Add Notes" }).click();
  await expect(page.getByRole("textbox")).toHaveValue(
    "See, look at all of these notes!"
  );
  await page.getByRole("textbox").click();
  await page
    .getByRole("textbox")
    .fill(
      "In fact, I'm going to take EVEN MORE notes, because this cow is just so noteworthy"
    );
  await page.getByRole("button", { name: "Add Notes", exact: true }).click();
  await expect(page.locator("#app")).toContainText(
    "In fact, I'm going to take EVEN MORE notes, because this cow is just so noteworthy"
  );

  await deleteCow(page, "Noteworthy Cow" + testId);
});
