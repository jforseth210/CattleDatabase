import { test, expect } from "@playwright/test";
import { createCow, deleteCow, VALID_SEXES } from "./cow_helper";
import { locateTableRow } from "../helper";
test("Test cow deletion", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createCow(
    page,
    "Test Cow" + testId,
    "Test User",
    VALID_SEXES.HEIFER_REPLACEMENT,
    "This is a note"
  );
  await deleteCow(page, "Test Cow" + testId);
  await expect(locateTableRow(page, "Test Cow" + testId)).toHaveCount(0);
});
