import { test, expect } from "@playwright/test";
import { deleteCow } from "./cow_helper";
import { locateTableRow } from "../helper";

test("bulk calving record entry", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await page.goto("http://localhost:31523/cows/");
  await page.getByRole("link", { name: "Add Calving Record" }).click();

  await page.getByPlaceholder("Owner").click();
  await page.getByPlaceholder("Owner").fill("TestUser");
  await page.getByPlaceholder("Owner").press("Tab");
  await page.locator('input[name="cow-0"]').click();
  await page.locator('input[name="cow-0"]').fill("Cow" + testId);
  await page.locator('input[name="cow-0"]').press("Tab");
  await expect(page.locator('input[name="owner-0"]')).toHaveValue("TestUser");
  await expect(page.locator("tbody")).toContainText("Cow does not exist");
  await expect(page.locator("tbody")).toContainText("Please specify a calf");
  await expect(page.locator("tbody")).toContainText(
    "Please specify a date of birth"
  );
  await expect(page.locator("tbody")).toContainText("Please specify a sex");
  await page.locator('input[name="cow-0"]').click();
  await page.locator('input[name="cow-0"]').press("Tab");
  await page.locator('input[name="calf-0"]').fill("Calf" + testId);
  await page.locator('input[name="calf-0"]').press("Tab");
  await expect(page.locator("tbody")).toContainText("Calf will be added");
  await page.locator('input[name="owner-0"]').click();
  await page.locator('input[name="owner-0"]').fill("estet" + testId);
  await page.locator('input[name="dob-0"]').fill("2024-01-01");

  await page.locator('input[name="sex-0"]').click();
  await expect(page.locator("tbody")).toContainText("Looks good!");
  await page.locator('input[name="sex-0"]').click();
  await page.locator('input[name="sex-0"]').fill("heifer");
  await page.locator('input[name="sex-0"]').press("Tab");
  await expect(page.locator("tbody")).toContainText("Looks good!");
  await page.locator('input[name="cow-1"]').click();
  await page.locator('input[name="cow-1"]').fill("Cow2" + testId);
  await expect(page.locator('input[name="owner-1"]')).toHaveValue("TestUser");
  await page.getByText("Please specify a date of birth").nth(1).click();
  await expect(page.locator("tbody")).toContainText("Please specify a sex");
  await expect(page.locator("tbody")).toContainText(
    "Please specify a date of birth"
  );
  await expect(page.locator("tbody")).toContainText("Calf will be added");
  await page.locator('input[name="cow-1"]').click();
  await page.locator('input[name="cow-1"]').press("Tab");
  await page.locator('input[name="calf-1"]').fill("Test2" + testId);
  await page.locator('input[name="calf-1"]').press("Tab");
  await page.locator('input[name="owner-1"]').press("Tab");
  await page.locator('input[name="dob-1"]').fill("2012-02-15");
  await page.locator('input[name="dob-1"]').press("Tab");
  await page.locator('input[name="dob-1"]').press("Tab");
  await page.locator('input[name="sex-1"]').fill("h");
  await page.locator('input[name="sex-1"]').click();
  await page.locator('input[name="sex-1"]').press("Tab");
  await expect(page.locator("tbody")).toContainText("Looks good!");
  await page.getByText("Cow does not exist").first().click();
  await expect(page.locator("tbody")).toContainText("Cow does not exist");
  await expect(page.locator("tbody")).toContainText("Cow does not exist");
  await page.getByLabel("Create cows and sires if they").check();
  await expect(page.locator("tbody")).toContainText("Cow will be created");
  await expect(page.locator("tbody")).toContainText("Cow will be created");
  await page.getByLabel("Create cows and sires if they").uncheck();
  await expect(page.locator("tbody")).toContainText("Cow does not exist");
  await expect(page.locator("tbody")).toContainText("Cow does not exist");
  await page.locator('input[name="cow-2"]').click();
  await page.locator('input[name="cow-2"]').fill("Bad");
  await page.locator('input[name="cow-2"]').press("Tab");
  await page.locator('input[name="calf-2"]').fill("Test2");
  await page.locator('input[name="calf-2"]').press("Tab");
  await page.locator('input[name="owner-2"]').fill("TestUser");
  await page.locator('input[name="owner-2"]').press("Tab");
  await page.locator('input[name="dob-2"]').fill("9000-05-06");
  await page.locator('input[name="dob-2"]').press("Tab");
  await page.locator('input[name="dob-2"]').press("Tab");
  await page.locator('input[name="sex-2"]').fill("it's a boy");
  await page.locator('input[name="sex-2"]').press("Tab");
  await expect(page.locator("tbody")).toContainText(
    "Future dates are not allowed"
  );
  await expect(page.locator("tbody")).toContainText("Please enter h, s, or b");
  await page.locator('input[name="cow-2"]').click();
  await page.locator('input[name="cow-2"]').fill("Cow3" + testId);
  await page.locator('input[name="cow-2"]').press("Tab");
  await page.locator('input[name="calf-2"]').fill("Test3" + testId);
  await page.locator('input[name="calf-2"]').press("Tab");
  await page.locator('input[name="owner-2"]').fill("TestUser");
  await page.locator('input[name="owner-2"]').press("Tab");
  await page.locator('input[name="dob-2"]').press("ArrowRight");
  await page.locator('input[name="sex-2"]').press("ArrowRight");
  await page.locator('input[name="description-2"]').press("ArrowLeft");
  await page.locator('input[name="sex-2"]').press("ArrowLeft");
  await page.locator('input[name="dob-2"]').fill("2022-05-06");
  await page.locator('input[name="dob-2"]').press("Tab");
  await page.locator('input[name="dob-2"]').press("Tab");
  await page.locator('input[name="sex-2"]').fill("bull");
  await page.locator('input[name="sex-2"]').press("Tab");
  await expect(page.locator("tbody")).toContainText("Cow does not exist");
  await expect(page.locator("tbody")).toContainText("Calf will be added");
  await expect(page.locator("tbody")).toContainText("Looks good!");
  await expect(page.locator("tbody")).toContainText("Looks good!");
  await page.getByRole("button", { name: "Submit" }).click();
  await page.getByLabel("Create cows and sires if they").check();
  await page.getByRole("button", { name: "Submit" }).click();
  await expect(
    locateTableRow(page, "Calf" + testId).filter({ hasText: "2024-01-01" })
  ).toMatchAriaSnapshot(`
    - row /Calf${testId} \\d+-\\d+-\\d+ estet${testId} Heifer Cow${testId}/:
      - cell:
        - checkbox: "on"
      - cell "Calf${testId}"
      - cell /\\d+-\\d+-\\d+/
      - cell "estet${testId}"
      - cell "Heifer"
      - cell "Cow${testId}"
      - cell
      - cell
      - cell
  `);
  await expect(
    locateTableRow(page, "Test2" + testId).filter({ hasText: "2012-02-15" })
  ).toMatchAriaSnapshot(`
    - row /Test2${testId} \\d+-\\d+-\\d+ TestUser Heifer Cow2${testId}/:
      - cell:
        - checkbox: "on"
      - cell "Test2${testId}"
      - cell /\\d+-\\d+-\\d+/
      - cell "TestUser"
      - cell "Heifer"
      - cell "Cow2${testId}"
      - cell
      - cell
      - cell
  `);

  await expect(
    locateTableRow(page, "Test3" + testId).filter({ hasText: "2022-05-06" })
  ).toMatchAriaSnapshot(`
    - row /Test3${testId} \\d+-\\d+-\\d+ TestUser Bull Cow3${testId}/:
      - cell:
        - checkbox: "on"
      - cell "Test3${testId}"
      - cell /\\d+-\\d+-\\d+/
      - cell "TestUser"
      - cell "Bull"
      - cell "Cow3${testId}"
      - cell
      - cell
      - cell
  `);
  await page.locator("#table-bottom").scrollIntoViewIfNeeded();
  await expect(locateTableRow(page, "Cow" + testId, "2024-01-01"))
    .toMatchAriaSnapshot(`
    - row "Cow${testId} estet${testId} Cow Calf${testId}":
      - cell:
        - checkbox: "on"
      - cell "Cow${testId}"
      - cell
      - cell "estet${testId}"
      - cell "Cow"
      - cell
      - cell
      - cell "Calf${testId}"
      - cell
  `);
  await expect(locateTableRow(page, "Cow2" + testId, "2012-02-15"))
    .toMatchAriaSnapshot(`
    - row "Cow2${testId} TestUser Cow Test2${testId}":
      - cell:
        - checkbox: "on"
      - cell "Cow2${testId}"
      - cell
      - cell "TestUser"
      - cell "Cow"
      - cell
      - cell
      - cell "Test2${testId}"
      - cell
  `);

  await expect(locateTableRow(page, "Cow3" + testId, "2022-05-06"))
    .toMatchAriaSnapshot(`
    - row "Cow3${testId} TestUser Cow Test3${testId}":
      - cell:
        - checkbox: "on"
      - cell "Cow3${testId}"
      - cell
      - cell "TestUser"
      - cell "Cow"
      - cell
      - cell
      - cell "Test3${testId}"
      - cell
  `);
  await deleteCow(page, "Cow" + testId);
  await deleteCow(page, "Cow2" + testId);
  await deleteCow(page, "Cow3" + testId);
  await deleteCow(page, "Test2" + testId);
  await deleteCow(page, "Calf" + testId);
  await deleteCow(page, "Test3" + testId);
});
