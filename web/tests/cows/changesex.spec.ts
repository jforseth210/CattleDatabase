import { test, expect } from "@playwright/test";
import { createCow, deleteCow, openCowPage, VALID_SEXES } from "./cow_helper";
test("Test change sex", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createCow(
    page,
    "Bull... for now" + testId,
    "Test User",
    VALID_SEXES.BULL,
    ""
  );
  // Navigate
  await openCowPage(page, "Bull... for now" + testId);
  await expect(page.locator("#app")).toContainText("Sex: Bull");
  await page.getByRole("button", { name: " Change Sex" }).click();
  await expect(page.getByRole("combobox")).toMatchAriaSnapshot(`
    - combobox:
      - option "Bull (AI)" [selected]
      - option "Bull"
      - option "Steer"
      - option "Cow"
      - option "Heifer"
      - option "Heifer (Replacement)"
      - option "Heifer (Market)"
      - option "Free-Martin"
    `);
  await page.getByRole("combobox").selectOption("Steer");
  await expect(page.getByRole("combobox")).toMatchAriaSnapshot(`
    - combobox:
      - option "Bull (AI)"
      - option "Bull"
      - option "Steer" [selected]
      - option "Cow"
      - option "Heifer"
      - option "Heifer (Replacement)"
      - option "Heifer (Market)"
      - option "Free-Martin"
    `);
  await page.getByRole("button", { name: "Update" }).click();
  await expect(page.getByRole("heading")).toContainText(
    "Bull... for now" + testId
  );
  await expect(page.locator("#app")).toContainText("Owned By: Test User");
  await expect(page.locator("#app")).toContainText("Sex: Steer");
  await expect(page.locator("#app")).toContainText("Archived: No");
  await expect(page.locator("#app")).toContainText("Notes:");
  // Record archive
  await deleteCow(page, "Bull... for now" + testId);
});
