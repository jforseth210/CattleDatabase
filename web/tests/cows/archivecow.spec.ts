import { test, expect } from "@playwright/test";
import { createCow, deleteCow, VALID_SEXES } from "./cow_helper";
import { locateTableRow } from "../helper";
test("Test archiving", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createCow(
    page,
    "Archived cow" + testId,
    "Test User",
    VALID_SEXES.COW,
    "This record isn't important"
  );
  // Navigate
  await page.goto("http://localhost:31523/cows");
  await page.locator("#table-bottom").scrollIntoViewIfNeeded();
  await page.getByRole("cell", { name: "Archived cow" + testId }).click();

  // Record archive
  await page.getByRole("link", { name: " Archive" }).click();

  // Check archive
  await page.getByRole("link", { name: "Cows" }).click();
  await expect(locateTableRow(page, "Archived cow" + testId)).toHaveCount(0);
  await page.getByLabel("Show Archived Cows").check();
  await page.locator("#table-bottom").scrollIntoViewIfNeeded();
  await expect(locateTableRow(page, "Archived cow" + testId))
    .toMatchAriaSnapshot(`
    - row "Archived cow${testId} Test User Cow This record isn't important":
      - cell:
        - checkbox: "on"
      - cell "Archived cow${testId}"
      - cell
      - cell "Test User"
      - cell "Cow"
      - cell
      - cell
      - cell
      - cell "This record isn't important"
  `);

  // Navigate
  await page.getByRole("cell", { name: "Archived cow" + testId }).click();
  await expect(page.locator("#app")).toContainText("Archived: Yes");
  await deleteCow(page, "Archived cow" + testId);
});
