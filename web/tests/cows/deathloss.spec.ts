import { test, expect } from "@playwright/test";
import { createCow, deleteCow, VALID_SEXES } from "./cow_helper";
import { locateTableRow } from "../helper";
test("Test cow death loss", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createCow(
    page,
    "Dead cow walking" + testId,
    "Test User",
    VALID_SEXES.STEER,
    "This cow will be dead soon"
  );
  // Navigate
  await page.goto("http://localhost:31523/cows");
  await page.locator("#table-bottom").scrollIntoViewIfNeeded();
  await page.getByRole("cell", { name: "Dead cow walking" + testId }).click();

  // Record death
  await page.getByRole("button", { name: " Death Loss" }).click();
  await page.locator('input[type="date"]').fill("2024-12-20");
  await page.locator("textarea").click();
  await page.locator("textarea").fill("Caught in alien tractor beam");
  await page.getByRole("button", { name: "Save" }).click();

  // Check death loss
  await expect(page.locator("#app")).toContainText(
    "Died: 2024-12-20: Caught in alien tractor beam"
  );
  await expect(page.locator("#app")).toContainText("Archived: Yes");
  await page.getByRole("link", { name: "Events" }).nth(1).click();
  await expect(page.getByRole("table")).toMatchAriaSnapshot(`
    - table:
      - rowgroup:
        - row "Date ▴▾ Name ▴▾ Description ▴▾":
          - cell:
            - checkbox: "on"
          - cell "Date ▴▾"
          - cell "Name ▴▾"
          - cell "Description ▴▾"
      - rowgroup:
        - 'row /\\d+-\\d+-\\d+ Died Dead cow walking${testId} died: Caught in alien tractor beam/':
          - cell:
            - checkbox: "on"
          - cell /\\d+-\\d+-\\d+/
          - cell "Died"
          - 'cell "Dead cow walking${testId} died: Caught in alien tractor beam"'
      - rowgroup
  `);
  await page.getByRole("link", { name: "Cows" }).click();
  await expect(locateTableRow(page, "Dead cow walking" + testId)).toHaveCount(
    0
  );
  await page.getByLabel("Show Archived Cows").check();
  await page.locator("#table-bottom").scrollIntoViewIfNeeded();
  await expect(locateTableRow(page, "Dead cow walking" + testId))
    .toMatchAriaSnapshot(`
    - row "Dead cow walking${testId} Test User Steer This cow will be dead soon":
      - cell:
        - checkbox: "on"
      - cell "Dead cow walking${testId}"
      - cell
      - cell "Test User"
      - cell "Steer"
      - cell
      - cell
      - cell
      - cell "This cow will be dead soon"
  `);
  await deleteCow(page, "Dead cow walking" + testId);
});
