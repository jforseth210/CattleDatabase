import { test, expect } from "@playwright/test";
import { createEvent, deleteEvent } from "../events/event_helper";
import { createCow, deleteCow, openCowPage, VALID_SEXES } from "./cow_helper";
import { locateTableRow } from "../helper";

test("Test add cow to existing event", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createEvent(
    page,
    "2024-12-23",
    "Test event for cow" + testId,
    "This is a test event"
  );
  await createCow(
    page,
    "Test cow for event" + testId,
    "Test user",
    VALID_SEXES.COW,
    ""
  );

  await openCowPage(page, "Test cow for event" + testId);
  await page.getByRole("link", { name: "Events" }).nth(1).click();
  await page.getByRole("button", { name: "Add to Existing Event" }).click();
  await page
    .getByRole("combobox")
    .selectOption("2024-12-23 - Test event for cow" + testId);
  await page.getByRole("button", { name: "Add to Event" }).click();
  await expect(locateTableRow(page, "Test event for cow" + testId))
    .toMatchAriaSnapshot(`
    - row /\\d+-\\d+-\\d+ Test event for cow${testId} This is a test event/:
      - cell:
        - checkbox: "on"
      - cell /\\d+-\\d+-\\d+/
      - cell "Test event for cow${testId}"
      - cell "This is a test event"
  `);
  await page.getByRole("cell", { name: "Test event for cow" + testId }).click();
  await expect(locateTableRow(page, "Test cow for event" + testId))
    .toMatchAriaSnapshot(`
    - row "Test cow for event${testId} Test user Cow":
      - cell:
        - checkbox: "on"
      - cell "Test cow for event${testId}"
      - cell
      - cell "Test user"
      - cell "Cow"
      - cell
      - cell
      - cell
      - cell
  `);
  //await deleteCow(page, "Test cow for event" + testId);
  //await deleteEvent(page, "Test event for cow" + testId);
});
