import { test, expect } from "@playwright/test";
import { createCow, deleteCow, VALID_SEXES } from "./cow_helper";

test("Test simple transfer", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createCow(
    page,
    "Transfer cow" + testId,
    "Test User",
    VALID_SEXES.COW,
    "This cows gonna get sold"
  );
  // Navigate
  await page.goto("http://localhost:31523/cows");
  await page.locator("#table-bottom").scrollIntoViewIfNeeded();
  await page.getByRole("cell", { name: "Transfer cow" + testId }).click();

  // Transfer
  await page.getByRole("button", { name: " Transfer Ownership" }).click();
  await page.locator('input[name="transfer_event_enabled"]').uncheck();
  await page.locator('input[name="archive_cow"]').uncheck();
  await page.getByRole("textbox").click();
  await page.getByRole("textbox").fill("New owner");

  // Check transfer
  await page.getByRole("button", { name: "Transfer", exact: true }).click();
  await expect(page.getByRole("heading")).toContainText(
    "Transfer cow" + testId
  );
  await expect(page.locator("#app")).toContainText("Owned By: New owner");
  await expect(page.locator("#app")).toContainText("Sex: Cow");
  await expect(page.locator("#app")).toContainText("Archived: No");
  await expect(page.locator("#app")).toContainText(
    "Notes: This cows gonna get sold"
  );
  await page.getByRole("link", { name: "Events" }).nth(1).click();
  await expect(page.getByRole("table")).toMatchAriaSnapshot(`
    - table:
      - rowgroup:
        - row "Date ▴▾ Name ▴▾ Description ▴▾":
          - cell:
            - checkbox: "on"
          - cell "Date ▴▾"
          - cell "Name ▴▾"
          - cell "Description ▴▾"
      - rowgroup
      - rowgroup
  `);
  await deleteCow(page, "Transfer cow" + testId);
});
test("Test simple transfer with archive", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createCow(
    page,
    "Transfer cow" + testId,
    "Test User",
    VALID_SEXES.COW,
    "This cows gonna get sold"
  );
  // Navigate
  await page.goto("http://localhost:31523/cows");
  await page.locator("#table-bottom").scrollIntoViewIfNeeded();
  await page.getByRole("cell", { name: "Transfer cow" + testId }).click();

  // Transfer
  await page.getByRole("button", { name: " Transfer Ownership" }).click();
  await page.locator('input[name="transfer_event_enabled"]').uncheck();
  await page.getByRole("textbox").click();
  await page.getByRole("textbox").fill("New owner");

  // Check transfer
  await page.getByRole("button", { name: "Transfer", exact: true }).click();
  await expect(page.getByRole("heading")).toContainText(
    "Transfer cow" + testId
  );
  await expect(page.locator("#app")).toContainText("Owned By: New owner");
  await expect(page.locator("#app")).toContainText("Sex: Cow");
  await expect(page.locator("#app")).toContainText("Archived: Yes");
  await expect(page.locator("#app")).toContainText(
    "Notes: This cows gonna get sold"
  );
  await page.getByRole("link", { name: "Events" }).nth(1).click();
  await expect(page.getByRole("table")).toMatchAriaSnapshot(`
      - table:
        - rowgroup:
          - row "Date ▴▾ Name ▴▾ Description ▴▾":
            - cell:
              - checkbox: "on"
            - cell "Date ▴▾"
            - cell "Name ▴▾"
            - cell "Description ▴▾"
        - rowgroup
        - rowgroup
    `);
  await deleteCow(page, "Transfer cow" + testId);
});
test("Test transfer with event", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createCow(
    page,
    "Transfer cow" + testId,
    "Test User",
    VALID_SEXES.COW,
    "This cows gonna get sold"
  );
  // Navigate
  await page.goto("http://localhost:31523/cows");
  await page.locator("#table-bottom").scrollIntoViewIfNeeded();
  await page.getByRole("cell", { name: "Transfer cow" + testId }).click();

  // Transfer
  await page.getByRole("button", { name: " Transfer Ownership" }).click();
  await page.locator('input[name="archive_cow"]').uncheck();
  await page.locator('input[name="new_owner"]').click();
  await page.locator('input[name="new_owner"]').fill("New owner guy");
  await page.locator('input[name="date"]').fill("2021-05-01");
  await page.locator('input[name="price"]').click();
  await page.locator('input[name="price"]').fill("500");
  await page.locator('textarea[name="description"]').click();
  await page
    .locator('textarea[name="description"]')
    .fill("This information is additional");
  await page.getByRole("button", { name: "Transfer", exact: true }).click();

  // Check transfer
  await expect(page.getByRole("heading")).toContainText(
    "Transfer cow" + testId
  );
  await expect(page.locator("#app")).toContainText("Owned By: New owner");
  await expect(page.locator("#app")).toContainText("Sex: Cow");
  await expect(page.locator("#app")).toContainText(
    "Notes: This cows gonna get sold"
  );
  await page.getByRole("link", { name: "Events" }).nth(1).click();
  await expect(page.getByRole("table")).toContainText("2021-05-01");
  await expect(page.getByRole("table")).toContainText("Transfer");
  await expect(page.getByRole("table")).toContainText(
    `Transfer Transfer cow${testId} from New owner guy to New owner guy: This information is additional`
  );

  // Check transfer transaction
  await page.getByRole("link", { name: "Transactions" }).nth(1).click();
  await expect(page.getByRole("table")).toContainText("2021-05-01");
  await expect(page.getByRole("table")).toContainText("New owner guy");
  await expect(page.getByRole("table")).toContainText("Sold");
  await expect(page.getByRole("table")).toContainText(
    `New owner guy sold Transfer cow${testId}: This information is additional`
  );
  await expect(page.getByRole("table")).toContainText("$500.00");
  await deleteCow(page, "Transfer cow" + testId);
});
