import { test, expect } from "@playwright/test";
import { createCow, deleteCow, openCowPage, VALID_SEXES } from "./cow_helper";
test("Test cow page exists and looks right", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createCow(
    page,
    "Test Cow" + testId,
    "Test User",
    VALID_SEXES.HEIFER_REPLACEMENT,
    "This is a note"
  );
  await openCowPage(page, "Test Cow" + testId);
  await expect(page.getByRole("heading")).toContainText("Test Cow" + testId);
  await expect(page.locator("#app")).toContainText("Owned By: Test User");
  await expect(page.locator("#app")).toContainText("Sex: Heifer (Replacement)");
  await expect(page.locator("#app")).toContainText("Archived: No");
  await expect(page.locator("#app")).toContainText("Notes: This is a note");
  await expect(page.locator("#app")).toContainText("Offspring");
  await expect(page.locator("#app")).toContainText("Events");
  await expect(page.locator("#app")).toContainText("Transactions");
  await expect(page.locator("#app")).toContainText("Family Tree");
  await expect(page.locator("#cows")).toContainText("Add Calf");
  await page.getByRole("link", { name: "Events" }).nth(1).click();
  await expect(page.locator("#events")).toContainText("Add Event");
  await expect(page.locator("#events")).toContainText("Add to Existing Event");
  await page.getByRole("link", { name: "Family Tree" }).click();
  await expect(page.locator("canvas")).toBeVisible();
  await deleteCow(page, "Test Cow" + testId);
});
