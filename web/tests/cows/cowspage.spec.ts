import { test, expect } from "@playwright/test";
import { createCow, VALID_SEXES } from "./cow_helper";

test("everything exists", async ({ page }) => {
  await page.goto("http://localhost:31523/cows/");
  await expect(page.getByPlaceholder("Search")).toBeEmpty();
  await expect(page.locator("html")).toMatchAriaSnapshot(`
    - document:
      - navigation:
        - link "CattleDB"
        - list:
          - listitem:
            - link "Cows"
          - listitem:
            - link "Events"
          - listitem:
            - link "Transactions"
          - listitem:
            - link "Calendar"
        - list:
          - searchbox "Search"
          - button "Search"
          - listitem:
            - button "testuser"
      - tablist:
        - tab "All Cattle" [selected]
        - tab "Current Cows"
      - paragraph: Curently viewing active cattle in the database, including cows, bulls, and calves.
      - button "Add Cow"
      - link "Add Calving Record"
      - link "Filter"
      - button "Print"
      - checkbox "Show Archived Cows"
      - text: Show Archived Cows
      - table:
        - rowgroup:
          - row "Cow ▴▾ Born ▾ Owner ▴▾ Sex ▴▾ Dam Sire Calves Notes ▴▾":
            - cell:
              - checkbox: "on"
            - cell "Cow ▴▾"
            - cell "Born ▾"
            - cell "Owner ▴▾"
            - cell "Sex ▴▾"
            - cell "Dam"
            - cell "Sire"
            - cell "Calves"
            - cell "Notes ▴▾"
        - rowgroup
  `);
});
test("everything exists with cow", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createCow(
    page,
    "Test Cow" + testId,
    "Test User",
    VALID_SEXES.HEIFER_REPLACEMENT,
    "This is a note"
  );
  await page.goto("http://localhost:31523/cows/");
  await expect(page.getByPlaceholder("Search")).toBeEmpty();
  await expect(page.locator("html")).toMatchAriaSnapshot(`
    - document:
      - navigation:
        - link "CattleDB"
        - list:
          - listitem:
            - link "Cows"
          - listitem:
            - link "Events"
          - listitem:
            - link "Transactions"
          - listitem:
            - link "Calendar"
        - list:
          - searchbox "Search"
          - button "Search"
          - listitem:
            - button "testuser"
      - tablist:
        - tab "All Cattle" [selected]
        - tab "Current Cows"
      - paragraph: Curently viewing active cattle in the database, including cows, bulls, and calves.
      - button "Add Cow"
      - link "Add Calving Record"
      - link "Filter"
      - button "Print"
      - checkbox "Show Archived Cows"
      - text: Show Archived Cows
      - table:
        - rowgroup:
          - row "Cow ▴▾ Born ▾ Owner ▴▾ Sex ▴▾ Dam Sire Calves Notes ▴▾":
            - cell:
              - checkbox: "on"
            - cell "Cow ▴▾"
            - cell "Born ▾"
            - cell "Owner ▴▾"
            - cell "Sex ▴▾"
            - cell "Dam"
            - cell "Sire"
            - cell "Calves"
            - cell "Notes ▴▾"
        - rowgroup:
          - row "Test Cow${testId} Test User Heifer (Replacement) This is a note":
            - cell:
              - checkbox: "on"
            - cell "Test Cow${testId}"
            - cell
            - cell "Test User"
            - cell "Heifer (Replacement)"
            - cell
            - cell
            - cell
            - cell "This is a note"
  `);
});
