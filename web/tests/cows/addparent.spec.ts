import { test, expect } from "@playwright/test";
import { createCow, deleteCow, VALID_SEXES } from "./cow_helper";
test("Test add dam", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createCow(
    page,
    "Parentlesscow1" + testId,
    "Test User",
    VALID_SEXES.STEER,
    ""
  );
  await createCow(page, "Mommacow1" + testId, "Test User", VALID_SEXES.COW, "");
  await createCow(page, "Papacow1" + testId, "Test User", VALID_SEXES.BULL, "");
  // Navigate
  await page.goto("http://localhost:31523/cows");
  await page.locator("#table-bottom").scrollIntoViewIfNeeded();
  await page.getByRole("cell", { name: "Parentlesscow1" + testId }).click();

  // Record parents
  await page.getByRole("button", { name: " Add Parent" }).click();
  await page.locator("#addParentForm").getByRole("searchbox").click();
  await expect(
    page.getByRole("option", { name: "Papacow" + testId })
  ).toHaveCount(0);
  await page.getByRole("option", { name: "Mommacow1" + testId }).click();
  await page.getByRole("button", { name: "Add Parent", exact: true }).click();
  await expect(page.locator("#app")).toContainText("Dam: Mommacow1" + testId);
  await page.getByRole("link", { name: "Dam: Momma" }).click();
  await page.getByRole("link", { name: "Offspring" }).click();
  await expect(page.getByRole("table")).toContainText(
    "Parentlesscow1" + testId
  );
  await deleteCow(page, "Parentlesscow1" + testId);
  await deleteCow(page, "Papacow1" + testId);
  await deleteCow(page, "Mommacow1" + testId);
});
test("Test add sire", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createCow(
    page,
    "Parentlesscow2" + testId,
    "Test User",
    VALID_SEXES.STEER,
    ""
  );
  await createCow(page, "Mommacow2" + testId, "Test User", VALID_SEXES.COW, "");
  await createCow(page, "Papacow2" + testId, "Test User", VALID_SEXES.BULL, "");
  // Navigate
  await page.goto("http://localhost:31523/cows");
  await page.locator("#table-bottom").scrollIntoViewIfNeeded();
  await page.getByRole("cell", { name: "Parentlesscow2" + testId }).click();

  // Record parents
  await page.getByRole("button", { name: " Add Parent" }).click();
  await page.getByText("Sire").click();
  await page.locator("#addParentForm").getByRole("searchbox").click();
  await page.getByRole("option", { name: "Papacow2" + testId }).click();
  await expect(
    page.getByRole("option", { name: "Mommacow2" + testId })
  ).toHaveCount(0);
  await page.getByRole("button", { name: "Add Parent", exact: true }).click();
  await expect(page.locator("#app")).toContainText("Sire: Papacow2" + testId);
  await page.getByRole("link", { name: "Sire: Papa" }).click();
  await page.getByRole("link", { name: "Offspring" }).click();
  await expect(page.getByRole("table")).toContainText(
    "Parentlesscow2" + testId
  );
  await deleteCow(page, "Parentlesscow2" + testId);
  await deleteCow(page, "Papacow2" + testId);
  await deleteCow(page, "Mommacow2" + testId);
});
