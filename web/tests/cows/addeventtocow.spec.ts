import { test, expect } from "@playwright/test";
import { createCow, deleteCow, openCowPage, VALID_SEXES } from "./cow_helper";
test("Test change sex", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createCow(
    page,
    "Eventful Cow" + testId,
    "Test User",
    VALID_SEXES.COW,
    ""
  );
  // Navigate
  await openCowPage(page, "Eventful Cow" + testId);
  await page.getByRole("link", { name: "Events" }).nth(1).click();
  await page.getByRole("button", { name: " Add Event" }).click();
  await page.locator('input[name="date"]').fill("2024-12-05");
  await page.locator('input[name="name"]').click();
  await page.locator('input[name="name"]').fill("Eventful event" + testId);
  await page.locator('input[name="name"]').press("Tab");
  await page
    .locator('textarea[name="description"]')
    .fill("Description of the eventful event");
  await page.getByRole("button", { name: "Add", exact: true }).click();
  await expect(page.getByRole("table")).toContainText("2024-12-05");
  await expect(page.getByRole("table")).toContainText(
    "Eventful event" + testId
  );
  await expect(page.getByRole("table")).toContainText(
    "Description of the eventful event"
  );
  await page.getByRole("cell", { name: "2024-12-05" }).click();
  await expect(page.getByRole("heading")).toContainText(
    "Eventful event" + testId
  );
  await expect(page.locator("#app")).toContainText("Date: 2024-12-05");
  await expect(page.locator("#app")).toContainText(
    "Description: Description of the eventful event"
  );
  await expect(page.getByRole("table")).toMatchAriaSnapshot(`
    - table:
      - rowgroup:
        - row "Tag ▴▾ Born ▴▾ Owner ▴▾ Sex ▴▾ Sire ▴▾ Dam ▴▾ Calves ▴▾ Notes ▴▾":
          - cell:
            - checkbox: "on"
          - cell "Tag ▴▾"
          - cell "Born ▴▾"
          - cell "Owner ▴▾"
          - cell "Sex ▴▾"
          - cell "Sire ▴▾"
          - cell "Dam ▴▾"
          - cell "Calves ▴▾"
          - cell "Notes ▴▾"
      - rowgroup:
        - row "Eventful Cow${testId} Test User Cow":
          - cell:
            - checkbox: "on"
          - cell "Eventful Cow${testId}"
          - cell
          - cell "Test User"
          - cell "Cow"
          - cell
          - cell
          - cell
          - cell
      - rowgroup
  `);
  await page.getByRole("button", { name: " Delete Event" }).click();
  await deleteCow(page, "Eventful Cow" + testId);
});
