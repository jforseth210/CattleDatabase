import { test, expect } from "@playwright/test";
import { createCow, deleteCow, openCowPage, VALID_SEXES } from "./cow_helper";
import { locateTableRow } from "../helper";
test("Test change tag number", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createCow(page, "Coow" + testId, "Test User", VALID_SEXES.BULL, "");
  // Navigate
  await openCowPage(page, "Coow" + testId);
  await page.getByRole("button", { name: " Change Tag Number" }).click();
  await page.getByRole("textbox").fill("Cow" + testId);
  await page.getByRole("button", { name: "Update" }).click();

  // Record archive
  await expect(page.getByRole("heading")).toContainText("Cow" + testId);
  await page.getByRole("link", { name: "Cows" }).click();
  await expect(locateTableRow(page, "Cow" + testId)).toMatchAriaSnapshot(`
    - row "Cow${testId} Test User Bull":
      - cell:
        - checkbox: "on"
      - cell "Cow${testId}"
      - cell
      - cell "Test User"
      - cell "Bull"
      - cell
      - cell
      - cell
      - cell
  `);
  await deleteCow(page, "Cow" + testId);
});
