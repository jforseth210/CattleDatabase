import { expect } from "@playwright/test";
import type { Page } from "@playwright/test";
export enum VALID_SEXES {
  AI_BULL = "Bull (AI)",
  BULL = "Bull",
  STEER = "Steer",
  COW = "Cow",
  HEIFER = "Heifer",
  HEIFER_REPLACEMENT = "Heifer (Replacement)",
  HEIFER_MARKET = "Heifer (Market)",
  FREE_MARTIN = "Free-Martin",
}
export async function openCowPage(page: Page, tagNumber: string) {
  await page.goto("http://localhost:31523/cows/cow/" + encodeURI(tagNumber));
}
export async function createCow(
  page: Page,
  tagNumber: string,
  owner: string,
  sex: VALID_SEXES,
  notes: string
) {
  await page.goto("http://localhost:31523/cows/");
  await page.getByRole("button", { name: "Add Cow" }).click();
  await expect(page.locator("#newCowDam")).toHaveValue("");
  await expect(page.locator("#newCowSire")).toHaveValue("");
  await expect(page.locator("small")).toContainText(
    "Tag number can't be blank"
  );
  await page.locator("#newCowTagNumber").click();
  await page.locator("#newCowTagNumber").fill(tagNumber);
  await page.locator("#newCowTagNumber").press("Tab");
  await expect(page.locator("small")).toContainText(
    tagNumber + " is available."
  );
  await page.locator("#newCowOwner").click();
  await page.locator("#newCowOwner").fill(owner);
  await expect(page.locator("#newCowOwner")).toHaveValue(owner);
  await page.locator("#newCowTagNumber").click();
  await expect(page.locator("#newCowTagNumber")).toHaveValue(tagNumber);
  await page.locator("#newCowSex").selectOption(sex);
  await expect(page.locator("#newCowSex")).toHaveValue(sex);
  await page.locator("#newCowNotes").click();
  await page.locator("#newCowNotes").fill(notes);
  await expect(page.locator("#newCowNotes")).toHaveValue(notes);
  await page.getByRole("button", { name: "Add", exact: true }).click();
}

export async function deleteCow(page: Page, tagNumber: string) {
  /*
   Apparently, if you try to go to a deletion page a couple times
   in rapid succession in Chrome, it will fail. Hitting it a few time usually works.
  */
  let success = false;
  let i = 0;
  while (!success) {
    try {
      await openCowPage(page, tagNumber);
      success = true;
    } catch (e) {
      await page.waitForTimeout(50);
      if (i > 10) {
        throw e;
      }
    }
    i++;
  }
  await page.getByRole("button", { name: " Delete Cow" }).click();
  await page.getByRole("button", { name: "Delete", exact: true }).click();
}
