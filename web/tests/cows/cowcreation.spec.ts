import { test, expect } from "@playwright/test";
import { createCow, deleteCow, VALID_SEXES } from "./cow_helper";
import { locateTableRow } from "../helper";

test("cow creation via add cow button", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  createCow(
    page,
    "Test Cow" + testId,
    "Test User",
    VALID_SEXES.HEIFER_REPLACEMENT,
    "This is a note"
  );
  await page.locator("#table-bottom").scrollIntoViewIfNeeded();
  await expect(locateTableRow(page, "Test Cow" + testId)).toMatchAriaSnapshot(`
    - row "Test Cow${testId} Test User Heifer (Replacement) This is a note":
      - cell:
        - checkbox: "on"
      - cell "Test Cow${testId}"
      - cell
      - cell "Test User"
      - cell "Heifer (Replacement)"
      - cell
      - cell
      - cell
      - cell "This is a note"
  `);
  await deleteCow(page, "Test Cow" + testId);
});
