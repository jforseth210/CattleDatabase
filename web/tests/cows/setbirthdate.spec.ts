import { test, expect } from "@playwright/test";
import { createCow, deleteCow, openCowPage, VALID_SEXES } from "./cow_helper";
test("Test change sex", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createCow(
    page,
    "Birthday Bull" + testId,
    "Test User",
    VALID_SEXES.BULL,
    ""
  );
  // Navigate
  await openCowPage(page, "Birthday Bull" + testId);
  await page.getByRole("link", { name: "Events" }).nth(1).click();
  await expect(page.getByRole("table")).toMatchAriaSnapshot(`
    - table:
      - rowgroup:
        - row "Date ▴▾ Name ▴▾ Description ▴▾":
          - cell:
            - checkbox: "on"
          - cell "Date ▴▾"
          - cell "Name ▴▾"
          - cell "Description ▴▾"
      - rowgroup
      - rowgroup
  `);
  await page.getByRole("button", { name: " Set Birthdate" }).click();
  await page.getByRole("textbox").fill("2024-12-20");
  await page
    .getByRole("button", { name: "Set Birthdate", exact: true })
    .click();
  await expect(page.locator("#app")).toContainText("Born: 2024-12-20");
  await page.getByRole("link", { name: "Events" }).nth(1).click();
  await expect(page.getByRole("table")).toMatchAriaSnapshot(`
    - table:
      - rowgroup:
        - row "Date ▴▾ Name ▴▾ Description ▴▾":
          - cell:
            - checkbox: "on"
          - cell "Date ▴▾"
          - cell "Name ▴▾"
          - cell "Description ▴▾"
      - rowgroup:
        - row /\\d+-\\d+-\\d+ Born Birthday Bull${testId} was born/:
          - cell:
            - checkbox: "on"
          - cell /\\d+-\\d+-\\d+/
          - cell "Born"
          - cell "Birthday Bull${testId} was born"
      - rowgroup
  `);
  // Record archive
  await deleteCow(page, "Birthday Bull" + testId);
});
