import type { Page } from "@playwright/test";

export function locateTableRow(page: Page, name: string, exclude?: string) {
  if (exclude != null) {
    return page
      .locator("tr")
      .filter({ hasText: name })
      .filter({ hasNotText: exclude });
  }
  return page.locator("tr").filter({ hasText: name });
}
