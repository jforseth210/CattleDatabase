import { test, expect } from "@playwright/test";
import { createEvent, deleteEvent } from "./event_helper";

test("event creation via add event button", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createEvent(
    page,
    "2024-12-05",
    "Test Event" + testId,
    "This is a test event"
  );
  /*await expect(
    page.locator("tr").filter({ hasText: "Test Cow" + testId })
  ).toMatchAriaSnapshot("");*/
  await deleteEvent(page, "Test Event" + testId);
});
