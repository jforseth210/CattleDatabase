import { test, expect } from "@playwright/test";
import { createEvent, deleteEvent } from "./event_helper";
import { locateTableRow } from "../helper";

test("event creation via add event button", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createEvent(
    page,
    "2024-12-05",
    "Test Event" + testId,
    "This is a test event"
  );

  await page.goto("http://localhost:31523/events/");
  await page.locator("#table-bottom").scrollIntoViewIfNeeded();
  await expect(locateTableRow(page, "Test Event" + testId)).toHaveCount(1);

  await deleteEvent(page, "Test Event" + testId);

  await page.goto("http://localhost:31523/events/");
  await page.locator("#table-bottom").scrollIntoViewIfNeeded();
  await expect(locateTableRow(page, "Test Event" + testId)).toHaveCount(0);
});
