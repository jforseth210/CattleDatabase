import { test, expect } from "@playwright/test";
import { createEvent, deleteEvent, openEventPage } from "./event_helper";
test("Test change date", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createEvent(
    page,
    "0223-03-01",
    "Event with an incorrect date" + testId,
    "Oops"
  );
  // Navigate
  await openEventPage(page, "Event with an incorrect date" + testId);
  await page.getByText("Date: 0223-03-01").click();
  await expect(page.locator("#app")).toContainText("Date: 0223-03-01");
  await page.getByRole("button", { name: " Change Date" }).click();
  await expect(page.getByRole("textbox")).toHaveValue("0223-03-01");
  await page.getByRole("textbox").click();
  await page.getByRole("textbox").fill("2023-03-01");
  await page.getByRole("button", { name: "Change Date", exact: true }).click();
  await expect(page.locator("#app")).toContainText("Date: 2023-03-01");

  await deleteEvent(page, "Event with an incorrect date" + testId);
});
