import { test, expect } from "@playwright/test";
import { createEvent, deleteEvent, openEventPage } from "./event_helper";
import { locateTableRow } from "../helper";
test("Test archiving", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createEvent(
    page,
    "2024-12-26",
    "Event to archive" + testId,
    "This is a test event"
  );
  // Navigate
  openEventPage(page, "Event to archive" + testId);

  // Record archive
  await page.getByRole("link", { name: " Archive" }).click();

  // Check archive
  await page.getByRole("link", { name: "Events" }).click();
  await expect(locateTableRow(page, "Event to archive" + testId)).toHaveCount(
    0
  );
  await page.getByLabel("Show Archived Events").check();
  await page.locator("#table-bottom").scrollIntoViewIfNeeded();
  await expect(locateTableRow(page, "Event to archive" + testId))
    .toMatchAriaSnapshot(`
    - row /\\d+-\\d+-\\d+ Event to archive${testId} This is a test event \\$\\d+\\.\\d+/:
      - cell:
        - checkbox: "on"
      - cell /\\d+-\\d+-\\d+/
      - cell "Event to archive${testId}"
      - cell "This is a test event"
      - cell
      - cell
      - cell /\\$\\d+\\.\\d+/
  `);

  // Navigate
  await page.getByRole("cell", { name: "Event to archive" + testId }).click();
  await expect(page.locator("#app")).toContainText("Archived: Yes");
  await deleteEvent(page, "Event to archive" + testId);
});
