import { test, expect } from "@playwright/test";
import { createCow, deleteCow, VALID_SEXES } from "../cows/cow_helper";
import { createEvent, deleteEvent, openEventPage } from "./event_helper";
test("Test cow page exists and looks right", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  const event_name = "Test event" + testId;
  await createEvent(page, "2024-12-05", event_name, "This is a note");
  await openEventPage(page, event_name);
  await expect(page.locator("html")).toMatchAriaSnapshot(`
    - document:
      - navigation:
        - link "CattleDB"
        - list:
          - listitem:
            - link "Cows"
          - listitem:
            - link "Events"
          - listitem:
            - link "Transactions"
          - listitem:
            - link "Calendar"
        - list:
          - searchbox "Search"
          - button "Search"
          - listitem:
            - button "testuser"
      - heading "${event_name}" [level=2]
      - text: "/System Id: #\\\\d+/"
      - list:
        - listitem: "/Date: \\\\d+-\\\\d+-\\\\d+/"
        - listitem: "Archived: No"
        - listitem: "Description: This is a note"
      - button " Change Description"
      - button " Change Name"
      - button " Change Date"
      - link " Archive"
      - button " Delete Event"
      - list:
        - listitem:
          - link "Cows"
        - listitem:
          - link "Transactions"
      - tabpanel:
        - button " Add/Remove Cows"
        - button " Copy Cows from Another Event"
        - table:
          - rowgroup:
            - row "Tag ▴▾ Born ▴▾ Owner ▴▾ Sex ▴▾ Sire ▴▾ Dam ▴▾ Calves ▴▾ Notes ▴▾":
              - cell:
                - checkbox: "on"
              - cell "Tag ▴▾"
              - cell "Born ▴▾"
              - cell "Owner ▴▾"
              - cell "Sex ▴▾"
              - cell "Sire ▴▾"
              - cell "Dam ▴▾"
              - cell "Calves ▴▾"
              - cell "Notes ▴▾"
          - rowgroup
          - rowgroup
  `);
  await page.getByRole("link", { name: "Transactions" }).nth(1).click();
  await expect(page.locator("html")).toMatchAriaSnapshot(`
    - document:
      - navigation:
        - link "CattleDB"
        - list:
          - listitem:
            - link "Cows"
          - listitem:
            - link "Events"
          - listitem:
            - link "Transactions"
          - listitem:
            - link "Calendar"
        - list:
          - searchbox "Search"
          - button "Search"
          - listitem:
            - button "testuser"
      - heading "${event_name}" [level=2]
      - text: "/System Id: #\\\\d+/"
      - list:
        - listitem: "/Date: \\\\d+-\\\\d+-\\\\d+/"
        - listitem: "Archived: No"
        - listitem: "Description: This is a note"
      - button " Change Description"
      - button " Change Name"
      - button " Change Date"
      - link " Archive"
      - button " Delete Event"
      - list:
        - listitem:
          - link "Cows"
        - listitem:
          - link "Transactions"
      - tabpanel /Add Transaction To\\/From ▴▾ Name ▴▾ Description ▴▾ Price ▴▾ Total \\$\\d+\\.\\d+/:
        - button "Add Transaction"
        - table:
          - rowgroup:
            - row "To/From ▴▾ Name ▴▾ Description ▴▾ Price ▴▾":
              - cell:
                - checkbox: "on"
              - cell "To/From ▴▾"
              - cell "Name ▴▾"
              - cell "Description ▴▾"
              - cell "Price ▴▾"
          - rowgroup
          - rowgroup:
            - row /Total \\$\\d+\\.\\d+/:
              - cell "Total"
              - cell
              - cell
              - cell
              - cell /\\$\\d+\\.\\d+/
  `);
  await deleteEvent(page, event_name);
});
