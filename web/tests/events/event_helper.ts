import type { Page } from "@playwright/test";
export async function openEventPage(page: Page, event_name: string) {
  await page.goto("http://localhost:31523/events/");
  await page.getByLabel("Show Archived Events").check();
  await page.locator("#table-bottom").scrollIntoViewIfNeeded();
  await page.getByRole("cell", { name: event_name }).click();
}
export async function createEvent(
  page: Page,
  date: string,
  name: string,
  description: string
) {
  await page.goto("http://localhost:31523/events/");
  await page.getByRole("button", { name: "Add Event" }).click();
  await page.locator('input[name="date"]').fill(date);
  await page.locator('input[name="name"]').click();
  await page.locator('input[name="name"]').fill(name);
  await page.locator('input[name="name"]').press("Tab");
  await page.locator('textarea[name="description"]').fill(description);
  await page.getByRole("button", { name: "Add", exact: true }).click();
}

export async function deleteEvent(page: Page, event_name: string) {
  await openEventPage(page, event_name);
  await page.getByRole("button", { name: " Delete Event" }).click();
}
