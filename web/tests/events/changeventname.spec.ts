import { test, expect } from "@playwright/test";
import { createEvent, deleteEvent, openEventPage } from "./event_helper";
import { locateTableRow } from "../helper";
test("Test change tag number", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createEvent(
    page,
    "2023-03-01",
    "An eventful event" + testId,
    "A descriptive description"
  );
  // Navigate
  await openEventPage(page, "An eventful event" + testId);
  await page.getByRole("button", { name: " Change Name" }).click();
  await page.getByRole("textbox").fill("Event with a new name" + testId);
  await page.getByRole("button", { name: "Change Name", exact: true }).click();

  // Record archive
  await expect(page.getByRole("heading")).toContainText(
    "Event with a new name" + testId
  );
  await page.getByRole("link", { name: "Events" }).click();
  await expect(locateTableRow(page, "Event with a new name" + testId))
    .toMatchAriaSnapshot(`
    - row /\\d+-\\d+-\\d+ Event with a new name${testId} A descriptive description \\$\\d+\\.\\d+/:
      - cell:
        - checkbox: "on"
      - cell /\\d+-\\d+-\\d+/
      - cell "Event with a new name${testId}"
      - cell "A descriptive description"
      - cell
      - cell
      - cell /\\$\\d+\\.\\d+/
  `);
  await deleteEvent(page, "Event with a new name" + testId);
});
