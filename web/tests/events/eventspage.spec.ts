import { test, expect } from "@playwright/test";
import { createEvent } from "./event_helper";
import { locateTableRow } from "../helper";

test("everything exists", async ({ page }) => {
  await page.goto("http://localhost:31523/events/");
  await expect(page.getByPlaceholder("Search")).toBeEmpty();
  await expect(page.getByPlaceholder("Search")).toBeEmpty();
  await expect(page.getByRole("navigation")).toMatchAriaSnapshot(`
    - navigation:
      - link "CattleDB"
      - list:
        - listitem:
          - link "Cows"
        - listitem:
          - link "Events"
        - listitem:
          - link "Transactions"
        - listitem:
          - link "Calendar"
      - list:
        - searchbox "Search"
        - button "Search"
        - listitem:
          - button "testuser"
    `);
  await expect(page.getByPlaceholder("Search")).toBeEmpty();

  await expect(page.locator("#app")).toContainText("Add Event");
  await expect(page.locator("#app")).toContainText("Show Archived Events");
  await expect(page.locator("thead")).toContainText("Date");
  await expect(page.locator("thead")).toContainText("Name");
  await expect(page.locator("thead")).toContainText("Description");
  await expect(page.locator("thead")).toContainText("Cows");
  await expect(page.locator("thead")).toContainText("Transactions");
  await expect(page.locator("thead")).toContainText("Price");
});
test("everything exists with event", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createEvent(
    page,
    "2024-12-05",
    "Test Event" + testId,
    "This is a test event"
  );
  await page.goto("http://localhost:31523/events/");
  await expect(page.getByPlaceholder("Search")).toBeEmpty();
  await expect(page.getByRole("navigation")).toMatchAriaSnapshot(`
    - navigation:
      - link "CattleDB"
      - list:
        - listitem:
          - link "Cows"
        - listitem:
          - link "Events"
        - listitem:
          - link "Transactions"
        - listitem:
          - link "Calendar"
      - list:
        - searchbox "Search"
        - button "Search"
        - listitem:
          - button "testuser"
    `);
  await expect(page.getByPlaceholder("Search")).toBeEmpty();

  await expect(page.locator("#app")).toContainText("Add Event");
  await expect(page.locator("#app")).toContainText("Show Archived Events");
  await expect(page.locator("thead")).toContainText("Date");
  await expect(page.locator("thead")).toContainText("Name");
  await expect(page.locator("thead")).toContainText("Description");
  await expect(page.locator("thead")).toContainText("Cows");
  await expect(page.locator("thead")).toContainText("Transactions");
  await expect(page.locator("thead")).toContainText("Price");
  await expect(locateTableRow(page, "Test Event" + testId))
    .toMatchAriaSnapshot(`
    - row /\\d+-\\d+-\\d+ Test Event${testId} This is a test event \\$\\d+\\.\\d+/:
      - cell:
        - checkbox: "on"
      - cell "2024-12-05"
      - cell "Test Event${testId}"
      - cell "This is a test event"
      - cell
      - cell
      - cell "$0.00"
  `);
});
