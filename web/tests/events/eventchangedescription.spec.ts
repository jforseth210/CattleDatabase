import { test, expect } from "@playwright/test";
import { createEvent, deleteEvent, openEventPage } from "./event_helper";
test("Test change tag number", async ({ page }) => {
  const testId = `${Date.now()}_${Math.random().toString(36).slice(2, 11)}`;
  await createEvent(
    page,
    "2023-03-01",
    "Noteworthy Event" + testId,
    "See, look at all of these eventful notes!"
  );
  // Navigate
  await openEventPage(page, "Noteworthy Event" + testId);
  await page.getByText("Description: See, look at all").click();
  await expect(page.locator("#app")).toContainText(
    "Description: See, look at all of these eventful notes!"
  );
  await page.getByRole("button", { name: " Change Description" }).click();
  await expect(page.getByRole("textbox")).toHaveValue(
    "See, look at all of these eventful notes!"
  );
  await page.getByRole("textbox").click();
  await page
    .getByRole("textbox")
    .fill(
      "In fact, I'm going to take EVEN MORE notes, because this event is just so noteworthy"
    );
  await page
    .getByRole("button", { name: "Change Description", exact: true })
    .click();
  await expect(page.locator("#app")).toContainText(
    "In fact, I'm going to take EVEN MORE notes, because this event is just so noteworthy"
  );

  await deleteEvent(page, "Noteworthy Event" + testId);
});
