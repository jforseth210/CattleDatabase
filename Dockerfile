# syntax=docker/dockerfile:1

# Stage 1: Build the Node.js app
FROM node:18 AS build-stage

# Set the working directory for the Node.js app
WORKDIR /web

# Copy package.json and package-lock.json (if available)
COPY web/package*.json ./

# Install Node.js dependencies
RUN npm install --legacy-peer-deps

# Copy the rest of the Node.js app source code
COPY web/ ./

# Build the Node.js app
RUN npm run build

# Stage 2: Build the Python app
FROM python:3.12-slim

# Set the working directory for the Python app
WORKDIR /python-docker

# Copy and install Python dependencies
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

# Copy the built files from the build-stage
COPY --from=build-stage /static/dist ./static/dist

# Copy the rest of the Python application code
COPY . .
RUN rm -rf web

# Set the command to run the Python application
CMD [ "python3", "app.py" ]
