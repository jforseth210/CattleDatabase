"""
These are the models that interact with the database.
"""
import logging

from flask_sqlalchemy import SQLAlchemy
logger = logging.getLogger(__name__)
db = SQLAlchemy()



# The "in-between" tables that facilitate many-to-many
# relationships in the database.


# cows-events
calendar = db.Table(
    "calendar",
    db.Column("cow_id", db.Integer, db.ForeignKey("cow.cow_id")),
    db.Column("event_id", db.Integer, db.ForeignKey("event.event_id")),
)

# cows-transactions
transaction_cows = db.Table(
    "transaction_cows",
    db.Column("cow_id", db.Integer, db.ForeignKey("cow.cow_id")),
    db.Column(
        "transaction_id", db.Integer, db.ForeignKey(
            "transaction.transaction_id")
    ),
)
# events-transactions isn't needed since it's a one-to-many relationship.
# Each transaction has only one event.