import logging

from models.database import db
from models.search_result import SearchResult

logger = logging.getLogger(__name__)


class Event(db.Model):
    """
    The system representation of an event.
    """

    event_id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.String(10))
    name = db.Column(db.String(100))
    description = db.Column(db.Text)
    isArchived = db.Column(db.Boolean, default=False)

    def to_search_result(self):
        """
        Using this object's attributes, create a SearchResult object to display.
        """
        return SearchResult(
            title=self.name,
            body=str(self),
            url=f"/events/event/{self.event_id}",
            id=self.event_id,
            type="event",
            isArchived=self.isArchived
        )

    def __str__(self):
        return f"Event on {self.date}: {self.name} - {self.description}"

    def serialize(self, items):
        return serialize_event(self, items)


def serialize_event(event, items):
    """
    Convert object to JSON given a list of attributes.
    ["event_id","name"]

    Relationships can be handled with dictionaries. The value MUST BE A LIST
    [{"cows":["tag_number"]}]
    """
    return_dict = {}
    for item in items:
        if item == "event_id":
            return_dict[item] = event.event_id
        elif item == "name":
            return_dict[item] = event.name
        elif item == "date":
            return_dict[item] = event.date
        elif item == "description":
            return_dict[item] = event.description
        elif item == "isArchived":
            return_dict[item] = event.isArchived
        elif "cows" in item:
            return_dict["cows"] = [cow.serialize(
                item["cows"]) for cow in event.cows]
        elif "transactions" in item:
            return_dict["transactions"] = [
                transaction.serialize(item["transactions"])
                for transaction in event.transactions
            ]
        else:
            error = ValueError(
                "Invalid attribute for serialization: " + item)
            logger.error(
                "Invalid attribute for serialization: " + item, exc_info=error)
            raise error
    return return_dict
