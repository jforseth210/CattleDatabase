import re

from models.database import db
import logging


logger = logging.getLogger(__name__)


class User(db.Model):
    """
    The system representation of a user.
    """
    __tablename__ = "user"
    user_id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255), unique=True)
    hashed_password = db.Column(db.String(255))

    def __str__(self):
        return self.username
