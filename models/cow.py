import re

from models.database import db, calendar
from helper.sexes import get_sexes
import logging

from models.search_result import SearchResult

logger = logging.getLogger(__name__)


class Cow(db.Model):
    """
    The system representation of a cow.
    """

    cow_id = db.Column(db.Integer, primary_key=True)
    tag_number = db.Column(db.String(8))
    owner = db.Column(db.String(255))
    sex = db.Column(db.String(255))
    events = db.relationship(
        "Event", secondary=calendar, backref=db.backref("cows", lazy="dynamic")
    )

    dam_id = db.Column(db.Integer, db.ForeignKey("cow.cow_id"))
    sire_id = db.Column(db.Integer, db.ForeignKey("cow.cow_id"))
    description = db.Column(db.Text, default="")
    isArchived = db.Column(db.Boolean, default=False)

    def get_dam(self):
        """
        Gets the cow's dam if it exists.
        """
        return Cow.query.filter_by(cow_id=self.dam_id).first()

    def get_sire(self):
        """
        Gets the cow's sire if it exists.
        """
        return Cow.query.filter_by(cow_id=self.sire_id).first()

    # Find calves with a dam_id or sire_id matching this cow
    def get_calves(self):
        """
        Retrieves all of the cow's calves.
        """
        if self.sex in get_sexes("female"):
            return Cow.query.filter_by(dam_id=self.cow_id)

        if self.sex in get_sexes("male"):
            return Cow.query.filter_by(sire_id=self.cow_id)

    def get_transactions(self):
        """
        Go through this cow's events, and find any attached transactions
        """
        transactions = []
        for event in self.events:
            for transaction in event.transactions:
                if transaction.cows and self in transaction.cows:
                    transactions.append(transaction)
        return transactions

    def get_transaction_total(self):
        """
        Totals the prices of all of the cow's transactions and does some
        very sketchy floating point rounding.
        """
        return (
            int(sum(transaction.price for transaction in self.get_transactions()) * 100)
            / 100
        )

    def get_birthdate(self):
        """
        Get the date of the "Born" event (if applicable).
        If multiple "Born" events exist (they shouldn't),
        the first (by creation) will be used.
        """
        events = self.events
        for event in events:
            if event.name == "Born":
                return event.date
        logger.debug("No birthdate")
        return None

    def get_deathdate(self):
        """
        Get the date of the "Died" event (if applicable).
        If multiple "Died" events exist (they shouldn't),
        the first (by creation) will be used.
        """
        events = self.events
        for event in events:
            if event.name == "Died":
                return event.date
        logger.debug("No deathdate")
        return None

    def get_cause_of_death(self):
        """
        Get the date of the "Died" event (if applicable).
        If multiple "Died" events exist (they shouldn't),
        the first (by creation) will be used.
        """
        events = self.events
        for event in events:
            if event.name == "Died":
                return event.description.split("\n")[1]
        return None

    def to_search_result(self):
        """
        Using this object's attributes, create a SearchResult object to display.
        """
        return SearchResult(
            title=self.tag_number,
            body=str(self),
            url=f"/cows/cow/{self.tag_number}",
            id=self.tag_number,
            type="cow",
            isArchived=self.isArchived
        )

    def get_first_digit_of_tag(self):
        """
        On our ranch, the first digit of an eartag indicates year.

        This fetches the first numeric digit in the tag, even if
        there are alpha characters preceeding it.
        """
        # Regex is used to remove alphabetical prefixes, like owner initials
        first_digit = re.search(r"\d", self.tag_number)
        first_digit = first_digit.group()[0] if first_digit else "N/A"
        return first_digit

    def __str__(self):
        return (
            f"{self.sex} with tag {self.tag_number} owned by {self.owner}. "
            f"Notes: {self.description}"
        )

    def __eq__(self, other):
        return other is not None and self.cow_id == other.cow_id and self.tag_number == other.tag_number

    def serialize(self, requested_fields):
        return serialize_cow(self, requested_fields)


def serialize_cow(cow, requested_fields):
    """
    Convert object to JSON given a list of attributes.
    ["cow_id","tag_number"]

    Relationships can be handled with dictionaries. The value MUST BE A LIST
    [{"dam":["tag_number"]}]
    """
    return_dict = {}
    for field in requested_fields:
        if field == "cow_id":
            return_dict[field] = cow.cow_id
        elif field == "tag_number":
            return_dict[field] = cow.tag_number
        elif field == "sex":
            return_dict[field] = cow.sex
        elif field == "isArchived":
            return_dict[field] = cow.isArchived
        elif field == "birthdate":
            return_dict[field] = cow.get_birthdate()
        elif field == "deathdate":
            return_dict[field] = cow.get_deathdate()
        elif field == "causeOfDeath":
            return_dict[field] = cow.get_cause_of_death()
        elif field == "owner":
            return_dict[field] = cow.owner
        elif field == "description":
            return_dict[field] = cow.description

        elif "dam" in field:
            dam = cow.get_dam()
            if dam:
                return_dict["dam"] = dam.serialize(field["dam"])
            else:
                return_dict["dam"] = None
        elif "sire" in field:
            sire = cow.get_sire()
            if sire:
                return_dict["sire"] = sire.serialize(field["sire"])
            else:
                return_dict["sire"] = None
        elif "calves" in field:
            return_dict["calves"] = [
                calf.serialize(field["calves"]) for calf in cow.get_calves()
            ]
        elif "events" in field:
            return_dict["events"] = [
                event.serialize(field["events"]) for event in cow.events
            ]
        elif "transactions" in field:
            return_dict["transactions"] = [
                transaction.serialize(field["transactions"])
                for transaction in cow.get_transactions()
            ]
        else:
            error = ValueError(
                "Invalid attribute for serialization: " + field)
            logger.error(
                "Invalid attribute for serialization: " + field, exc_info=error)
            raise error
    return return_dict


def get_cow_from_tag(tag):
    """
    Given a tag_number string, returns the cow object
    """
    return Cow.query.filter_by(tag_number=tag).first()
