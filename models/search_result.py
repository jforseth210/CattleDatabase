class SearchResult:
    """
    The results from a search
    """

    def __init__(self, title, body, url, id, isArchived, type):
        self.title = title
        self.body = body
        self.url = url
        self.id = id
        self.isArchived = isArchived
        self.type = type

    def to_dict(self):
        """
        Convert a Search Object to an ordinary dictionary
        """
        return {
            "title": self.title,
            "body": self.body,
            "url": self.url,
            "id": self.id,
            "type": self.type,
            "isArchived": self.isArchived
        }
