import logging

from models.database import db, transaction_cows
from models.search_result import SearchResult

logger = logging.getLogger(__name__)


class Transaction(db.Model):
    """
    The system representation of a transaction.
    """

    transaction_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    price = db.Column(db.Float)
    tofrom = db.Column(db.String(255))
    description = db.Column(db.Text)
    isArchived = db.Column(db.Boolean, default=False)

    event_id = db.Column(
        "event_id", db.Integer, db.ForeignKey("event.event_id"), nullable=False
    )

    event = db.relationship(
        "Event", backref=db.backref("transactions", lazy=True))

    # A transaction can have a different cow list from its parent event
    cows = db.relationship(
        "Cow", secondary=transaction_cows, backref=db.backref("cows", lazy="dynamic")
    )

    def get_date(self):
        """
        Get the date from parent event
        """
        return self.event.date

    def get_formatted_price(self):
        """
        Convert price from formatted price with $ and commas and decimal points and two digits.
        """
        return f"${self.price:,.2f}"

    def get_formatted_total(self):
        """
        The price, multiplied by the number of cows it applies to.
        """
        return f"${self.price * len(self.cows):,.2f}"

    def to_search_result(self):
        """
        Using this object's attributes, create a SearchResult object to display.
        """
        return SearchResult(
            title=self.name,
            body=str(self),
            url=f"/transactions/transaction/{self.transaction_id}",
            id=self.transaction_id,
            type="transaction",
            isArchived=self.isArchived

        )

    def __str__(self):
        if self.price > 0:
            return (
                f"Transaction: {self.get_formatted_price()} from "
                f"{self.tofrom} for {self.name} -{self.description}"
            )
        return (
            f"Transaction: {self.get_formatted_price()} to "
            f"{self.tofrom} for {self.name} -{self.description}"
        )

    def __eq__(self, other):
        return self.transaction_id == other.transaction_id and self.name == other.name

    def serialize(self, requested_fields):
        return serialize_transaction(self, requested_fields)


def serialize_transaction(transaction, requested_fields):
    """
    Convert object to JSON given a list of attributes.
    ["transaction_id","name"]

    Relationships can be handled with dictionaries. The value MUST BE A LIST
    [{"event":["name"]}]
    """
    return_dict = {}
    for field in requested_fields:
        if "transaction_id" in field:
            return_dict["transaction_id"] = transaction.transaction_id
        elif "event" in field:
            return_dict["event"] = transaction.event.serialize(field["event"])
        elif "date" in field:
            return_dict["date"] = transaction.event.date
        elif "cows" in field:
            return_dict["cows"] = [cow.serialize(
                field["cows"]) for cow in transaction.cows]
        elif field == "isArchived":
            return_dict[field] = transaction.isArchived
        elif field == "tofrom":
            return_dict[field] = transaction.tofrom
        elif field == "name":
            return_dict[field] = transaction.name
        elif field == "description":
            return_dict[field] = transaction.description
        elif field == "price":
            return_dict[field] = transaction.price
        else:
            error = ValueError(
                "Invalid attribute for serialization: " + field)
            logger.error(
                "Invalid attribute for serialization: " + field, exc_info=error)
            raise error
    return return_dict
