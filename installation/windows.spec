# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(
    ['C:/Users/Forseth/Documents/Code/CattleDB/host_software/main.py'],
    pathex=['C:/Users/Forseth/Documents/Code/CattleDB/host_software/','C:/Users/Forseth/Documents/Code/CattleDB/host_software/venv/Lib/site-packages'],
    binaries=[],
    datas=[
        ('C:/Users/Forseth/Documents/Code/CattleDB/host_software/static', 'static/'), 
        ('C:/Users/Forseth/Documents/Code/CattleDB/host_software/templates', 'templates/'),
        ('C:/Users/Forseth/Documents/Code/CattleDB/host_software/venv/Lib/site-packages/ics/grammar/contentline.ebnf', 'ics/grammar'),
        ('C:/Users/Forseth/Documents/Code/CattleDB/host_software/venv/Lib/site-packages/flask_inertia/router.js', 'flask_inertia')
    ],
    hiddenimports=['sqlite3'],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name='main',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=True,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
    icon='C:\\Users\\Forseth\\Documents\\Code\\CattleDB\\host_software\\icon.ico',
)
coll = COLLECT(
    exe,
    a.binaries,
    a.zipfiles,
    a.datas,
    strip=False,
    upx=True,
    upx_exclude=[],
    name='main',
)
