:: Make sure venv is active!
cd C:\Users\Forseth\Documents\Code\CattleDB\host_software
git pull
CALL .\venv\Scripts\activate.bat
CALL pip install -r requirements.txt
cd web
CALL npm i
CALL npm run build:dev
cd ..\installation
CALL pyinstaller --noconfirm windows.spec 
START /b /wait C:\"Program Files (x86)"\NSIS\makensis.exe "CattleDB.nsi"