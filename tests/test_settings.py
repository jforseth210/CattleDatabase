"""
Module for testing the settings module
"""
import json
import subprocess
import pytest
from werkzeug.security import check_password_hash


class TestSettingsPage:
    """
    Tests to make sure the page itself shows correctly
    """

    def test_settings_page(self, client, mocker, testing_objects):
        """
        Make sure the settings page loads correctly under normal conditions.
        """
        mocker.patch("routes.web.settings.get_usernames", return_value=["Keith Rivers", "Sophia Olsen"])
        mocker.patch("routes.web.settings.get_network_ssid",
                     return_value="NETWORKNAME")

        response = client.get("/settings/")

        assert response.status_code == 200
        assert b"Settings" in response.data
        assert b"Keith Rivers" in response.data
        assert b"Sophia Olsen" in response.data
        assert b"NETWORKNAME" in response.data
        assert b"THISISAPASSWORD" not in response.data

    def test_settings_page_with_missing_config(self, client, mocker):
        """
        Make sure the settings page loads when the config is missing.
        The settings page should work, even if the configuration and database are missing.
        """
        mock_open = mocker.MagicMock()
        mock_open.return_value.__enter__.side_effect = FileNotFoundError
        mocker.patch("routes.web.settings.open", mock_open)
        mocker.patch("routes.web.settings.get_network_ssid",
                     return_value="NETWORKNAME")

        response = client.get("/settings/")

        assert response.status_code == 200

    def test_settings_page_with_config_with_no_users(self, client, mocker, mock_empty_config):
        """
        Make sure the settings page loads even if the config exists but is empty.
        """
        mock_open, _ = mock_empty_config
        mocker.patch("routes.web.settings.open", mock_open)
        mocker.patch("routes.web.settings.get_network_ssid",
                     return_value="NETWORKNAME")

        response = client.get("/settings/")

        assert response.status_code == 200


class TestNewUser:
    """
    Tests the creation of a new user
    """
    @pytest.mark.slow
    @pytest.mark.skip(reason="No longer using the json file")
    def test_create_new_user(self, client, mocker, mock_config_no_users):
        """
        Make sure that creating a new user from the settings page works properly.
        """
        mock_open, mock_file = mock_config_no_users
        mocker.patch("helper.login_checker.open", mock_open)
        mocker.patch("routes.web.settings.open", mock_open)

        response = client.post("/settings/add_user", data={
            "username": "user",
            "password": "password",
        }, follow_redirects=True)

        # read() should be called twice because we're following redirects.
        # /settings/add_user, and /settings both read the config.
        mock_file.read.assert_called()
        mock_file.write.assert_called_once()
        written_string = str(
            mock_file.write.call_args.args[0])
        written_json = json.loads(written_string)

        assert written_json.get("users", False)
        assert check_password_hash(
            written_json["users"][0]["hashed_password"], "password")
        assert response.status_code == 200
        assert b"User user was added successfully." in response.data

    @pytest.mark.slow
    @pytest.mark.skip(reason="No longer using the json file")
    def test_create_new_user_with_missing_config(self, client, mocker):
        """
        If the user is logged in and the config file gets deleted, they
        should still be able to create a new user.
        """
        mock_open = mocker.MagicMock()
        mock_file = mocker.MagicMock()  # mock for file returned from open
        mock_open.return_value.__enter__.side_effect = [
            FileNotFoundError, mock_file, mock_file]
        mock_file.read.return_value = "{}"
        mocker.patch("helper.login_checker.open", mock_open)
        mocker.patch("routes.web.settings.open", mock_open)

        response = client.post("/settings/add_user", data={
            "username": "user",
            "password": "password",
        }, follow_redirects=True)
        mock_file.write.assert_called_once()
        written_string = str(
            mock_file.write.call_args.args[0])
        written_json = json.loads(written_string)
        assert written_json.get("users", False)
        assert check_password_hash(
            written_json["users"][0]["hashed_password"], "password")
        assert response.status_code == 200
        assert (
            b"The config.json file was missing. "
            b"A new config.json was created and user user was added successfully.") in response.data

    @pytest.mark.slow
    @pytest.mark.skip(reason="No longer using the json file")
    def test_create_new_user_with_config_with_no_users(self, client, mocker, mock_empty_config):
        """
        If the user is logged in and the config file gets deleted, they
        should still be able to create a new user.
        """
        mock_open, mock_file = mock_empty_config
        mocker.patch("helper.login_checker.open", mock_open)
        mocker.patch("routes.web.settings.open", mock_open)

        response = client.post("/settings/add_user", data={
            "username": "user",
            "password": "password",
        }, follow_redirects=True)
        mock_file.write.assert_called_once()
        written_string = str(
            mock_file.write.call_args.args[0])
        written_json = json.loads(written_string)
        assert written_json.get("users", False)
        assert check_password_hash(
            written_json["users"][0]["hashed_password"], "password")
        assert response.status_code == 200
        assert b"User user was added successfully." in response.data

    @pytest.mark.slow
    @pytest.mark.skip(reason="No longer using the json file")
    def test_create_existing_user(self, client, mocker, mock_config):
        """
        The user shouldn't be allowed to create a user that exists already.
        """
        mock_open, mock_file = mock_config
        mocker.patch("helper.login_checker.open", mock_open)
        mocker.patch("routes.web.settings.open", mock_open)

        response = client.post("/settings/add_user", data={
            "username": "Keith Rivers",
            "password": "doesntmatter",
        }, follow_redirects=True)

        mock_file.write.assert_not_called()
        assert response.status_code == 200
        assert (b"Username Keith Rivers is already taken. "
                b"Please choose a different username") in response.data


class TestChangeUsername:
    """
    Tests changing the username
    """

    @pytest.mark.skip(reason="No longer using the json file")
    def test_change_username_not_cow_ownership(self, mocker, client, mock_config, testing_objects):
        """
        Make sure that changing the username works if we don't worry about changing cow ownership.
        """
        mocker.patch("routes.web.settings.get_username",
                     return_value="Keith Rivers")
        mock_open, mock_file = mock_config
        mocker.patch("helper.login_checker.open", mock_open)
        mocker.patch("routes.web.settings.open", mock_open)
        mock_cow_object = mocker.patch("routes.web.settings.Cow")
        mock_cow_object.query.filter_by.return_value = [
            testing_objects["cows"]["567"]
        ]
        mocker.patch("routes.web.settings.db")

        response = client.post("/settings/change_username", data={
            "new_username": "NewUsername",
            "change_cow_ownership": "off",
        })

        mock_file.write.assert_called_once()
        written_string = str(mock_file.write.call_args.args[0])
        expected = json.loads(mock_file.read())
        expected["users"][0]["username"] = "NewUsername"
        assert written_string == json.dumps(expected, indent=4)

        assert testing_objects["cows"]["567"].owner == "Keith Rivers"
        mock_cow_object.query.filter_by.assert_not_called()
        assert response.status_code == 302
        assert "/logout" in response.location

    @pytest.mark.skip(reason="No longer using the json file")
    def test_change_username_and_cow_ownership(self, mocker, client, mock_config, testing_objects):
        """
        Make sure that changing the username also works if we decide to change
        the ownership of all our cows.

        So if Keith Rivers changes their username to NewKeith Rivers all cows owned by
        Keith Rivers should be updated to be owned by NewKeith Rivers
        """
        mocker.patch("routes.web.settings.get_username",
                     return_value="Keith Rivers")
        mock_open, mock_file = mock_config
        mocker.patch("helper.login_checker.open", mock_open)
        mocker.patch("routes.web.settings.open", mock_open)
        mock_cow_object = mocker.patch("routes.web.settings.Cow")
        mock_cow_object.query.filter_by.return_value = [
            testing_objects["cows"]["567"]
        ]
        mocker.patch("routes.web.settings.db")

        response = client.post("/settings/change_username", data={
            "new_username": "NewUsername",
            "change_cow_ownership": "on",
        })

        mock_file.write.assert_called_once()
        written_string = str(
            mock_file.write.call_args.args[0])
        expected = json.loads(mock_file.read())
        expected["users"][0]["username"] = "NewUsername"
        assert written_string == json.dumps(expected, indent=4)

        assert testing_objects["cows"]["567"].owner == "NewUsername"
        mock_cow_object.query.filter_by.assert_called_once()
        assert response.status_code == 302
        assert "/logout" in response.location

    @pytest.mark.skip(reason="No longer using the json file")
    def test_change_username_to_one_already_taken(self, client, mocker, mock_config, testing_objects):
        """
        When the user changes their username, the program shouldn't allow
        them to change it to an already existing username.
        """
        mocker.patch("routes.web.settings.get_username",
                     return_value="Keith Rivers")
        mock_open, mock_file = mock_config
        mocker.patch("helper.login_checker.open", mock_open)
        mocker.patch("routes.web.settings.open", mock_open)
        mock_cow_object = mocker.patch("routes.web.settings.Cow")
        mock_cow_object.query.filter_by.return_value = [
            testing_objects["cows"]["567"]
        ]
        mocker.patch("routes.web.settings.db")

        response = client.post("/settings/change_username", data={
            "new_username": "Sophia Olsen",
            "change_cow_ownership": "off",
        }, follow_redirects=True)

        mock_file.write.assert_not_called()
        mock_cow_object.query.filter_by.assert_not_called()
        assert response.status_code == 200
        assert (b"Username Sophia Olsen is already taken. "
                b"Please choose a different username.") in response.data


class TestChangePassword:
    """
    Tests changing your password
    """

    @pytest.mark.slow
    @pytest.mark.skip(reason="No longer using the json file")
    def test_change_password(self, client, mocker, mock_config):
        """
        If the user tries to change their password, the config file should be updated
        with a valid hash of the new password.
        """
        mocker.patch("routes.web.settings.get_username",
                     return_value="Keith Rivers")
        mock_open, mock_file = mock_config
        mocker.patch("routes.web.settings.open", mock_open)
        response = client.post("/settings/change_password", data={
            "old_password": "password",
            "new_password": "NewPassword",
            "confirm_new_password": "NewPassword",
        }, follow_redirects=True)

        mock_file.read.assert_called()
        mock_file.write.assert_called_once()
        written_string = str(mock_file.write.call_args.args[0])
        written_json = json.loads(written_string)
        assert check_password_hash(
            written_json["users"][0]["hashed_password"], "NewPassword")
        assert response.status_code == 200
        assert b"Password changed successfully" in response.data

    @pytest.mark.skip(reason="No longer using the json file")
    def test_change_password_with_mismatched_passwords(self, client, mocker, mock_config):
        """
        If the user types in their new password wrong, it should warn them.
        """
        mocker.patch("routes.web.settings.get_username",
                     return_value="Keith Rivers")
        mock_open, mock_file = mock_config
        mocker.patch("routes.web.settings.open", mock_open)
        response = client.post("/settings/change_password", data={
            "old_password": "OldPassword",
            "new_password": "NewPassword",
            "confirm_new_password": "DifferentNewPassword",
        }, follow_redirects=True)

        mock_file.read.assert_called()
        mock_file.write.assert_not_called()
        assert response.status_code == 200
        assert b"New passwords do not match!" in response.data

    @pytest.mark.skip(reason="No longer using the json file")
    def test_change_password_with_incorrect_old_password(self, client, mocker, mock_config):
        """
        If the user types in their new password wrong, it should warn them.
        """
        mocker.patch("routes.web.settings.get_username",
                     return_value="Keith Rivers")
        mock_open, mock_file = mock_config
        mocker.patch("routes.web.settings.open", mock_open)
        response = client.post("/settings/change_password", data={
            "old_password": "IncorrectOldPassword",
            "new_password": "NewPassword",
            "confirm_new_password": "NewPassword",
        }, follow_redirects=True)

        mock_file.read.assert_called()
        mock_file.write.assert_not_called()
        assert response.status_code == 200
        assert b"Incorrect old password" in response.data


class TestCheckUPnP:
    """
    Tests performing a check on a UPnP rule.
    """

    def test_check_upnp_windows_configured(self, client, mocker):
        """
        Test that checking the UPnP setup returns the correct information
        when using the UPnP wizard for Windows.
        """
        mock_platform = mocker.MagicMock()
        mock_platform.system.return_value = "Windows"
        mocker.patch("routes.web.settings.platform", mock_platform)
        mocker.patch("routes.web.settings.upnp_wizard_installed",
                     return_value=True)

        mock_subprocess = mocker.MagicMock()
        mock_subprocess.CalledProcessError = subprocess.CalledProcessError
        with open("tests/sample_upnp_wizard_output/configured.txt", encoding="utf-8") as file:
            mock_subprocess.check_output.return_value = file.read().replace("\n",
                                                                            "\r\n").encode("utf-8")
        mocker.patch("routes.web.settings.subprocess", mock_subprocess)

        mocker.patch("routes.web.settings.get_network_ssid",
                     return_value="THENAMEOFTHENETWORK")
        mocker.patch("routes.web.settings.get_public_ip",
                     return_value="PUBLICIP")
        mocker.patch("routes.web.settings.get_private_ip",
                     return_value="PRIVATEIP")

        response = client.get("/settings/check_upnp")

        assert response.status_code == 200
        # If sample output is modified, this may change
        assert response.data == json.dumps({
            "check_successful": True,
            "default_port": 31523,
            "rules": [
                {"port": "31523",
                 "protocol": "TCP",
                 "toAddr": "192.168.1.112",
                 "toPort": "31523",
                 "desc": "CattleDB"
                 }],
            "ssid": "THENAMEOFTHENETWORK",
            "host_private_ip_address": "PRIVATEIP",
            "host_public_ip_address": "PUBLICIP"
        }).encode("utf-8")

    def test_check_upnp_windows_not_configured(self, client, mocker):
        """
        If UPnP isn't configured, make sure that checking UPnP returns the
        correct information when using the UPnP Wizard for Windows
        """
        mock_platform = mocker.MagicMock()
        mock_platform.system.return_value = "Windows"
        mocker.patch("routes.web.settings.platform", mock_platform)
        mocker.patch("routes.web.settings.upnp_wizard_installed",
                     return_value=True)

        mock_subprocess = mocker.MagicMock()
        mock_subprocess.CalledProcessError = subprocess.CalledProcessError
        with open("tests/sample_upnp_wizard_output/not_configured.txt", encoding="utf-8") as file:
            mock_subprocess.check_output.return_value = file.read().replace("\n",
                                                                            "\r\n").encode("utf-8")
        mocker.patch("routes.web.settings.subprocess", mock_subprocess)

        mocker.patch("routes.web.settings.get_network_ssid",
                     return_value="THENAMEOFTHENETWORK")
        mocker.patch("routes.web.settings.get_public_ip",
                     return_value="PUBLICIP")
        mocker.patch("routes.web.settings.get_private_ip",
                     return_value="PRIVATEIP")

        response = client.get("/settings/check_upnp")

        assert response.status_code == 200
        # If sample output is modified, this may change
        assert response.data == json.dumps({
            "check_successful": True,
            "default_port": 31523,
            "rules": [],
            "ssid": "THENAMEOFTHENETWORK",
            "host_private_ip_address": "PRIVATEIP",
            "host_public_ip_address": "PUBLICIP"
        }).encode("utf-8")

    def test_check_upnp_windows_four_failures(self, client, mocker):
        """
        The UPnP wizard command just fails sometimes.
        It should try at least four times before giving up.
        """
        mock_platform = mocker.MagicMock()
        mock_platform.system.return_value = "Windows"
        mocker.patch("routes.web.settings.platform", mock_platform)
        mocker.patch("routes.web.settings.upnp_wizard_installed",
                     return_value=True)

        mock_subprocess = mocker.MagicMock()
        mock_subprocess.CalledProcessError = subprocess.CalledProcessError
        with open("tests/sample_upnp_wizard_output/not_configured.txt", encoding="utf-8") as file:
            mock_subprocess.check_output.side_effect = [
                subprocess.CalledProcessError(returncode=1, cmd="Command"),
                subprocess.CalledProcessError(returncode=1, cmd="Command"),
                subprocess.CalledProcessError(returncode=1, cmd="Command"),
                subprocess.CalledProcessError(returncode=1, cmd="Command"),
                file.read().replace("\n", "\r\n").encode("utf-8")
            ]
        mocker.patch("routes.web.settings.subprocess", mock_subprocess)

        mocker.patch("routes.web.settings.get_network_ssid",
                     return_value="THENAMEOFTHENETWORK")
        mocker.patch("routes.web.settings.get_public_ip",
                     return_value="PUBLICIP")
        mocker.patch("routes.web.settings.get_private_ip",
                     return_value="PRIVATEIP")

        response = client.get("/settings/check_upnp")

        assert response.status_code == 200
        # If sample output is modified, this may change
        assert response.data == json.dumps({
            "check_successful": True,
            "default_port": 31523,
            "rules": [],
            "ssid": "THENAMEOFTHENETWORK",
            "host_private_ip_address": "PRIVATEIP",
            "host_public_ip_address": "PUBLICIP"
        }).encode("utf-8")

    def test_check_upnp_windows_five_failures(self, client, mocker):
        """
        After failing to configure UPnP five times, it probably won't ever work.
        Give up and explain what happened.
        """
        mock_platform = mocker.MagicMock()
        mock_platform.system.return_value = "Windows"
        mocker.patch("routes.web.settings.platform", mock_platform)
        mocker.patch("routes.web.settings.upnp_wizard_installed",
                     return_value=True)

        mock_subprocess = mocker.MagicMock()
        mock_subprocess.CalledProcessError = subprocess.CalledProcessError
        mock_subprocess.check_output.side_effect = [
            subprocess.CalledProcessError(returncode=1, cmd="Command"),
            subprocess.CalledProcessError(returncode=1, cmd="Command"),
            subprocess.CalledProcessError(returncode=1, cmd="Command"),
            subprocess.CalledProcessError(returncode=1, cmd="Command"),
            subprocess.CalledProcessError(returncode=1, cmd="Command")
        ]
        mocker.patch("routes.web.settings.subprocess", mock_subprocess)

        response = client.get("/settings/check_upnp")

        assert response.status_code == 200
        assert response.data == json.dumps({
            "check_successful": False,
            "failure_reason": "UPnP Wizard command failed",
            "rule_exists": None
        }).encode("utf-8")

    def test_check_upnp_windows_no_wiz(self, client, mocker):
        """
        If the UPnP Wizard isn't installed, we cannot check UPnP.
        Return a json string with more details
        """
        mock_platform = mocker.MagicMock()
        mock_platform.system.return_value = "Windows"
        mocker.patch("routes.web.settings.platform", mock_platform)
        mocker.patch("routes.web.settings.upnp_wizard_installed",
                     return_value=False)

        response = client.get("/settings/check_upnp")

        assert response.status_code == 200
        assert response.data == json.dumps({
            "check_successful": False,
            "failure_reason": "UPnP Wizard not installed",
            "rule_exists": None
        }).encode("utf-8")

    def test_check_upnp_linux_configured(self, client, mocker):
        """
        If UPnP is configured, this should return a json string with all of the information.
        """
        mock_platform = mocker.patch("routes.web.settings.platform")
        mock_platform.system.return_value = "Linux"
        mock_miniupnpc = mocker.MagicMock()
        mock_miniupnpc.UPnP.return_value.getgenericportmapping.side_effect = [
            (53231, "TCP", ('192.168.1.112', 53231),
             'Some Random Rule', '1', '', 0),
            (46582, "TCP", ('192.168.1.112', 46582),
             'Some Random Rule', '1', '', 0),
            (5424, "TCP", ('192.168.1.112', 5424), 'Some Random Rule', '1', '', 0),
            (55861, "TCP", ('192.168.1.112', 55861),
             'Some Random Rule', '1', '', 0),
            (31523, "TCP", ('192.168.1.112', 31523), 'CattleDB', '1', '', 0),
            None
        ]
        mocker.patch("routes.web.settings.miniupnpc", mock_miniupnpc)

        mocker.patch("routes.web.settings.get_network_ssid",
                     return_value="THENAMEOFTHENETWORK")
        mocker.patch("routes.web.settings.get_public_ip",
                     return_value="PUBLICIP")
        mocker.patch("routes.web.settings.get_private_ip",
                     return_value="PRIVATEIP")

        response = client.get("/settings/check_upnp")
        mock_miniupnpc.UPnP.return_value.discover.assert_called_once()
        mock_miniupnpc.UPnP.return_value.selectigd.assert_called_once()
        mock_miniupnpc.UPnP.return_value.getgenericportmapping.assert_called()
        assert response.status_code == 200
        assert response.data == json.dumps({
            "check_successful": True,
            "rules": [{
                "port": 31523,
                "protocol": "TCP",
                "toAddr": "192.168.1.112",
                "toPort": 31523,
                "desc": "CattleDB"
            }],
            "default_port": 31523,
            "ssid": "THENAMEOFTHENETWORK",
                    "host_private_ip_address": "PRIVATEIP",
            "host_public_ip_address": "PUBLICIP"
        }).encode("utf-8")

    def test_check_upnp_linux_not_configured(self, client, mocker):
        """
        # TODO: Implement test
        """
        mock_platform = mocker.patch("routes.web.settings.platform")
        mock_platform.system.return_value = "Linux"
        mock_miniupnpc = mocker.MagicMock()
        mock_miniupnpc.UPnP.return_value.getgenericportmapping.side_effect = [
            (53231, "TCP", ('192.168.1.112', 53231),
             'Some Random Rule', '1', '', 0),
            (46582, "TCP", ('192.168.1.112', 46582),
             'Some Random Rule', '1', '', 0),
            (5424, "TCP", ('192.168.1.112', 5424), 'Some Random Rule', '1', '', 0),
            (55861, "TCP", ('192.168.1.112', 55861),
             'Some Random Rule', '1', '', 0),
            (31523, "TCP", ('192.168.1.112', 31523), 'CattleDB', '1', '', 0),
            None
        ]
        mocker.patch("routes.web.settings.miniupnpc", mock_miniupnpc)

        mocker.patch("routes.web.settings.get_network_ssid",
                     return_value="THENAMEOFTHENETWORK")
        mocker.patch("routes.web.settings.get_public_ip",
                     return_value="PUBLICIP")
        mocker.patch("routes.web.settings.get_private_ip",
                     return_value="PRIVATEIP")

        response = client.get("/settings/check_upnp")
        mock_miniupnpc.UPnP.return_value.discover.assert_called_once()
        mock_miniupnpc.UPnP.return_value.selectigd.assert_called_once()
        mock_miniupnpc.UPnP.return_value.getgenericportmapping.assert_called()
        assert response.status_code == 200
        assert response.data == json.dumps({
            "check_successful": True,
            "rules": [{
                "port": 31523,
                "protocol": "TCP",
                "toAddr": "192.168.1.112",
                "toPort": 31523,
                "desc": "CattleDB"
            }],
            "default_port": 31523,
            "ssid": "THENAMEOFTHENETWORK",
            "host_private_ip_address": "PRIVATEIP",
            "host_public_ip_address": "PUBLICIP"
        }).encode("utf-8")


class TestAddUPnPRule:
    """
    Tests adding a new UPnP rule
    """

    def test_add_upnp_rule_successfully(self, client, mocker):
        """
        Make sure that the add UPnP rule setup util is called correctly.
        If it works, make sure that's displayed to the user.
        """

        mocker.patch("routes.web.settings.add_upnp_rule", return_value=True)

        response = client.get(
            "/settings/add_upnp", headers={"Referer": "/settings"}, follow_redirects=True)
        assert response.status_code == 200
        assert b'Successfully added UPnP rule' in response.data

    def test_add_upnp_rule_unsuccessfully(self, client, mocker):
        """
        Make sure that the add UPnP rule setup util is called correctly.
        If it works, make sure that's displayed to the user.
        """

        mocker.patch("routes.web.settings.add_upnp_rule", return_value=False)

        response = client.get(
            "/settings/add_upnp", headers={"Referer": "/settings"}, follow_redirects=True)
        assert response.status_code == 200
        assert b'Failed to add UPnP rule' in response.data


class TestDownloads:
    """
    Tests downloading the CattleDB files
    """

    def test_download_database(self, client, mocker):
        """
        Make sure that downloading the database works correctly
        """
        mock_os = mocker.patch("routes.web.settings.os")
        mock_os.path.exists.return_value = True
        mock_send_file = mocker.patch("routes.web.settings.send_file")
        mock_send_file.return_value = "The cattle.db file"

        response = client.get("/settings/download_database")

        assert response.status_code == 200
        assert response.data == b"The cattle.db file"
        mock_os.path.exists.assert_called_once_with("cattle.db")
        mock_send_file.assert_called_once_with("cattle.db")

    def test_download_database_when_missing(self, client, mocker):
        """
        If the database is missing, don't send a blank file, explain
        what went wrong.
        """
        mock_os = mocker.patch("routes.web.settings.os")
        mock_os.path.exists.return_value = False
        mock_send_file = mocker.patch("routes.web.settings.send_file")
        mock_send_file.return_value = "The cattle.db file"
        with pytest.raises(FileNotFoundError):
            client.get("/settings/download_database")
        mock_os.path.exists.assert_called_once_with("cattle.db")
        mock_send_file.assert_not_called()

    def test_download_config(self, client, mocker):
        """
        Make sure that downloading the config works correctly
        """
        mock_os = mocker.patch("routes.web.settings.os")
        mock_os.path.exists.return_value = True
        mock_send_file = mocker.patch("routes.web.settings.send_file")
        mock_send_file.return_value = "The config.json file"
        response = client.get("/settings/download_config")
        assert response.status_code == 200
        assert response.data == b"The config.json file"
        mock_send_file.assert_called_once_with("config.json")

    def test_download_config_when_missing(self, client, mocker):
        """
        If the config is missing, don't send a blank file, explain
        what went wrong.
        """
        mock_os = mocker.patch("routes.web.settings.os")
        mock_os.path.exists.return_value = False
        mock_send_file = mocker.patch("routes.web.settings.send_file")
        mock_send_file.return_value = "The cattle.db file"
        with pytest.raises(FileNotFoundError):
            client.get("/settings/download_config")
        mock_os.path.exists.assert_called_once_with("config.json")
        mock_send_file.assert_not_called()


class TestDeleteUPnP:
    """
    Tests removing UPnP rules
    """

    def test_delete_upnp_windows(self, client, mocker):
        """
        Make sure the UPnP wizard command is called correctly and if it works,
        make sure they're shown a success message.
        """
        mock_platform = mocker.patch("routes.web.settings.platform")
        mock_platform.system.return_value = "Windows"
        mocker.patch("routes.web.settings.upnp_wizard_installed",
                     return_value=True)
        mock_subprocess = mocker.patch("routes.web.settings.subprocess")

        response = client.get("/settings/delete_upnp",
                              headers={"Referer": "/settings"}, follow_redirects=True)

        mock_subprocess.run.assert_called_once_with(
            r'"C:\Program Files (x86)\UPnP Wizard\UPnPWizardC.exe" -legacy -remove CattleDB')
        assert response.status_code == 200
        assert b"UPnP rule(s) removed successfully." in response.data

    def test_delete_upnp_windows_no_wiz(self, client, mocker):
        """
        If the user doesn't have UPnP wiz installed, we can't
        delete the rule. Make sure the user is prompted to install it.
        """
        mock_platform = mocker.patch("routes.web.settings.platform")
        mock_platform.system.return_value = "Windows"
        mocker.patch("routes.web.settings.upnp_wizard_installed",
                     return_value=False)
        mock_subprocess = mocker.patch("routes.web.settings.subprocess")

        response = client.get("/settings/delete_upnp",
                              headers={"Referer": "/settings"}, follow_redirects=True)

        assert not mock_subprocess.run.called
        assert response.status_code == 200
        assert b"CattleDB needs an additional program" in response.data

    def test_delete_upnp_linux(self, client, mocker):
        """
        Make sure that miniupnpc is correctly called to delete
        all "CattleDB" rules.
        """
        mock_platform = mocker.patch("routes.web.settings.platform")
        mock_platform.system.return_value = "Linux"
        mock_miniupnpc = mocker.MagicMock()
        mock_miniupnpc.UPnP.return_value.getgenericportmapping.side_effect = [
            (53231, "TCP", ('192.168.1.112', 53231),
             'Some Random Rule', '1', '', 0),
            (46582, "TCP", ('192.168.1.112', 46582),
             'Some Random Rule', '1', '', 0),
            (5424, "TCP", ('192.168.1.112', 5424), 'Some Random Rule', '1', '', 0),
            (55861, "TCP", ('192.168.1.112', 55861),
             'Some Random Rule', '1', '', 0),
            (31523, "TCP", ('192.168.1.112', 31523), 'CattleDB', '1', '', 0),
            None
        ]
        mocker.patch("routes.web.settings.miniupnpc", mock_miniupnpc)

        response = client.get("/settings/delete_upnp",
                              headers={"Referer": "/settings"}, follow_redirects=True)

        mock_miniupnpc.UPnP.return_value.discover.assert_called_once()
        mock_miniupnpc.UPnP.return_value.selectigd.assert_called_once()
        mock_miniupnpc.UPnP.return_value.getgenericportmapping.assert_called()
        mock_miniupnpc.UPnP.return_value.deleteportmapping.assert_called_once_with(
            31523, "TCP")
        assert response.status_code == 200
        assert b"UPnP rule(s) removed successfully." in response.data


class TestDeleteAccount:
    """
    Tests removing an account
    """

    @pytest.mark.skip(reason="No longer using the json file")
    def test_delete_account_correct_password(self, client, mocker, mock_config):
        """
        If you try to delete your account and enter your password correctly
        it should remove your entry from the config.
        """
        mocker.patch("routes.web.settings.get_username",
                     return_value="Keith Rivers")
        mock_open, mock_file = mock_config
        mocker.patch("helper.login_checker.open", mock_open)
        mocker.patch("routes.web.settings.open", mock_open)

        response = client.post("/settings/delete_account",
                               data={"password": "password"})
        assert response.status_code == 302
        assert "/logout" in response.location
        mock_file.read.assert_called()

        written_string = str(mock_file.write.call_args.args[0])
        expected = json.loads(mock_file.read())
        expected["users"].pop(0)
        assert written_string == json.dumps(expected, indent=4)

    @pytest.mark.skip(reason="No longer using the json file")
    def test_delete_account_incorrect_password(self, client, mocker, mock_config):
        """
        If the user tries to delete their account, but enters their password
        incorrectly, they shouldn't be allowed to.
        """
        mocker.patch("routes.web.settings.get_username",
                     return_value="Keith Rivers")
        mock_open, mock_file = mock_config
        mocker.patch("helper.login_checker.open", mock_open)
        mocker.patch("routes.web.settings.open", mock_open)

        response = client.post(
            "/settings/delete_account",
            data={"password": "Incorrect Password"},
            follow_redirects=True
        )
        assert response.status_code == 200
        assert b"Invalid Password" in response.data
        mock_file.write.assert_not_called()


class TestDeleteDatabase:
    """
    Tests removing the database
    """

    def test_delete_database_successfully(self, client, mocker, mock_config, capfd, testing_objects):
        """
        If the user is sure they want to delete the database, it should
        be moved to the Recycle Bin, and redirect to the install page.
        """
        mocker.patch("routes.web.settings.get_username",
                     return_value="Keith Rivers")
        mock_user_object = mocker.patch("helper.login_checker.User")
        mock_user_object.query.filter_by.return_value.first.return_value = testing_objects[
            "users"]["Keith Rivers"]
        # VERY IMPORTANT:
        mock_send2trash = mocker.patch("routes.web.settings.send2trash")

        response = client.post("/settings/delete_database",
                               data={"password": "password"})
        assert response.status_code == 302
        assert "/install" in response.location
        mock_send2trash.assert_called_once_with("cattle.db")

        out, _ = capfd.readouterr()
        assert out == ("Successfully deleted the database\n"
                       "If this action was unintentional please close this window, "
                       "restore the file from your computer's recycle bin, and restart CattleDB.\n")

    def test_delete_database_incorrect_pw(self, client, mocker, mock_config, testing_objects, capfd):
        """
        If you try to delete the database,
        but don't enter your password correctly,
        it shouldn't let you.
        """
        mocker.patch("routes.web.settings.get_username",
                     return_value="Keith Rivers")
        mock_user_object = mocker.patch("helper.login_checker.User")
        mock_user_object.query.filter_by.return_value.first.return_value = testing_objects[
            "users"]["Keith Rivers"]
        # VERY IMPORTANT:
        mock_send2trash = mocker.patch("routes.web.settings.send2trash")

        response = client.post(
            "/settings/delete_database",
            data={"password": "Incorrect Password"},
            follow_redirects=True
        )
        assert response.status_code == 200
        assert b"Invalid Password" in response.data
        mock_send2trash.assert_not_called()


        out, _ = capfd.readouterr()
        assert out == ""


class TestDeleteConfig:
    """
    Tests removing the config file
    """

    def test_delete_config_successfully(self, client, mocker, mock_config, capfd, testing_objects):
        """
        If the user is sure they want to delete the database, it should
        be moved to the Recycle Bin, and redirect to the install page.
        """
        mocker.patch("routes.web.settings.get_username",
                     return_value="Keith Rivers")
        mock_user_object1 = mocker.patch("helper.login_checker.User")
        mock_user_object2= mocker.patch("routes.web.settings.User")
        mock_user_object1.query.filter_by.return_value.first.return_value = testing_objects[
            "users"]["Keith Rivers"]
        mock_user_object2.query.filter_by.return_value.first.return_value = testing_objects[
            "users"]["Keith Rivers"]
        # VERY IMPORTANT:
        mock_send2trash = mocker.patch("routes.web.settings.send2trash")

        response = client.post("/settings/delete_config",
                               data={"password": "password"})
        assert response.status_code == 302
        assert "/install" in response.location
        mock_send2trash.assert_called_once_with("config.json")

        out, _ = capfd.readouterr()
        assert out == ("Successfully deleted the config\n"
                       "If this action was unintentional please close this window, "
                       "restore the file from your computer's recycle bin, and restart CattleDB.\n")

    def test_delete_config_incorrect_pw(self, client, mocker, mock_config, capfd):
        """
        If you try to delete the database,
        but don't enter your password correctly,
        it shouldn't let you.
        """
        mocker.patch("routes.web.settings.get_username",
                     return_value="Keith Rivers")
        mock_open, mock_file = mock_config
        mocker.patch("helper.login_checker.open", mock_open)
        mocker.patch("routes.web.settings.open", mock_open)
        # VERY IMPORTANT:
        mock_send2trash = mocker.patch("routes.web.settings.send2trash")

        response = client.post(
            "/settings/delete_config",
            data={"password": "Incorrect Password"},
            follow_redirects=True
        )
        assert response.status_code == 200
        assert b"Invalid Password" in response.data
        mock_send2trash.assert_not_called()
        mock_file.write.assert_not_called()

        out, _ = capfd.readouterr()
        assert out == ""


class TestDeleteBoth:
    """
    Tests deleting the database and the config
    """

    def test_delete_both_successfully(self, client, mocker, mock_config, capfd):
        """
        If the user is sure they want to delete the database and config, they should
        be moved to the Recycle Bin, and redirect the user to the install page.
        """
        mocker.patch("routes.web.settings.get_username",
                     return_value="Keith Rivers")
        mock_open, mock_file = mock_config
        mocker.patch("helper.login_checker.open", mock_open)
        mocker.patch("routes.web.settings.open", mock_open)
        # VERY IMPORTANT:
        mock_send2trash = mocker.patch("routes.web.settings.send2trash")

        response = client.post("/settings/delete_both",
                               data={"password": "password"})
        assert response.status_code == 302
        assert "/install" in response.location
        mock_send2trash.assert_has_calls([
            mocker.call("config.json"),
            mocker.call("cattle.db")
        ])
        mock_file.write.assert_not_called()

        out, _ = capfd.readouterr()
        assert out == ("Successfully deleted the config\n"
                       "Successfully deleted the database\n"
                       "If this action was unintentional please close this window, "
                       "restore the file from your computer's recycle bin, and restart CattleDB.\n")

    def test_delete_both_incorrect_pw(self, client, mocker, mock_config, capfd):
        """
        If the user tries to delete the database and config without the correct password
        make sure it doesn't let them.
        """
        mocker.patch("routes.web.settings.get_username",
                     return_value="Keith Rivers")
        mock_open, mock_file = mock_config
        mocker.patch("routes.web.settings.open", mock_open)
        # VERY IMPORTANT:
        mock_send2trash = mocker.patch("routes.web.settings.send2trash")

        response = client.post(
            "/settings/delete_both",
            data={"password": "Incorrect Password"},
            follow_redirects=True
        )
        assert response.status_code == 200
        assert b"Invalid Password" in response.data
        mock_send2trash.assert_not_called()
        mock_file.write.assert_not_called()

        out, _ = capfd.readouterr()
        assert out == ""
