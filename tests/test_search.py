"""
Tests everything in the search page
"""
import pytest

from models.cow import Cow
from models.event import Event
from models.transaction import Transaction


@ pytest.fixture
def patch_cow(mocker):
    """
    A fixture to patch in some default values for Cow.query.all

    TODO remove
    """
    mock_cow = mocker.patch("search_functions.Cow")
    mock_cow.query.all.return_value = [
        Cow(
            tag_number="TestCowTag",
            owner="TestCowOwner",
            sex="TestCowSex"
        ),
        Cow(
            tag_number="800",
            owner="TestCowDifferentOwner",
            sex="TestCowDifferentSex",
            description="Cow with numerical tag"
        ),
        Cow(
            tag_number="Calf",
            owner="TestCowOwner",
            dam_id=0
        )
    ]


@ pytest.fixture
def patch_event(mocker):
    """
    A fixture to patch in some default values for Event.query.all

    TODO remove
    """
    mock_event = mocker.patch("search_functions.Event")
    mock_event.query.all.return_value = [
        Event(
            name="TestEventName",
            description="TestEventDescription",
            date="1970-01-01"
        )
    ]


@ pytest.fixture
def patch_transaction(mocker):
    """
    A fixture to patch in some default values for Transaction.query.all

    TODO remove
    """
    mock_transaction = mocker.patch("search_functions.Transaction")
    mock_transaction.query.all.return_value = [
        Transaction(
            name="TestTransactionName",
            tofrom="TestTransactionToFrom",
            price=20.20
        )
    ]


@ pytest.fixture
def mock_get_unique(mocker):
    """
    Patch the get_unique_values function

    TODO remove
    """
    mocker.patch("search.get_unique_values", return_value={})

class TestSearchQueries:
    """
    Tests some search queries
    """
    params = [
        pytest.param("TestCowTag", ("TestTransactionName",
                     "TestEventName"), id="Search by tag number"),
        pytest.param("TestCowOwner", ("TestTransactionName",
                     "TestEventName"), id="Search by owner"),
        pytest.param("TestCowSex", ("TestTransactionName",
                     "TestEventName"), id="Search by sex"),
        pytest.param("TestEventName", ("TestCowTag",
                     "TestTransactionName"), id="Search by event name"),
        pytest.param("TestEventDescription", ("TestCowTag",
                     "TestTransactionName"), id="Search by event description"),
        pytest.param("1970-01-01", ("TestCowTag",
                     "TestTransactionName"), id="Search by event date"),
        pytest.param("TestTransactionName", ("TestCowTag",
                     "TestEventName"), id="Search by transaction name"),
        pytest.param("TestTransactionToFrom", ("TestCowTag",
                     "TestEventName"), id="Search by transaction to/from"),
        pytest.param("$20.20", ("TestCowTag",
                     "TestEventName"), id="Search by transaction price"),
    ]

    
    