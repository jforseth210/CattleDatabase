"""
Responsible for testing everything in the api_events module
"""
import json
import pytest
from sqlalchemy.exc import IntegrityError


class TestAddEvent:
    """
    Responsible for testing adding an event.
    """

    def test_add(self, client, mocker, testing_objects, fake_get_cow_from_tag):
        """
        This tests the add event api under very basic conditions.

        It should create the event and add it to the database with all of the correct information.
        """
        mock_event = mocker.patch("routes.api.api_events.Event")
        mock_db = mocker.patch("routes.api.api_events.db")
        mocker.patch("routes.api.api_events.get_cow_from_tag",
                     side_effect=fake_get_cow_from_tag)
        response = client.post("/api/events/add", data=json.dumps({
            "cows": [
                "576",
                "567"
            ],
            "name": "Branded",
            "date": "1970-01-01",
            "description": "Branded calves"
        }))
        mock_event.assert_called_once_with(
            cows=[testing_objects["cows"]["576"],
                  testing_objects["cows"]["567"]],
            name="Branded",
            date="1970-01-01",
            description="Branded calves"
        )
        mock_db.session.add.assert_called_once()
        mock_db.session.commit.assert_called_once()
        assert response.status_code == 200
        assert response.data == json.dumps(
            {"operation": "add_event", "result": "success"}).encode("utf-8")

    def test_add_event_with_no_cows(self, client, mocker):
        """
        Tests creating an event with no cows
        """
        mock_event = mocker.patch("routes.api.api_events.Event")
        mock_db = mocker.patch("routes.api.api_events.db")
        response = client.post("/api/events/add", data=json.dumps({
            "cows": [],
            "name": "Branded",
            "date": "1970-01-01",
            "description": "Branded calves"
        }))
        mock_event.assert_called_once_with(
            cows=[],
            name="Branded",
            date="1970-01-01",
            description="Branded calves"
        )
        mock_db.session.add.assert_called_once()
        mock_db.session.commit.assert_called_once()
        assert response.status_code == 200
        assert response.data == json.dumps(
            {"operation": "add_event", "result": "success"}).encode("utf-8")


class TestGetEvent:
    """
    Tests fetching an event
    """

    def test_get_event(self, client, mocker, testing_objects):
        """
        Tests fetching an event
        """
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["branded"]

        response = client.get("/api/events/event", query_string={
            "event_id": 2})
        assert response.status_code == 200
        assert json.loads(response.data) == {'cows': [{'sex': 'Steer', 'tag_number': '921'}, {'sex': 'Heifer', 'tag_number': '922'}], 'date': '1970-01-01',
                                             'description': 'A fake branding for testing purposes. In January...', 'event_id': 2, 'isArchived': False, 'name': 'Branded', 'transactions': [{'name': 'Cefimoxin', 'price': -1.4, 'transaction_id': 9}]}

    def test_get_event_no_transaction_events_or_calves(self, client, mocker, testing_objects):
        """
        Tests fetching an event
        """
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["calved"]

        response = client.get("/api/events/event", query_string={
            "event_id": 1})
        assert response.status_code == 200
        assert json.loads(response.data) == {'cows': [], 'date': '1970-01-02', 'description': '567 gave birth to 922',
                                             'event_id': 0, 'isArchived': False, 'name': 'Calved', 'transactions': []}

    def test_get_nonexistant_event(self, client, mocker):
        """
        Tests trying to retrieve an event_id without a corresponding event
        """
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = None

        response = client.get("/api/events/event", query_string={
            "event_id": 1})
        assert response.status_code == 404
        assert response.data == b'{"result": "failure", "reason": "Event not found"}'


class TestGetEventList:
    """
    Tests fetching a list of events
    """

    def test_get_event_list(self, client, mocker, testing_objects):
        """
        Tests getting a list of events
        """
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.all.return_value = [
            testing_objects["events"]["branded"], testing_objects["events"]["calved"]]
        response = client.get("/api/events/get_list")
        assert response.status_code == 200
        assert json.loads(response.data) == [{'event_id': 0, 'name': 'Calved', 'isArchived': False, 'date': '1970-01-02'}, {
            'event_id': 2, 'name': 'Branded', 'isArchived': False, 'date': '1970-01-01'}]

    def test_get_empty_event_list(self, client, mocker):
        """
        Tests getting a list of events when there are none
        """
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.all.return_value = []
        response = client.get("/api/events/get_list")
        assert response.status_code == 200
        assert response.data == json.dumps([]).encode("utf-8")


class TestGetEventCows:
    """
    Tests the route to retrieve an event's cows
    """

    def test_get_event_cows(self, client, mocker, testing_objects):
        """
        Tests retrieving an events cows under normal circumstances.
        """
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["branded"]

        response = client.get("/api/events/get_cows",
                              query_string={"event_id": "2"})
        assert response.status_code == 200
        assert json.loads(response.data) == [{'tag_number': '921', 'sex': 'Steer'}, {
            'tag_number': '922', 'sex': 'Heifer'}]

    def test_get_nonexistant_event_cows(self, client, mocker):
        """
        Tests retrieving an events cows under normal circumstances.
        """
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = None

        response = client.get("/api/events/get_cows",
                              query_string={"event_id": "2"})
        assert response.status_code == 404
        assert response.data == b'{"result": "failure", "reason": "Event not found"}'


class TestEventUpdateCows:
    """
    Tests updating the cows associated with an event
    """

    def test_event_update_cows(self, client, mocker, testing_objects, fake_get_cow_from_tag):
        """
        Tests updating an event's cows
        """
        mocker.patch("routes.api.api_events.db")
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["calved"]
        mocker.patch("routes.api.api_events.get_cow_from_tag",
                     side_effect=fake_get_cow_from_tag)
        response = client.post("/api/events/update_cows", data=json.dumps({
            "event_id": 2,
            "cows": ["567", "576"]
        }))
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "update_cows",
            "result": "success"
        }).encode("utf-8")
        assert testing_objects["events"]["calved"].cows == [
            testing_objects["cows"]["567"], testing_objects["cows"]["576"]]

    def test_non_existant_event_update_cows(self, client, mocker, fake_get_cow_from_tag):
        """
        Tests trying to modify an event that doesn't exist
        """
        mocker.patch("routes.api.api_events.db")
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = None
        mocker.patch("routes.api.api_events.get_cow_from_tag",
                     side_effect=fake_get_cow_from_tag)
        response = client.post("/api/events/update_cows", data=json.dumps({
            "event_id": 46546,
            "cows": ["567", "576"]
        }))
        assert response.status_code == 404
        assert response.data == b'{"result": "failure", "reason": "Event not found"}'

    def test_event_update_to_no_cows(self, client, mocker, testing_objects, fake_get_cow_from_tag):
        """
        Tests removing all cows from an event
        """
        mocker.patch("routes.api.api_events.db")
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["calved"]
        mocker.patch("routes.api.api_events.get_cow_from_tag",
                     side_effect=fake_get_cow_from_tag)
        response = client.post("/api/events/update_cows", data=json.dumps({
            "event_id": 2,
            "cows": []
        }))
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "update_cows",
            "result": "success"
        }).encode("utf-8")
        assert testing_objects["events"]["calved"].cows == []


class TestChangeEventName:
    """
    Tests modifying the name of an event
    """

    def test_change_event_name(self, client, mocker, testing_objects):
        """
        Tests changing the name of an event successfully
        """
        assert testing_objects["events"]["calved"].name != "Incremented number of offspring"
        mocker.patch("routes.api.api_events.db")
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["calved"]
        response = client.post("/api/events/change_name", data=json.dumps({
            "event_id": 2,
            "name": "Incremented number of offspring"
        }))
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "change_name",
            "result": "success"
        }).encode("utf-8")
        assert testing_objects["events"]["calved"].name == "Incremented number of offspring"

    def test_non_existant_event_change_name(self, client, mocker):
        """
        Tests trying to modify an nonexistant event
        """
        mocker.patch("routes.api.api_events.db")
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = None
        response = client.post("/api/events/change_name", data=json.dumps({
            "event_id": 46546,
            "name": "Incremented number of offspring"
        }))
        assert response.status_code == 404
        assert response.data == json.dumps({}).encode("utf-8")


class TestChangeEventDescription:
    """
    Tests modifying the description of an event
    """

    def test_change_event_description(self, client, mocker, testing_objects):
        """
        Tests changing the description of an event successfully
        """
        assert testing_objects["events"]["calved"].description != "A description describing the event being described"
        mocker.patch("routes.api.api_events.db")
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["calved"]
        response = client.post("/api/events/change_description", data=json.dumps({
            "event_id": 2,
            "description": "A description describing the event being described"
        }))
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "change_description",
            "result": "success"
        }).encode("utf-8")
        assert testing_objects["events"]["calved"].description == "A description describing the event being described"

    def test_non_existant_event_change_description(self, client, mocker):
        """
        Tests updating an event's cows
        """
        mocker.patch("routes.api.api_events.db")
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = None
        response = client.post("/api/events/change_description", data=json.dumps({
            "event_id": 46546,
            "description": "A description describing the event being described"
        }))
        assert response.status_code == 404
        assert response.data == json.dumps({}).encode("utf-8")


class TestChangeEventDate:
    """
    Tests modifying the date of an event
    """

    def test_change_event_date(self, client, mocker, testing_objects):
        """
        Tests changing the date of an event successfully
        """
        assert testing_objects["events"]["calved"].date != "1970-05-14"
        mocker.patch("routes.api.api_events.db")
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["calved"]
        response = client.post("/api/events/change_date", data=json.dumps({
            "event_id": 2,
            "date": "1970-05-14"
        }))
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "change_date",
            "result": "success"
        }).encode("utf-8")
        assert testing_objects["events"]["calved"].date == "1970-05-14"

    def test_non_existant_event_change_date(self, client, mocker):
        """
        Tests updating an event's cows
        """
        mocker.patch("routes.api.api_events.db")
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = None
        response = client.post("/api/events/change_date", data=json.dumps({
            "event_id": 46546,
            "date": "1970-05-14"
        }))
        assert response.status_code == 404
        assert response.data == b'{"result": "failure", "reason": "Event not found"}'


class TestArchive:
    """
    This class is responsible for testing archiving a event.
    """

    def test_archive_unarchived_event(self, client, mocker, testing_objects):
        """
        Tests taking a event that's not archived and archiving it.
        """
        mocker.patch("routes.api.api_events.db")
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["calved"]
        assert testing_objects["events"]["calved"].isArchived is False

        response = client.post("/api/events/archive", data=json.dumps({
            "event_id": 2
        }))

        assert testing_objects["events"]["calved"].isArchived is True
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "archive_event",
            "result": "success"
        }) .encode("utf-8")

    def test_archive_archived_event(self, client, mocker, testing_objects):
        """
        Tests taking a event that is archived and trying to archive it again.
        """
        mocker.patch("routes.api.api_events.db")
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["archived"]

        response = client.post("/api/events/archive", data=json.dumps({
            "event_id": 3
        }))
        assert testing_objects["events"]["archived"].isArchived is True
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "archive_event",
            "result": "success"
        }) .encode("utf-8")

    def test_archive_nonexistant_event(self, client, mocker):
        """
        Tests trying to archive a event that doesn't exist.
        """
        mocker.patch("routes.api.api_events.db")
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = None

        response = client.post("/api/events/archive", data=json.dumps({
            "event_id": 2642
        }))
        assert response.status_code == 404
        assert response.data == b'{"result": "failure", "reason": "Event not found"}'


class TestUnarchive:
    """
    This class is responsible for testing archiving a event.
    """

    def test_unarchive_unarchived_event(self, client, mocker, testing_objects):
        """
        Tests taking a event that's not archived and unarchiving it.
        """
        mocker.patch("routes.api.api_events.db")
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["calved"]
        assert testing_objects["events"]["calved"].isArchived is False

        response = client.post("/api/events/unarchive", data=json.dumps({
            "event_id": 2
        }))
        assert testing_objects["events"]["calved"].isArchived is False
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "unarchive_event",
            "result": "success"
        }) .encode("utf-8")

    def test_unarchive_archived_event(self, client, mocker, testing_objects):
        """
        Tests taking a event that is archived and trying to unarchive it
        """
        mocker.patch("routes.api.api_events.db")
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["archived"]
        assert testing_objects["events"]["archived"].isArchived is True

        response = client.post("/api/events/unarchive", data=json.dumps({
            "event_id": 3
        }))
        assert testing_objects["events"]["archived"].isArchived is False
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "unarchive_event",
            "result": "success"
        }) .encode("utf-8")

    def test_archive_nonexistant_event(self, client, mocker):
        """
        Tests trying to unarchive a event that doesn't exist.
        """
        mocker.patch("routes.api.api_events.db")
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = None

        response = client.post("/api/events/unarchive", data=json.dumps({
            "event_id": 6546
        }))
        assert response.status_code == 404
        assert response.data == b'{"result": "failure", "reason": "Event not found"}'


class TestDeleteEvent:
    """
    Tests the deletion of an event
    """

    def test_delete_event(self, client, mocker, testing_objects):
        """
        Tests deleting an event
        """
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_db = mocker.patch("routes.api.api_events.db")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["calved"]

        response = client.post("/api/events/delete", data=json.dumps({
            "event_id": 2
        }))

        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "delete_event",
            "result": "success"
        }).encode("utf-8")
        mock_db.session.delete.assert_called_once()
        mock_db.session.commit.assert_called_once()

    def test_delete_event_with_transactions(self, client, mocker, testing_objects):
        """
        Tests removing an event with existing transactions
        """
        mock_db = mocker.patch("routes.api.api_events.db")
        mock_db.session.commit.side_effect = IntegrityError(
            # I don't know what the arguments are
            # supposed to be. This works:
            mocker.MagicMock(),
            mocker.MagicMock(),
            mocker.MagicMock()
        )
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["calved"]

        response = client.post("/api/events/delete", data=json.dumps({
            "event_id": 2
        }))

        assert response.status_code == 400
        assert response.data == b'{"result": "failure", "reason": "Transactions still exist"}'

    def test_delete_nonexistant_event(self, client, mocker):
        """
        Tests removing an event that doesn't exist
        """
        mock_db = mocker.patch("routes.api.api_events.db")
        mock_event_object = mocker.patch("routes.api.api_events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = None

        response = client.post("/api/events/delete", data=json.dumps({
            "event_id": 2
        }))

        assert response.status_code == 404
        mock_db.session.delete.assert_not_called()
        mock_db.session.delete.assert_not_called()


class TestEmptyRequests:
    """
    Tests to make sure the server correctly handles requests with missing information.
    """
    base = "/api/events"
    routes = [
        ("/add", "POST"),
        ("/event", "GET"),
        ("/get_cows", "GET"),
        ("/update_cows", "POST"),
        ("/change_name", "POST"),
        ("/change_description", "POST"),
        ("/change_date", "POST"),
        ("/archive", "POST"),
        ("/unarchive", "POST"),
        ("/delete", "POST")
    ]

    @ pytest.mark.parametrize("route, method", routes)
    def test_empty_request(self, client, route, method):
        """
        Sends a request to the route with no args.
        """
        response = None
        if method == "POST":
            response = client.post(f"{self.base}{route}")
        elif method == "GET":
            response = client.get(f"{self.base}{route}")
        else:
            raise AssertionError(
                "Test setup incorrectly. Method should be GET or POST")
        assert response.status_code == 400
        assert json.loads(response.data.decode("utf-8"))["result"] == "failure"
        assert "Missing required argument:" in json.loads(response.data.decode("utf-8")
                                                          )["reason"]

    @ pytest.mark.parametrize("route, method", routes)
    def test_request_with_invalid_keys(self, client, route, method):
        """
        Sends a request to the route with no args.
        """
        response = None
        if method == "POST":
            response = client.post(f"{self.base}{route}", data=json.dumps(
                {"Favorite Color": "Red"}))
        elif method == "GET":
            response = client.get(f"{self.base}{route}", query_string={
                "Favorite Color": "Red"})
        else:
            raise AssertionError(
                "Test setup incorrectly. Method should be GET or POST")
        assert response.status_code == 400
        assert json.loads(response.data.decode("utf-8"))["result"] == "failure"
        assert "Missing required argument:" in json.loads(response.data.decode("utf-8")
                                                          )["reason"]
