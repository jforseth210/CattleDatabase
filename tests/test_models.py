"""
Test the various cattle models
"""
import pytest

from helper.sexes import get_sexes
from models.cow import Cow
from models.event import Event
from models.transaction import Transaction


class TestGetSexes:
    """
    Test getting various lists of sexes
    """

    def test_get_all(self):
        """
        Test get_sexes when we want all possible sexes, male and female, fertile and infertile.
        """
        result = get_sexes("both")
        assert sorted(result) == sorted([
            "Cow",
            "Heifer",
            "Heifer (Replacement)",
            "Heifer (Market)",
            "Free-Martin",
            "Bull",
            "Bull (AI)",
            "Steer"
        ])

    def test_get_male(self):
        """
        Test get_sexes when we want all male sexes, fertile and infertile.
        """
        result = get_sexes("male")
        assert sorted(result) == sorted(["Bull", "Bull (AI)", "Steer"])

    def test_get_female(self):
        """
        Test get_sexes when we want all female sexes, fertile and infertile.
        """
        result = get_sexes("female")
        assert sorted(result) == sorted([
            "Cow",
            "Heifer",
            "Heifer (Replacement)",
            "Heifer (Market)",
            "Free-Martin"
        ])

    def test_get_male_fertile(self):
        """
        Test get_sexes when we want only fertile male sexes (types of bulls)
        """
        result = get_sexes("male", True)
        assert sorted(result) == sorted(["Bull", "Bull (AI)"])

    def test_get_female_fertile(self):
        """
        Test get_sexes when we want only fertile female sexes
        """
        result = get_sexes("female", True)
        assert sorted(result) == sorted(["Cow",
                                         "Heifer", "Heifer (Replacement)"])

    def test_get_male_infertile(self):
        """
        Test get_sexes when we want only infertile male sexes
        """
        result = get_sexes("male", False)
        assert sorted(result) == sorted(["Steer"])

    def test_get_female_infertile(self):
        """
        Test get_sexes when we want only infertile female sexes
        """
        result = get_sexes("female", False)
        assert sorted(result) == sorted([
            "Heifer (Market)", "Free-Martin"])


class TestCow:
    """
    Test the cow model
    """

    class TestGetDam:
        """
        Tests getting the actual dam object from the dam id.
        """

        def test_get_dam(self, app, mocker):
            """
            Most of the heavy lifting on this one is done by Flask_SQLAlchemy.
            Basically, we take a cow with a given dam_id and make sure that get_dam()
            returns cow provided by the Flask_SQLAlchemy query.
            """
            mock_query = mocker.patch.object(Cow, "query")
            mock_query.filter_by.return_value.first.return_value = Cow(
                tag_number="Mother", owner="Keith Rivers")

            result = Cow(dam_id=12345).get_dam()

            assert result.tag_number == "Mother"
            mock_query.filter_by.assert_called_once_with(cow_id=12345)

        def test_get_nonexistant_dam(self, app, mocker):
            """
            If a cow doesn't have a dam in the system, get_dam should return None
            """
            mock_query = mocker.patch.object(Cow, "query")
            mock_query.filter_by.return_value.first.return_value = None
            result = Cow(dam_id=12345).get_dam()
            assert result is None
            mock_query.filter_by.assert_called_once_with(cow_id=12345)

    class TestGetSire:
        """
        Tests getting the actual sire object from the dam id.
        """

        def test_get_sire(self, app, mocker):
            """
            Most of the heavy lifting on this one is done by Flask_SQLAlchemy.
            Basically, we take a cow with a given sire_id and make sure that get_sire()
            returns cow provided by the Flask_SQLAlchemy query.
            """
            mock_query = mocker.patch.object(Cow, "query")
            mock_query.filter_by.return_value.first.return_value = Cow(
                tag_number="Father", owner="Keith Rivers")
            result = Cow(sire_id=12345).get_sire()
            assert result.tag_number == "Father"
            mock_query.filter_by.assert_called_once_with(cow_id=12345)

        def test_get_nonexistant_sire(self, app, mocker):
            """
            If a cow doesn't have a sire in the system, get_sire() should return None
            """
            mock_query = mocker.patch.object(Cow, "query")
            mock_query.filter_by.return_value.first.return_value = None
            result = Cow(sire_id=12345).get_sire()
            assert result is None
            mock_query.filter_by.assert_called_once_with(cow_id=12345)

    class TestGetCalves:
        """
        Testing to make sure the model correctly retrieves the cow's offspring.
        """

        def test_get_cows_calves(self, app, mocker):
            """
            Make sure that get_calves works as expected. Should return whatever the query spits out.
            """
            mock_query = mocker.patch.object(Cow, "query")
            mock_query.filter_by.return_value = [
                Cow(tag_number="Calf1"), Cow(tag_number="Calf2")]
            result = Cow(sex="Cow", cow_id=12345).get_calves()
            assert result == [Cow(tag_number="Calf1"), Cow(tag_number="Calf2")]
            mock_query.filter_by.assert_called_once_with(dam_id=12345)

        def test_get_cows_nonexistant_calves(self, app, mocker):
            """
            If a cow doesn't have any calves, make sure it returns None
            """
            mock_query = mocker.patch.object(Cow, "query")
            mock_query.filter_by.return_value = None
            result = Cow(sex="Cow", cow_id=12345).get_calves()
            assert result is None
            mock_query.filter_by.assert_called_once_with(dam_id=12345)

        def test_get_bulls_calves(self, app, mocker):
            """
            Make sure that get_calves works as expected. Should return whatever the query spits out.
            Make sure filter is called with sire_id instead of dam_id
            """
            mock_query = mocker.patch.object(Cow, "query")
            mock_query.filter_by.return_value = [
                Cow(tag_number="Calf1"), Cow(tag_number="Calf2")]
            result = Cow(sex="Bull", cow_id=12345).get_calves()
            assert result == [Cow(tag_number="Calf1"), Cow(tag_number="Calf2")]
            mock_query.filter_by.assert_called_once_with(sire_id=12345)

        def test_get_bulls_nonexistant_calves(self, app, mocker):
            """
            If a bull doesn't have any calves, make sure it returns None
            """
            mock_query = mocker.patch.object(Cow, "query")
            mock_query.filter_by.return_value = None
            result = Cow(sex="Bull", cow_id=12345).get_calves()
            assert result is None
            mock_query.filter_by.assert_called_once_with(sire_id=12345)

    class TestGetTransactions:
        """
        Tests to make sure the model correctly retrieves all of the transactions.
        """

        def test_get_transactions(self, app, mocker):
            """
            Make sure that get_transactions returns every transaction
            from every event the cow is involved in.
            """
            transactions = Cow(tag_number="TheTransactionCow", events=[
                Event(
                    name="Event 1",
                    transactions=[
                        Transaction(name="Event 1 Transaction 1",
                                    cows=[Cow(tag_number="TheTransactionCow")]),
                        Transaction(name="Event 1 Transaction 2",
                                    cows=[Cow(tag_number="TheTransactionCow")]),
                    ]),
                Event(
                    name="Event 1",
                    transactions=[
                        Transaction(name="Event 2 Transaction 1",
                                    cows=[Cow(tag_number="TheTransactionCow")]),
                        Transaction(name="Event 2 Transaction 2",
                                    cows=[Cow(tag_number="TheTransactionCow")]),
                    ])
            ]).get_transactions()
            assert transactions == [
                Transaction(name="Event 1 Transaction 1"),
                Transaction(name="Event 1 Transaction 2"),
                Transaction(name="Event 2 Transaction 1"),
                Transaction(name="Event 2 Transaction 2"),
            ]

        def test_get_transactions_excluding_cow(self, app, mocker):
            """
            Make sure that get_transactions returns every transaction
            from every event the cow is involved in.
            """
            transactions = Cow(tag_number="TheTransactionCow", events=[
                Event(
                    name="Event 1",
                    transactions=[
                        Transaction(name="Event 1 Transaction 1"),
                        Transaction(name="Event 1 Transaction 2",
                                    cows=[Cow(tag_number="TheTransactionCow")]),
                    ]),
                Event(
                    name="Event 1",
                    transactions=[
                        Transaction(name="Event 2 Transaction 1",
                                    cows=[Cow(tag_number="TheTransactionCow")]),
                        Transaction(name="Event 2 Transaction 2"),
                    ])
            ]).get_transactions()
            assert transactions == [
                Transaction(name="Event 1 Transaction 2"),
                Transaction(name="Event 2 Transaction 1")
            ]

        def test_get_nonexistant_transactions(self, app, mocker):
            """
            If there are no transactions, get transactions should return an empty list
            """
            transactions = Cow(events=[
                Event(
                    name="Event 1",
                    transactions=[]),
                Event(
                    name="Event 1",
                    transactions=[])
            ]).get_transactions()
            assert transactions == []

    class TestGetTransactionTotal:
        """
        Tests to make sure the model is able to correctly total up all the transactions.
        """
        params = [
            pytest.param([1, 1, 1, 1], 4, id="Identical whole numbers"),
            pytest.param([1, 3, 7, 12], 23, id="Whole numbers"),
            pytest.param([1, -3, -7, 12], 3,
                         id="Positive and negative integers"),
            pytest.param([1.1, 3.6, 7.4, 1.3], 13.4, id="Positive floats"),
            pytest.param([1.1, -3.6, -7.4, 1.3], -8.6,
                         id="Positive and negative floats"),
        ]

        @pytest.mark.parametrize("prices, expected_total", params)
        def test_get_transaction_total(self, app, prices, expected_total):
            """
            Given a cow with several transactions, get transaction total
            should find the total price of all of those transactions.

            This can include positive and negative numbers, integers and floats.
            """
            total = Cow(tag_number="TheTransactionCow", events=[
                Event(
                    name="Event 1",
                    transactions=[
                        Transaction(name="Event 1 Transaction 1",
                                    price=prices[0],
                                    cows=[Cow(tag_number="TheTransactionCow")]),
                        Transaction(name="Event 1 Transaction 2",
                                    price=prices[1],
                                    cows=[Cow(tag_number="TheTransactionCow")]),
                    ]),
                Event(
                    name="Event 1",
                    transactions=[
                        Transaction(name="Event 2 Transaction 1",
                                    price=prices[2],
                                    cows=[Cow(tag_number="TheTransactionCow")]),
                        Transaction(name="Event 2 Transaction 2",
                                    price=prices[3],
                                    cows=[Cow(tag_number="TheTransactionCow")]),
                    ])
            ]).get_transaction_total()
            assert total == expected_total

    class TestGetBirthdate:
        """
        Tests retrieving the birthdate from the born event if it exists.
        """

        def test_get_birthdate_correctly(self, app):
            """
            If a cow has a "Born" event, get_birthdate returns that event's date.
            """
            birthdate = Cow(events=[
                Event(
                    name="Born",
                    date="1970-01-01"
                )
            ]).get_birthdate()
            assert birthdate == "1970-01-01"

        def test_get_birthdate_birth_event(self, app):
            """
            If it doesn't have "Born" event (with that exact name),
            get_birthdate should return None.
            """
            birthdate = Cow(events=[
                Event(
                    name="Birth",
                    date="1970-01-01"
                )
            ]).get_birthdate()
            assert birthdate is None

        def test_get_birthdate_calved_event(self, app):
            """
            If it doesn't have "Born" event (with that exact name),
            get_birthdate should return None.
            """
            birthdate = Cow(events=[
                Event(
                    name="Calved",
                    date="1970-01-01"
                )
            ]).get_birthdate()
            assert birthdate is None

        def test_get_birthdate_multiple_born_events(self, app):
            """
            If it doesn't have "Born" event (with that exact name),
            get_birthdate should return None.
            """
            birthdate = Cow(events=[
                Event(
                    name="Born",
                    date="1970-01-01"
                ),
                Event(
                    name="Born",
                    date="1970-01-02"
                )
            ]).get_birthdate()
            assert birthdate == "1970-01-01"

    class TestSearch:
        """
        Tests the search function

        # TODO: Tests for dam and sire filtration, filter case-sensitivity
        """
        params = [
            pytest.param(Cow(
                tag_number="doesn't matter", owner="Old MacDonald"),
                "", {}, True, id="Empty query, no filters"),
            pytest.param(Cow(
                tag_number="example tag number"),
                "tag number", {}, True, id="Query match, no filters"),
            pytest.param(Cow(
                tag_number="example tag number"),
                "tag numberer", {}, False, id="Query mismatch, no filters"),
            pytest.param(Cow(
                tag_number="example tag number"),
                "TAG NUMBER", {}, True, id="Case-insensitive query match, no filters"),
            pytest.param(Cow(
                tag_number="example tag number", owner="Old MacDonald"),
                "tag number", {"owners": "Young Macdonald"}, False,
                id="Query match, owner filter mismatch"),
            pytest.param(Cow(
                tag_number="example tag number", owner="Old MacDonald"),
                "", {"owners": "Young Macdonald"}, False, id="Empty query, owner filter mismatch"),
            pytest.param(Cow(
                tag_number="example tag number", owner="Old MacDonald"),
                "", {"owners": ["Old MacDonald"]}, True, id="Empty query, owner filter match"),
            pytest.param(Cow(
                tag_number="example tag number", owner="Old MacDonald", sex="Bull"),
                "", {"owners": ["Old MacDonald"], "sexes": ["Cow"]}, False,
                id="Empty query, owner filter match, sex filter mismatch"),
            pytest.param(Cow(
                tag_number="123"),
                "", {"tags": ["1"]}, True, id="Empty query, tag match"),
            pytest.param(Cow(
                tag_number="123"),
                "", {"tags": ["2"]}, False, id="Empty query, tag mismatch"),
        ]
