"""
Module to test the web installer
"""
import json
import pytest
from werkzeug.security import check_password_hash


@pytest.fixture
def mock_no_database(mocker):
    """
    Fixture to mock a missing database.

    TODO Remove or move.
    """
    def fake_exists(path):
        if path == "cattle.db":
            return False
    mocker.patch("routes.web.web_installer.os.path.exists", fake_exists)


@pytest.fixture
def mock_database_exists(mocker):
    """
    Fixture to mock database existing.

    TODO Remove or move.
    """
    def fake_exists(path):
        if path == "cattle.db":
            return True
    mocker.patch("routes.web.web_installer.os.path.exists", fake_exists)


class TestInstall:
    """
    Tests the main install route
    """

    def test_no_database(self, client, mock_no_database):
        """
        Make sure installer prompts to create the database if it doesn't exist.
        """
        response = client.get("/install/")
        assert response.status_code == 200
        assert b"Database" in response.data
        assert b"Config" not in response.data

    def test_no_config(self, client, mock_database_exists, mocker):
        """
        Make sure installer creates the config if it doesn't exist.
        """
        mock_open = mocker.MagicMock()  # mock for open
        mock_file = mocker.MagicMock()  # mock for file returned from open
        mock_open.return_value.__enter__.side_effect = [
            FileNotFoundError, mock_file
        ]
        mocker.patch("routes.web.web_installer.open", mock_open)
        response = client.get("/install/")
        mock_file.write.assert_called_once_with(
            "{}")
        assert response.status_code == 200
        assert b"Config" in response.data
        assert b"Database" not in response.data

    def test_config_no_users(self, client, mock_database_exists, mocker):
        """
        Make sure the installer prompts to create a new user if the config is empty.
        """
        mock_open = mocker.MagicMock()
        mock_open.return_value.__enter__.return_value.read.return_value = "{}"
        mocker.patch("routes.web.web_installer.open", mock_open)
        response = client.get("/install/")
        mock_open.return_value.__enter__.return_value.read.assert_called_once()
        mock_open.return_value.__enter__.return_value.write.assert_not_called()
        assert response.status_code == 200
        assert b"Create User" in response.data
        assert b"Name" in response.data
        assert b"Password" in response.data
        assert b"Confirm Password" in response.data
        assert b"Select a config.json file" not in response.data
        assert b"Database" not in response.data

    def test_config_wan_lan(self, client, mock_database_exists, mocker):
        """
        Make sure the installer prompts the user to choose wan/lan mode if they haven't yet.
        """
        mock_open = mocker.MagicMock()
        mock_file = mocker.MagicMock()
        mock_open.return_value.__enter__.return_value = mock_file
        mock_file.read.return_value = '{"users":[{"username":"me","hashed_password":"something"}]}'
        mocker.patch("routes.web.web_installer.open", mock_open)
        response = client.get("/install/")
        mock_open.return_value.__enter__.return_value.read.assert_called_once()
        mock_open.return_value.__enter__.return_value.write.assert_not_called()
        assert response.status_code == 200
        assert b"Set Up Network" in response.data
        assert b"Online Mode" in response.data
        assert b"Offline Mode" in response.data
        assert b"{ssid}" not in response.data

    def test_everything_set_up(self, client, mock_database_exists, mocker):
        """
        Make sure that the installer shows server info if everything is already set up.
        """
        mock_open = mocker.MagicMock()
        mock_file = mocker.MagicMock()
        mock_open.return_value.__enter__.return_value = mock_file
        mock_file.read.return_value = json.dumps({
            "users": [{
                "username": "me",
                "hashed_password": "something"
            }],
            "using_wan": "false"
        })
        mocker.patch("routes.web.web_installer.open", mock_open)
        response = client.get("/install/")
        mock_open.return_value.__enter__.return_value.read.assert_called_once()
        mock_open.return_value.__enter__.return_value.write.assert_not_called()
        assert response.status_code == 302
        assert "/install/show_server_info" in response.location


class TestDatabaseSetup:
    """
    Tests setting up the database
    """

    def test_get_import_database(self, client):
        """
        Make sure that GET requests to / install/import_database are redirected to / install
        """
        response = client.get("/install/import_database")
        assert response.status_code == 302
        assert "/install" in response.location

    def test_post_import_database_incorrectly(self, client, mock_no_database, mocker):
        """
        If the user tries to import a file that's not named cattle.db, don't let them.
        """
        mock_request = mocker.MagicMock()
        mock_request.method = "POST"

        mock_file = mocker.MagicMock()
        mock_file.filename = "my_cows.xlsx"
        mock_request.files = {"file": mock_file}
        mocker.patch("routes.web.web_installer.request", mock_request)

        response = client.post(
            "/install/import_database", follow_redirects=True)
        mock_file.save.assert_not_called()
        assert response.status_code == 200
        assert b"Database must be a valid CattleDB database" in response.data
        assert b"Sorry, spreadsheets are not allowed!" in response.data
        # Check for duplicate "Sorry..." messages
        assert len(str(response.data).split(
            "Sorry, spreadsheets are not allowed!")) == 2

    def test_post_import_database_correctly(self, client, mocker):
        """
        If you upload a "cattle.db" file, it should import it and use
        it as the working database.
        """
        mock_request = mocker.MagicMock()
        mock_request.method = "POST"

        mock_file = mocker.MagicMock()
        mock_file.filename = "cattle.db"
        mock_request.files = {"file": mock_file}
        mocker.patch("routes.web.web_installer.request", mock_request)

        response = client.post("/install/import_database")
        mock_file.save.assert_called_once_with("cattle.db")
        assert response.status_code == 302
        assert "/install" in response.location

    def test_create_database(self, client, mock_no_database, mocker):
        """
        Make sure that a new database is created correctly during the install.
        """
        mock_open = mocker.MagicMock()
        mock_open.return_value.__enter__.return_value.read.return_value = json.dumps({
            "users": [{
                "username": "me",
                "hashed_password": "something"
            }],
            "using_wan": "false"
        })
        mocker.patch("routes.web.web_installer.open", mock_open)
        mocker.patch("routes.web.web_installer.db")
        response = client.get("/install/create_database")
        mock_open.return_value.__enter__.return_value.read.assert_not_called()
        mock_open.return_value.__enter__.return_value.write.assert_called_once_with(
            "")
        assert response.status_code == 302
        assert "/install" in response.location


class TestImportConfig:
    """
    Tests importing an existing config
    """

    def test_get_import_config(self, client):
        """
        Make sure that GET requests to / install/import_config are redirected to / install
        """
        response = client.get("/install/import_config")
        assert response.status_code == 302
        assert "/install" in response.location

    def test_post_import_config_correctly(self, client, mocker):
        """
        If you upload a "config.json" file, it should import it and use
        it as the working config.
        """
        mock_request = mocker.MagicMock()
        mock_request.method = "POST"

        mock_file = mocker.MagicMock()
        mock_file.filename = "config.json"
        mock_request.files = {"file": mock_file}
        mocker.patch("routes.web.web_installer.request", mock_request)

        response = client.post("/install/import_config")

        mock_file.save.assert_called_once_with("config.json")
        assert response.status_code == 302
        assert "/install" in response.location

    def test_post_import_config_incorrectly(self, client, mocker):
        """
        If the user tries to import a spreadsheet, it should warn them
        that that's not a valid cattle.db file.
        """
        mock_request = mocker.MagicMock()
        mock_request.method = "POST"

        mock_file = mocker.MagicMock()
        mock_file.filename = "somefile.xlsx"
        mock_request.files = {"file": mock_file}
        mocker.patch("routes.web.web_installer.request", mock_request)
        mock_os = mocker.patch("routes.web.web_installer.os")
        mock_os.path.exists.return_value = False
        response = client.post("/install/import_config", follow_redirects=True)

        mock_file.save.assert_not_called()
        assert response.status_code == 200
        assert b"Config file must be a valid CattleDB config" in response.data


class TestCreateCredentials:
    """
    Tests creating new credentials
    """

    def test_get_create_credentials(self, client, mocker):
        """
        Make sure that / install/create_credentials displays the correct page.
        """
        mock_open = mocker.MagicMock()
        mock_open.return_value.__enter__.return_value.read.return_value = "{}"
        mocker.patch("routes.web.web_installer.open", mock_open)
        response = client.get("/install/create_credentials")
        assert response.status_code == 200
        assert b"Create User" in response.data
        assert b"Name" in response.data
        assert b"Password" in response.data
        assert b"Confirm Password" in response.data
        assert b"Select a config.json file" not in response.data
        assert b"Database" not in response.data

    def test_post_create_credentials_mismatched_passwords(self, client, mocker, mock_config_no_users):
        """
        If the user tries to create credentials and the passwords don't match,
        make them try again.
        """
        mock_open, mock_file = mock_config_no_users
        mocker.patch("routes.web.web_installer.open", mock_open)
        response = client.post("/install/create_credentials", data={
            "username": "user",
            "password": "password",
            "confirm_password": "a different password"
        }, follow_redirects=True)
        assert response.status_code == 200
        assert b"Passwords do not match" in response.data

    @pytest.mark.slow
    def test_post_create_credentials(self, client, mocker):
        """
        If the user tries to create a new user, it should add the username and
        password hash to the config file.
        """
        mock_open = mocker.MagicMock()
        mock_open.return_value.__enter__.return_value.read.return_value = "{}"
        mocker.patch("routes.web.web_installer.open", mock_open)

        response = client.post("/install/create_credentials", data={
            "username": "user",
            "password": "password",
            "confirm_password": "password"
        }, follow_redirects=True)
        mock_open.return_value.__enter__.return_value.read.assert_called_once()
        mock_open.return_value.__enter__.return_value.write.assert_called_once()
        written_string = str(
            mock_open.return_value.__enter__.return_value.write.call_args.args[0])
        written_json = json.loads(written_string)
        assert written_json.get("users", False)
        assert check_password_hash(
            written_json["users"][0]["hashed_password"], "password")
        assert response.status_code == 200
        assert b"Create Additional Users" in response.data

    def test_post_create_existing_credentials(self, client, mocker):
        """
        If the user tries to create an account with an existing username, don't
        let them.
        """
        mock_open = mocker.MagicMock()
        mock_open.return_value.__enter__.return_value.read.return_value = json.dumps({"users": [{
            "username": "existing_user",
            "hashed_password": "password"
        }]})
        mocker.patch("routes.web.web_installer.open", mock_open)

        response = client.post("/install/create_credentials", data={
            "username": "existing_user",
            "password": "password",
            "confirm_password": "password"
        }, follow_redirects=True)
        mock_open.return_value.__enter__.return_value.read.assert_called()
        mock_open.return_value.__enter__.return_value.write.assert_not_called()
        assert response.status_code == 200
        assert (b"Username existing_user has already been created. "
                b"Please choose a different username.") in response.data


class TestSetupOnlineMode:
    """
    Tests setting up online mode.
    """

    def test_windows_no_upnp_wizard(self, client, mocker):
        """
        If the user tries to setup online mode without UPnP wizard,
        make sure they're prompted to install it.
        """
        mocker.patch(
            "routes.web.web_installer.upnp_wizard_installed", return_value=False)
        mocker.patch("routes.web.web_installer.platform.system",
                     return_value="Windows")

        response = client.get("/install/setup_online_mode")

        assert response.status_code == 200
        assert b"Install UPnP Wizard" in response.data

    def test_upnp_rule_already_exists(self, client, mocker):
        """
        If a "CattleDB" UPnP rule already exists, it should explain that
        UPnP has already been configured.
        """
        mocker.patch(
            "routes.web.web_installer.upnp_wizard_installed", return_value=True)
        mocker.patch("routes.web.web_installer.platform.system",
                     return_value="Windows")
        mocker.patch(
            "routes.web.web_installer.check_for_upnp_rule", return_value=True)

        response = client.get("/install/setup_online_mode")

        assert response.status_code == 200
        assert b"Online Mode Already Configured" in response.data

    def test_upnp_rule_added_successfully(self, client, mocker):
        """
        If add_upnp_rule is called correctly, and it works, make
        sure the user gets a success message.
        """
        mock_open = mocker.MagicMock()  # mock for open
        mock_file = mocker.MagicMock()  # mock for file returned from open
        mock_open.return_value.__enter__.return_value = mock_file
        mock_file.read.return_value = b"{}"
        mocker.patch("routes.web.web_installer.open", mock_open)
        mocker.patch(
            "routes.web.web_installer.upnp_wizard_installed", return_value=True)
        mocker.patch("routes.web.web_installer.platform.system",
                     return_value="Windows")
        mocker.patch("routes.web.web_installer.check_for_upnp_rule",
                     return_value=False)
        mocker.patch("routes.web.web_installer.add_upnp_rule",
                     return_value=True)

        response = client.get("/install/setup_online_mode")

        mock_file.read.assert_called_once()
        mock_file.write.assert_called_once()
        written_string = str(
            mock_file.write.call_args.args[0])
        written_json = json.loads(written_string)
        assert written_json.get("using_wan", False)
        assert response.status_code == 200
        assert b"Online mode configured successfully!" in response.data

    def test_upnp_rule_failed_to_add(self, client, mocker):
        """
        If add_upnp_rule is called correctly, and it doesn't work, make
        sure the user gets a failure message.
        """
        mocker.patch(
            "routes.web.web_installer.upnp_wizard_installed", return_value=True)
        mocker.patch("routes.web.web_installer.platform.system",
                     return_value="Windows")
        mocker.patch("routes.web.web_installer.check_for_upnp_rule",
                     return_value=False)
        mocker.patch("routes.web.web_installer.add_upnp_rule",
                     return_value=False)

        response = client.get("/install/setup_online_mode")

        assert response.status_code == 200
        assert b"Could Not Configure Online Mode" in response.data


class TestSetupOfflineMode:
    """
    Tests setting up offline mode
    """

    def test_setup_offline_mode(self, client, mocker):
        """
        If the user wants offline mode, mae sure the config is updated correctly.
        """
        mock_open = mocker.MagicMock()  # mock for open
        mock_file = mocker.MagicMock()  # mock for file returned from open
        mock_open.return_value.__enter__.return_value = mock_file
        mock_file.read.return_value = b"{}"
        mocker.patch("routes.web.web_installer.open", mock_open)

        response = client.get("/install/setup_offline_mode")

        assert response.status_code == 302
        mock_file.read.assert_called_once()
        mock_file.write.assert_called_once()
        written_string = str(
            mock_file.write.call_args.args[0])
        written_json = json.loads(written_string)
        assert not written_json.get("using_wan", True)


class TestShowServerInfo:
    """
    Tests displaying server information
    """

    def test_show_server_info_using_wan(self, client, mocker):
        """
        Assuming that UPnP was configured successfully, it should display both
        public and private IP addresses.
        """
        mock_open = mocker.MagicMock()  # mock for open
        mock_file = mocker.MagicMock()  # mock for file returned from open
        mock_open.return_value.__enter__.return_value = mock_file
        mock_file.read.return_value = b'{"using_wan":true}'
        mocker.patch("routes.web.web_installer.open", mock_open)

        mocker.patch("routes.web.web_installer.get_network_ssid",
                     return_value="THENAMEOFTHENETWORK")
        mocker.patch("routes.web.web_installer.get_public_ip",
                     return_value="PUBLICIP")
        mocker.patch("routes.web.web_installer.get_private_ip",
                     return_value="PRIVATEIP")

        response = client.get("/install/show_server_info")

        mock_file.read.assert_called_once()
        assert response.status_code == 200
        assert b"you will need to manually configure your network." not in response.data
        assert b"and you set up online mode" in response.data
        assert b"(not THENAMEOFTHENETWORK)" in response.data
        assert b"<a href=\"http://PUBLICIP:" in response.data
        assert b"<a href=\"http://PRIVATEIP:" in response.data

    def test_show_server_info_not_using_wan(self, client, mocker):
        """
        The "Show Server Info" page should explain that the WAN address
        won't work without additional configuration. It should still show
        it since the user may be port forwarding.
        """
        mock_open = mocker.MagicMock()  # mock for open
        mock_file = mocker.MagicMock()  # mock for file returned from open
        mock_open.return_value.__enter__.return_value = mock_file
        mock_file.read.return_value = b'{"using_wan":false}'
        mocker.patch("routes.web.web_installer.open", mock_open)

        mocker.patch("routes.web.web_installer.get_network_ssid",
                     return_value="THENAMEOFTHENETWORK")
        mocker.patch("routes.web.web_installer.get_public_ip",
                     return_value="PUBLICIP")
        mocker.patch("routes.web.web_installer.get_private_ip",
                     return_value="PRIVATEIP")

        response = client.get("/install/show_server_info")

        mock_file.read.assert_called_once()
        assert response.status_code == 200

        assert b"you will need to manually configure your network" in response.data
        assert b"and you set up online mode" not in response.data
        assert b"(not THENAMEOFTHENETWORK)" in response.data
        assert b"<a href=\"http://PUBLICIP:" in response.data
        assert b"<a href=\"http://PRIVATEIP:" in response.data
