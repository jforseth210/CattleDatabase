"""
Fixtures for use in various unit tests.
"""
import json

import pytest

from helper.setup_utils import CURRENT_VERSION
from app import create_app
from models.database import db
from models.cow import get_cow_from_tag
from tests.testing_models import create_testing_objects


@pytest.fixture(scope="session")
def app():
    """
    pytest-flask uses this, something to do with application context.
    """
    # return main_app
    class TestConfig:
        """
        A flask config for testing purposes. Used below.
        """
        TESTING = True
        DEBUG = True
        SQLALCHEMY_DATABASE_URI = "sqlite:///:memory:"
        SQLALCHEMY_TRACK_MODIFICATIONS = False
        SECRET_KEY = "top secret"
        INERTIA_TEMPLATE = "inertia_base.html"

    testing_app = create_app(TestConfig)
    with testing_app.app_context():
        db.create_all()
        # populate_test_db(db)
    return testing_app


@pytest.fixture()
def _db(app):
    return db


@pytest.fixture(autouse=True)
def mock_logged_in(request, mocker, mock_config_no_users, testing_objects):
    """
    Fake being logged in, except when testing logins,
    then set up a fake account to log into instead.
    """
    if 'notloggedin' in request.keywords:
        mock_user_object = mocker.patch("helper.login_checker.User")
        mock_user_object.query.filter_by.return_value.first.return_value = testing_objects[
            "users"]["Keith Rivers"]
        return
    username = "Keith Rivers"
    mocker.patch("flask_simplelogin.is_logged_in", return_value=True)
    mocker.patch("flask_simplelogin.get_username", return_value=username)


@pytest.fixture(autouse=True)
def disable_update_checker(mocker):
    """
    A simple fixture to prevent the update checker from running while testing code.
    """
    mocker.patch("app.check_for_updates", return_value=(
        False, CURRENT_VERSION, CURRENT_VERSION))


@ pytest.fixture()
def mock_config(mocker):
    """
    A mock for a configuration file
    """
    mock_open = mocker.MagicMock()
    mock_file = mocker.MagicMock()
    mock_open.return_value.__enter__.return_value = mock_file

    mock_file.read.return_value = json.dumps({})
    return mock_open, mock_file


@ pytest.fixture()
def mock_empty_config(mocker):
    """
    A mock for a configuration file with no contents
    """
    mock_open = mocker.MagicMock()
    mock_file = mocker.MagicMock()
    mock_open.return_value.__enter__.return_value = mock_file

    mock_file.read.return_value = "{}"
    return mock_open, mock_file


@pytest.fixture()
def testing_objects():
    return create_testing_objects()


@pytest.fixture()
def fake_get_cow_from_tag(testing_objects):
    return lambda tag: testing_objects["cows"].get(tag, None)


@ pytest.fixture()
def mock_config_no_users(mocker):
    """
    A mock for a configuration file with no users
    """
    mock_open = mocker.MagicMock()
    mock_file = mocker.MagicMock()
    mock_open.return_value.__enter__.return_value = mock_file

    mock_file.read.return_value = json.dumps({
        "users": []
    })
    return mock_open, mock_file
