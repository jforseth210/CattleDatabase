"""
This module is responsible for testing the cows module of the web interface.
"""
from models.cow import get_cow_from_tag


class TestAdd:
    """
    This class is responsible for testing adding a cow to CattleDB via the web interface.
    """

    def test_add_no_dam_sire_or_events(self, client, mocker):
        """
        This tests the add cow route under very basic conditions.
        No dam or sire. No calved event for the dam. No born event for the calf

        It should create the cow and add it to the database with all of the correct information.
        """
        mock_cow = mocker.patch("routes.web.cows.Cow")
        mock_db = mocker.patch("routes.web.cows.db")
        response = client.post(
            "/cows/add",
            headers={"Referer": "/cows"},
            data={
                "tag_number": "947",
                "owner": "Keith Rivers",
                "sex": "Steer",
                "description": "Generated for testing purposes in test_add_no_dam_sire_or_events",
                "born_event": False,
                "calved_event": False
            }
        )
        mock_cow.assert_called_once_with(
            dam_id=None,
            sire_id=None,
            tag_number="947",
            owner="Keith Rivers",
            sex="Steer",
            description="Generated for testing purposes in test_add_no_dam_sire_or_events"
        )
        mock_db.session.add.assert_called_once()
        mock_db.session.commit.assert_called_once()
        assert response.status_code == 302
        assert "/cows" in response.location

    def test_add_dam_and_sire_no_events(self, app, client, mocker, fake_get_cow_from_tag):
        """
        This tests the add cow route with specified parents.
        No calved event for the dam. No born event for the calf

        It should create the cow and add it to the database with all of the correct information.
        """
        mock_db = mocker.patch("routes.web.cows.db")
        mock_cow = mocker.patch("routes.web.cows.Cow")
        mocker.patch("routes.web.cows.get_cow_from_tag",
                     side_effect=fake_get_cow_from_tag)
        mocker.patch("helper.api_helper_functions.get_cow_from_tag",
                     side_effect=fake_get_cow_from_tag)
        response = client.post(
            "/cows/add",
            headers={"Referer": "/cows"},
            data={
                "dam": "567",
                "sire": "Frankenstein",
                "tag_number": "947",
                "owner": "Keith Rivers",
                "sex": "Steer",
                "description": "Generated for testing purposes in test_add_dam_and_sire_no_events",
                "born_event": False,
                "calved_event": False
            }
        )
        mock_cow.assert_called_once_with(
            dam_id=1,
            sire_id=7,
            tag_number="947",
            owner="Keith Rivers",
            sex="Steer",
            description="Generated for testing purposes in test_add_dam_and_sire_no_events"
        )
        mock_db.session.add.assert_called_once()
        mock_db.session.commit.assert_called_once()
        assert response.status_code == 302
        assert "/cows" in response.location

    def test_add_dam_sire_and_events(self, client, mocker, testing_objects, fake_get_cow_from_tag):
        """
        Test adding a cow with a dam, sire, and "born" and "calved" events

        It should create the cow and add it to the database with all of the correct information.
        """
        mock_db = mocker.patch("routes.web.cows.db")
        mock_cow = mocker.patch("routes.web.cows.Cow")
        mock_cow.return_value = "created cow object"
        mock_event = mocker.patch("routes.web.cows.Event")
        mocker.patch("routes.web.cows.get_cow_from_tag",
                     side_effect=fake_get_cow_from_tag)
        mocker.patch("helper.api_helper_functions.get_cow_from_tag",
                     side_effect=fake_get_cow_from_tag)
        response = client.post(
            "/cows/add",
            headers={"Referer": "/cows"},
            data={
                "dam": "567",
                "sire": "Frankenstein",
                "tag_number": "947",
                "owner": "Keith Rivers",
                "sex": "Steer",
                "description": "Generated for testing purposes in test_add_dam_sire_and_events",
                "born_event_enabled": "on",
                "calved_event_enabled": "on",
                "date": "1970-01-01"
            }
        )
        mock_cow.assert_called_once_with(
            dam_id=1,
            sire_id=7,
            tag_number="947",
            owner="Keith Rivers",
            sex="Steer",
            description="Generated for testing purposes in test_add_dam_sire_and_events"
        )
        assert mock_event.call_count == 2
        assert mock_db.session.add.call_count == 3
        assert response.status_code == 302
        assert "/cows" in response.location

    def test_add_not_applicable_cow(self, client, mocker, testing_objects):
        """
        This method tests adding a cow named "Not Applicable", which shouldn't
        be allowed because that string is used to identify when a dam or sire is
        unspecificied.
        """

        response = client.post(
            "/cows/add",
            headers={"Referer": "/cows"},
            data={
                "dam": "Not Applicable",
                "sire": "Not Applicable",
                "tag_number": "Not Applicable",
                "owner": "Keith Rivers",
                "sex": "Steer",
                "description": "Generated for testing purposes in test_add_not_applicable_cow",
                "born_event": False,
                "calved_event": False,
            }
        )
        assert get_cow_from_tag("Not Applicable") is None
        assert response.status_code == 400


class TestAddParent:
    """
    This class is responsible for all of the tests related to the add parent route
    """

    def test_add_dam(self, client, mocker, testing_objects, fake_get_cow_from_tag):
        """
        This method tests adding a sire to a calf
        """
        mocker.patch("routes.web.cows.Cow")
        mocker.patch(
            "routes.web.cows.get_cow_from_tag", side_effect=fake_get_cow_from_tag)
        testing_objects["cows"]["921"].dam_id = None
        testing_objects["cows"]["921"].sire_id = None
        response = client.post(
            "/cows/add_parent",
            headers={"Referer": "/cows"},
            data={
                "tag_number": "921",
                "parent_type": "Dam",
                "parent_tag": "567",
            }
        )

        assert testing_objects["cows"]["921"].dam_id == testing_objects["cows"]["567"].cow_id
        assert testing_objects["cows"]["921"].sire_id is None

        assert response.status_code == 302
        assert "/cows" in response.location

    def test_add_sire(self, client, mocker, testing_objects):
        """
        This method tests adding a sire to a calf
        """
        mocker.patch("routes.web.cows.Cow")
        mocker.patch("routes.web.cows.get_cow_from_tag", side_effect=[
                     testing_objects["cows"]["921"], testing_objects["cows"]["Frankenstein"]])
        testing_objects["cows"]["921"].dam_id = None
        testing_objects["cows"]["921"].sire_id = None
        response = client.post(
            "/cows/add_parent",
            headers={"Referer": "/cows"},
            data={
                "tag_number": "921",
                "parent_type": "Sire",
                "parent_tag": "Frankenstein",
            }
        )

        assert testing_objects["cows"]["921"].dam_id is None
        assert testing_objects["cows"]["921"].sire_id == testing_objects["cows"]["Frankenstein"].cow_id
        assert response.status_code == 302
        assert "/cows" in response.location


class TestCowExists:
    """
    Tests checking whether or not a tag number is taken
    """

    def test_existant_cow_exists(self, app, client, mocker, testing_objects):
        """
        Tests checking whether a cow that exists exists.
        """
        mocker.patch("routes.web.cows.get_cow_from_tag",
                     return_value=testing_objects["cows"]["567"])
        response = client.get("/cows/exists/567")
        assert response.status_code == 200
        assert response.data == b"True"

    def test_nonexistant_cow_exists(self, app, client, mocker):
        """
        Tests checking whether a cow that exists exists.
        """
        mocker.patch("routes.web.cows.get_cow_from_tag", return_value=None)
        response = client.get("/cows/exists/567")
        assert response.status_code == 404
        assert response.data == b"False"


class TestGetCow:
    """
    This class is responsible for testing fetching a single cow.
    """

    def test_get_cow(self, app, client, mocker, testing_objects, fake_get_cow_from_tag):
        """
        This method tests basic usage of the get cow route
        """
        mocker.patch("routes.web.cows.get_cow_from_tag",
                     side_effect=fake_get_cow_from_tag)
        mock_event_object = mocker.patch("routes.web.cows.Event")
        mock_event_object.query.all.return_value = [
            testing_objects["events"]["branded"],
            testing_objects["events"]["calved"]
        ]
        mocker.spy(testing_objects["cows"]["567"], 'serialize')

        response = client.get(
            f"/cows/cow/{testing_objects['cows']['567'].tag_number}")
        assert response.status_code == 200
        assert testing_objects["cows"]["567"].serialize.call_count > 0

    def test_get_non_existant_cow(self, app, client, mocker):
        """
        This method tests basic usage of the get cow route
        """
        response = client.get(
            "/cows/cow/Nonexistant",
            headers={"Referer": "/cows"}
        )
        assert response.status_code == 404


class TestChangeTag:
    """
    Tests changing a cow's tag number
    """

    def test_change_tag(self, client, mocker, testing_objects):
        """
        Tests changing a cows tag under basic conditions
        """
        mocker.patch("routes.web.cows.db")
        mocker.patch("routes.web.cows.get_cow_from_tag",
                     return_value=testing_objects["cows"]["921"])

        response = client.post("/cows/change_tag", data={
            "old_tag": "921",
            "new_tag": "920"
        })
        assert response.status_code == 302
        assert testing_objects["cows"]["921"].tag_number == "920"

    def test_change_tag_old_doesnt_exist(self, client, mocker):
        """
        Tests changing a cows tag when the cow we're trying to change doesn't exist
        """
        mocker.patch("routes.web.cows.get_cow_from_tag", return_value=None)

        response = client.post("/cows/change_tag", data={
            "old_tag": "The ProfitaBULL",
            "new_tag": "920"
        })
        assert response.status_code == 404

    def test_change_tag_with_weird_unicode(self, client, mocker, testing_objects):
        """
        Tests changing the tag number to something weird
        """
        mocker.patch("routes.web.cows.get_cow_from_tag",
                     return_value=testing_objects["cows"]["921"])

        response = client.post("/cows/change_tag", data={
            "old_tag": "921",
            "new_tag": "♠♥♦♣"
        })
        assert testing_objects["cows"]["921"].tag_number == "♠♥♦♣"
        assert response.status_code == 302


class TestChangeDescription:
    """
    Tests changing the description of a cow
    """

    def test_change_description(self, client, mocker, testing_objects):
        """
        Tests changing the description under basic conditions
        """
        mocker.patch("routes.web.cows.get_cow_from_tag",
                     return_value=testing_objects["cows"]["921"])

        response = client.post(
            "/cows/change_description",
            headers={"Referer": "/cows/cow/921"},
            data={
                "tag_number": "921",
                "description": "A generic steer calf, but with an updated description!"
            }
        )
        assert response.status_code == 302
        assert testing_objects["cows"]["921"].description == "A generic steer calf, but with an updated description!"

    def test_change_description_with_weird_unicode(self, client, mocker, testing_objects):
        """
        Tests changing the description to something weird
        """
        mocker.patch("routes.web.cows.get_cow_from_tag",
                     return_value=testing_objects["cows"]["921"])

        response = client.post(
            "/cows/change_description",
            headers={"Referer": "/cows/cow/921"},
            data={
                "tag_number": "921",
                "description": "Unicode stuff: 😀😁😂🤣😃"
            }
        )
        assert testing_objects["cows"]["921"].description == "Unicode stuff: 😀😁😂🤣😃"
        assert response.status_code == 302

    def test_change_description_tag_doesnt_exist(self, client, mocker):
        """
        Tests changing a cows description when that cow doesn't exist
        """
        mocker.patch("routes.web.cows.get_cow_from_tag", return_value=None)

        response = client.post("/cows/change_description", data={
            "tag_number": "The ProfitaBULL",
            "description": "A magical creature akin to a unicorn"
        })

        assert response.status_code == 404


class TestChangeSex:
    """
    Tests changing the sex of a cow
    (as in singlar for cattle, not as in a female one of the aforementioned)
    """

    def test_change_sex(self, client, mocker, testing_objects):
        """
        Tests changing the sex under basic conditions
        """
        mocker.patch("routes.web.cows.get_cow_from_tag",
                     return_value=testing_objects["cows"]["922"])
        # Sanity check in case test data changes:
        assert testing_objects["cows"]["922"].sex != "Steer"

        response = client.post(
            "/cows/change_sex",
            follow_redirects=True,
            data={
                "tag_number": "922",
                "sex": "Steer"
            }
        )

        assert testing_objects["cows"]["922"].sex == "Steer"
        assert response.status_code == 200

    def test_change_sex_tag_doesnt_exist(self, client, mocker):
        """
        Tests changing a cows sex when that cow doesn't exist
        """
        mocker.patch("routes.web.cows.get_cow_from_tag", return_value=None)
        response = client.post("/cows/change_sex", data={
            "tag_number": "The ProfitaBULL",
            "sex": "Heifer"
        })
        assert response.status_code == 404

    def test_change_sex_to_invalid_sex(self, client, mocker, testing_objects):
        """
        Tests trying to change the sex to an invalid sex
        """
        mocker.patch("routes.web.cows.get_cow_from_tag",
                     return_value=testing_objects["cows"]["922"])

        response = client.post("/cows/change_sex", data={
            "tag_number": "922",
            "sex": "Eh, whatever"
        })
        assert testing_objects["cows"]["922"].sex == "Heifer"
        assert response.status_code == 400


class TestChangeBirthdate:
    """
    Tests updating the birthdate of a cow
    """

    def test_change_birthdate(self, client, mocker, testing_objects):
        """
        Tests changing the date of an existing birth event
        """

        # Make sure conftest is set up correctly
        num_births = 0
        for event in testing_objects["cows"]["576"].events:
            if event.name == "Born":
                num_births += 1
                assert event.date != "1970-02-01"
        assert num_births == 1

        # Patch and make request
        mocker.patch("routes.web.cows.db")
        mocker.patch("routes.web.cows.get_cow_from_tag",
                     return_value=testing_objects["cows"]["576"])
        response = client.post(
            "/cows/set_birthdate",
            data={
                "tag_number": "576",
                "birthdate": "1970-02-01"
            },
            follow_redirects=True
        )

        # Check response
        assert response.status_code == 200

        # Check mocks modified correctly
        num_births = 0
        for event in testing_objects["cows"]["576"].events:
            if event.name == "Born":
                num_births += 1
                assert event.date == "1970-02-01"
        assert num_births == 1

    def test_set_birthdate(self, client, mocker, testing_objects):
        """
        Tests changing the date of an existing birth event
        """
        # Make sure conftest is set up correctly
        for event in testing_objects["cows"]["567"].events:
            assert event.name != "Born"

        # Patch and make request
        mocker.patch("routes.web.cows.db")
        mocker.patch("routes.web.cows.get_cow_from_tag",
                     return_value=testing_objects["cows"]["567"])
        mock_event_object = mocker.patch("routes.web.cows.Event")
        response = client.post(
            "/cows/set_birthdate",
            data={
                "tag_number": "567",
                "birthdate": "1970-02-01"
            },
            follow_redirects=True
        )

        # Check response
        assert response.status_code == 200

        # Check mocks modified correctly
        mock_event_object.assert_called_once_with(
            date="1970-02-01",
            name="Born",
            description="567 was born",
            cows=[testing_objects["cows"]["567"]]
        )

    def test_change_birthdate_nonexistant_cow(self, client, mocker):
        """
        Tests trying to set/change the birthdate of a cow that doesn't exist
        """
        mocker.patch("routes.web.cows.get_cow_from_tag", return_value=None)
        response = client.post(
            "/cows/set_birthdate", data={"tag_number": "567", "birthdate": "1970-02-01"})
        assert response.status_code == 404


class TestTransferOwnership:
    """
    This tests everything to do with transferring ownership via the web interface
    """

    def test_transfer_ownership_without_event(self, client, mocker, testing_objects):
        """
        Tests transferring ownership under basic conditions
        """
        mocker.patch("routes.web.cows.get_cow_from_tag",
                     return_value=testing_objects["cows"]["921"])
        mock_db = mocker.patch("routes.web.cows.db")
        mock_event = mocker.patch("routes.web.cows.Event")
        mock_transaction = mocker.patch("routes.web.cows.Transaction")

        response = client.post(
            "/cows/transfer_ownership",
            headers={"Referer": "/cows/cow/921"},
            data={
                "tag_number": "921",
                "new_owner": "Sofia Olsen",
                "price": 1563,
                "date": "1970-01-01",
                "description": "Transfer 921 from Keith Rivers to Sofia Olsen"
            }
        )
        assert response.status_code == 302
        assert mock_db.session.add.call_count == 0

    def test_transfer_ownership_with_event(self, client, mocker, testing_objects):
        """
        Tests transferring ownership under basic conditions
        """
        mocker.patch("routes.web.cows.get_cow_from_tag",
                     return_value=testing_objects["cows"]["921"])
        mock_db = mocker.patch("routes.web.cows.db")
        mock_event = mocker.patch("routes.web.cows.Event")
        mock_transaction = mocker.patch("routes.web.cows.Transaction")

        response = client.post(
            "/cows/transfer_ownership",
            headers={"Referer": "/cows/cow/921"},
            data={
                "tag_number": "921",
                "new_owner": "Sofia Olsen",
                "price": 1563,
                "date": "1970-01-01",
                "transfer_event_enabled": "on",
                "description": "Transfer 921 from Keith Rivers to Sofia Olsen"
            }
        )
        assert response.status_code == 302
        assert mock_db.session.add.call_count == 1
        testing_objects["cows"]["921"].owner = "Sofia Olsen"
        mock_event.assert_called_once()
        mock_transaction.assert_called_once()

    def test_transfer_ownership_of_nonexistant_cow(self, client, mocker):
        """
        Tests transferring ownership for a cow that doesn't exist
        """
        mocker.patch("routes.web.cows.get_cow_from_tag", return_value=None)
        mocker.patch("routes.web.cows.db")
        mocker.patch("routes.web.cows.Event")
        mocker.patch("routes.web.cows.Transaction")

        response = client.post(
            "/cows/transfer_ownership",
            headers={"Referer": "/cows/cow/921"},
            follow_redirects=True,
            data={
                "tag_number": "",
                "new_owner": "New Owner",
                "price": 19.09,
                "date": "1970-01-01",
                "description": "Description"
            }
        )
        assert response.status_code == 404


class TestArchive:
    """
    This class is responsible for testing archiving a cow.
    """

    def test_archive_unarchived_cow(self, client, mocker, testing_objects):
        """
        Tests taking a cow that's not archived and archiving it.
        """
        mocker.patch("routes.web.cows.get_cow_from_tag",
                     return_value=testing_objects["cows"]["921"])
        assert testing_objects["cows"]["921"].isArchived is False

        response = client.get(
            "/cows/archive",
            follow_redirects=True,
            data={
                "tag_number": "921"
            }
        )

        assert testing_objects["cows"]["921"].isArchived is True
        assert response.status_code == 200

    def test_archive_archived_cow(self, client, mocker, testing_objects):
        """
        Tests taking a cow that is archived and trying to archive it again.
        """
        mocker.patch(
            "routes.web.cows.get_cow_from_tag", return_value=testing_objects["cows"]["215"])

        response = client.get(
            "/cows/archive",
            follow_redirects=True,
            data={
                "tag_number": "Tag Number"
            }
        )
        assert testing_objects["cows"]["215"].isArchived is True
        assert response.status_code == 200

    def test_archive_nonexistant_cow(self, client, mocker):
        """
        Tests trying to archive a cow that doesn't exist.
        """
        mocker.patch("models.cow.get_cow_from_tag", return_value=None)

        response = client.get(
            "/cows/archive",
            follow_redirects=True,
            data={
                "tag_number": "The ProfitaBULL"
            }
        )
        assert response.status_code == 404


class TestUnarchive:
    """
    This class is responsible for testing archiving a cow.
    """

    def test_unarchive_unarchived_cow(self, client, mocker, testing_objects):
        """
        Tests taking a cow that's not archived and unarchiving it.
        """
        assert testing_objects["cows"]["921"].isArchived is False
        mocker.patch("routes.web.cows.get_cow_from_tag",
                     return_value=testing_objects["cows"]["921"])

        response = client.get(
            "/cows/unarchive",
            headers={"Referer": "/cows/cow/921"},
            data={
                "tag_number": "921"
            }
        )
        assert testing_objects["cows"]["921"].isArchived is False
        assert response.status_code == 302

    def test_unarchive_archived_cow(self, client, mocker, testing_objects):
        """
        Tests taking a cow that is archived and trying to unarchive it
        """
        mocker.patch("routes.web.cows.get_cow_from_tag",
                     return_value=testing_objects["cows"]["215"])
        assert testing_objects["cows"]["215"].isArchived is True

        response = client.get(
            "/cows/unarchive",
            follow_redirects=True,
            headers={"Referer": "/cows/cow/921"},
            data={
                "tag_number": "215"
            }
        )
        assert testing_objects["cows"]["215"].isArchived is False
        assert response.status_code == 200

    def test_unarchive_nonexistant_cow(self, client, mocker):
        """
        Tests trying to unarchive a cow that doesn't exist.
        """
        mocker.patch("routes.web.cows.get_cow_from_tag", return_value=None)

        response = client.get(
            "/cows/unarchive",
            follow_redirects=True,
            data={
                "tag_number": "The ProfitaBULL"
            }
        )
        assert response.status_code == 404
