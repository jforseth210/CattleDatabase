"""
Test everything in api.py
"""
import base64
import json
import pytest


class TestCredentials:
    """
    Tests dedicated to logging in
    """
    @pytest.mark.notloggedin
    def test_correct_credentials(self, client):
        """
        This tests if it's possible to log in to the api using basic authentication.
        The api should respond with HTTP 200 and a success message
        """
        auth_token = base64.b64encode(
            b'Keith Rivers:password').decode('utf-8')
        auth_string = f"Basic {auth_token}"

        response = client.post(
            "/api/test_credentials",
            headers={
                "Authorization": auth_string,
                "content-type": "application/json"
            })
        assert response.status_code == 200
        assert response.data == json.dumps(
            {"operation": "test_credentials", "succeeded": "True"}).encode("utf-8")

    @pytest.mark.notloggedin
    def test_incorrect_credentials(self, client):
        """
        This makes sure that only the correct credentials are accepted.
        The api should respond by redirecting to the html login page.
        """
        response = client.post("/api/test_credentials")
        assert response.status_code == 302
        assert "/login/?next=/api/test_credentials" in response.location


class TestGetServerInfo:
    """
    Test fetching server information
    """

    def test_get_server_info(self, client, mocker):
        """
        This tests the server info api. If we give it fake addresses via a mock,
        it should send those fake addresses upon request.
        """
        mocker.patch("routes.api.api.get_private_ip", return_value="PRIVATEIP")
        mocker.patch("routes.api.api.get_public_ip", return_value="PUBLICIP")

        response = client.get("/api/get_server_info")

        assert response.status_code == 200
        assert response.data == json.dumps({
            "LAN_address": "PRIVATEIP",
            "WAN_address": "PUBLICIP"
        }).encode("UTF-8")
