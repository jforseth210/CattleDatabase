"""
Tests running the main module
"""


def test_home(client):
    """
    Make sure that / redirects to /cows.
    """
    response = client.get("/")
    assert response.status_code == 302
    assert "/cows" in response.location
