"""
Responsible for testing everything in the api_transactions module
"""
import json

import pytest


class TestAddTransaction:
    """
    Responsible for testing adding an transaction.
    """

    def test_add(self, client, mocker, testing_objects):
        """
        This tests the add transaction api under very basic conditions.

        It should create the transaction and add it to the database with all of the correct information.
        """
        mock_transaction = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_db = mocker.patch("routes.api.api_transactions.db")
        mock_event_object = mocker.patch("routes.api.api_transactions.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["branded"]
        response = client.post("/api/transactions/add", data=json.dumps({
            "event_id": 2,
            "name": "Predstataarol",
            "description": "An expense",
            "to_from": "Sweeney Supply Co. Inc. Ltd.",
            "price": -1.22
        }))
        mock_transaction.assert_called_once_with(
            event=testing_objects["events"]["branded"],
            cows=testing_objects["events"]["branded"].cows,
            name="Predstataarol",
            description="An expense",
            tofrom="Sweeney Supply Co. Inc. Ltd.",
            price=-1.22
        )
        mock_db.session.add.assert_called_once()
        mock_db.session.commit.assert_called_once()
        assert response.status_code == 200
        assert response.data == json.dumps(
            {"operation": "add_transaction", "result": "success"}).encode("utf-8")


class TestGetTransaction:
    """
    Tests fetching an transaction
    """

    def test_get_transaction(self, client, mocker, testing_objects):
        """
        Tests fetching an transaction
        """
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["vet bill"]

        response = client.get("/api/transactions/transaction", query_string={
            "transaction_id": 2})
        assert response.status_code == 200
        assert json.loads(response.data) == {'cows': [{'sex': 'Steer', 'tag_number': '633'}, {'sex': 'Steer', 'tag_number': '641'}], 'date': '1970-01-02', 'description': 'A very expensive transaction', 'event': {
            'date': '1970-01-02', 'event_id': 0, 'name': 'Calved'}, 'isArchived': False, 'name': 'Vet Bill', 'price': -312.98, 'tofrom': 'Doc Hirshorn', 'transaction_id': 9}

    def test_get_nonexistant_transaction(self, client, mocker):
        """
        Tests trying to retrieve an transaction_id without a corresponding transaction
        """
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = None

        response = client.get("/api/transactions/transaction", query_string={
            "transaction_id": 1})
        assert response.status_code == 404
        assert response.data == b'{"result": "failure", "reason": "Transaction not found"}'


class TestGetTransactionList:
    """
    Tests fetching a list of transactions
    """

    def test_get_transaction_list(self, client, mocker, testing_objects):
        """
        Tests getting a list of transactions
        """
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.all.return_value = [
            testing_objects["transactions"]["vet bill"], testing_objects["transactions"]["medication"]]
        response = client.get("/api/transactions/get_list")
        assert response.status_code == 200
        assert json.loads(response.data) == [{'transaction_id': 9, 'name': 'Vet Bill', 'isArchived': False, 'date': '1970-01-02', 'price': -312.98, 'cows': [
            {'tag_number': '633'}, {'tag_number': '641'}]}, {'transaction_id': 9, 'name': 'Cefimoxin', 'isArchived': False, 'date': '1970-01-01', 'price': -1.4, 'cows': []}]

    def test_get_empty_transaction_list(self, client, mocker):
        """
        Tests getting a list of transactions when there are none
        """
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.all.return_value = []
        response = client.get("/api/transactions/get_list")
        assert response.status_code == 200
        assert response.data == json.dumps([]).encode("utf-8")


class TestGetTransactionCows:
    """
    Tests the route to retrieve an transaction's cows
    """

    def test_get_transaction_cows(self, client, mocker, testing_objects):
        """
        Tests retrieving an transactions cows under normal circumstances.
        """
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["vet bill"]

        response = client.get("/api/transactions/get_cows",
                              query_string={"transaction_id": "2"})
        assert response.status_code == 200
        assert json.loads(response.data) == {'cows': [
            {'tag_number': '633', 'sex': 'Steer'}, {'tag_number': '641', 'sex': 'Steer'}]}

    def test_get_nonexistant_transaction_cows(self, client, mocker):
        """
        Tests retrieving an transactions cows under normal circumstances.
        """
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = None

        response = client.get("/api/transactions/get_cows",
                              query_string={"transaction_id": "2"})
        assert response.status_code == 404
        assert response.data == b'{"result": "failure", "reason": "Transaction not found"}'


class TestTransactionUpdateCows:
    """
    Tests updating the cows associated with an transaction
    """

    def test_transaction_update_cows(self, client, mocker, testing_objects):
        """
        Tests updating an transaction's cows
        """
        mocker.patch("routes.api.api_transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["vet bill"]
        mocker.patch("routes.api.api_transactions.get_cow_from_tag",
                     side_effect=[testing_objects["cows"]["567"], testing_objects["cows"]["576"], testing_objects["cows"]["567"], testing_objects["cows"]["576"]])
        response = client.post("/api/transactions/update_cows", data=json.dumps({
            "transaction_id": 2,
            "cows": ["567", "576"]
        }))
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "update_cows",
            "result": "success"
        }).encode("utf-8")
        assert testing_objects["transactions"]["vet bill"].cows == [
            testing_objects["cows"]["567"], testing_objects["cows"]["576"]]
        assert testing_objects["cows"]["567"] in testing_objects["transactions"]["vet bill"].event.cows
        assert testing_objects["cows"]["576"] in testing_objects["transactions"]["vet bill"].event.cows

    def test_non_existant_transaction_update_cows(self, client, mocker, testing_objects):
        """
        Tests trying to modify an transaction that doesn't exist
        """
        mocker.patch("routes.api.api_transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = None
        mocker.patch("routes.api.api_transactions.get_cow_from_tag",
                     side_effect=[testing_objects["cows"]["567"], testing_objects["cows"]["576"]])
        response = client.post("/api/transactions/update_cows", data=json.dumps({
            "transaction_id": 46546,
            "cows": ["567", "576"]
        }))
        assert response.status_code == 404
        assert response.data == b'{"result": "failure", "reason": "Transaction not found"}'

    def test_transaction_update_to_no_cows(self, client, mocker, testing_objects):
        """
        Tests removing all cows from an transaction
        """
        mocker.patch("routes.api.api_transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["vet bill"]
        mocker.patch(
            "routes.api.api_transactions.get_cow_from_tag", return_value=None)
        response = client.post("/api/transactions/update_cows", data=json.dumps({
            "transaction_id": 2,
            "cows": []
        }))
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "update_cows",
            "result": "success"
        }).encode("utf-8")
        assert testing_objects["transactions"]["vet bill"].cows == []


class TestChangeTransactionName:
    """
    Tests modifying the name of an transaction
    """

    def test_change_transaction_name(self, client, mocker, testing_objects):
        """
        Tests changing the name of an transaction successfully
        """
        assert testing_objects["transactions"]["vet bill"].name != "Incremented number of offspring"
        mocker.patch("routes.api.api_transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["vet bill"]
        response = client.post("/api/transactions/change_name", data=json.dumps({
            "transaction_id": 2,
            "name": "Incremented number of offspring"
        }))
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "change_name",
            "result": "success"
        }).encode("utf-8")
        assert testing_objects["transactions"]["vet bill"].name == "Incremented number of offspring"

    def test_non_existant_transaction_change_name(self, client, mocker):
        """
        Tests trying to modify an nonexistant transaction
        """
        mocker.patch("routes.api.api_transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = None
        response = client.post("/api/transactions/change_name", data=json.dumps({
            "transaction_id": 46546,
            "name": "Incremented number of offspring"
        }))
        assert response.status_code == 404
        assert response.data == b'{"result": "failure", "reason": "Transaction not found"}'


class TestChangeTransactionDescription:
    """
    Tests modifying the description of an transaction
    """

    def test_change_transaction_description(self, client, mocker, testing_objects):
        """
        Tests changing the description of an transaction successfully
        """
        assert testing_objects["transactions"]["vet bill"].description != "A description describing the transaction being described"
        mocker.patch("routes.api.api_transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["vet bill"]
        response = client.post("/api/transactions/change_description", data=json.dumps({
            "transaction_id": 2,
            "description": "A description describing the transaction being described"
        }))
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "change_description",
            "result": "success"
        }).encode("utf-8")
        assert testing_objects["transactions"]["vet bill"].description == "A description describing the transaction being described"

    def test_non_existant_transaction_change_description(self, client, mocker):
        """
        Tests updating an transaction's cows
        """
        mocker.patch("routes.api.api_transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = None
        response = client.post("/api/transactions/change_description", data=json.dumps({
            "transaction_id": 46546,
            "description": "A description describing the transaction being described"
        }))
        assert response.status_code == 404
        assert response.data == b'{"result": "failure", "reason": "Transaction not found"}'


class TestChangeTransactionToFrom:
    """
    Tests modifying the to/from of an transaction
    """

    def test_change_transaction_to_from(self, client, mocker, testing_objects):
        """
        Tests changing the to/from of an transaction successfully
        """
        assert testing_objects["transactions"]["vet bill"].tofrom != "Doc O'Daniels"
        mocker.patch("routes.api.api_transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["vet bill"]
        response = client.post("/api/transactions/change_to_from", data=json.dumps({
            "transaction_id": 2,
            "to_from": "Doc O'Daniels"
        }))
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "change_to_from",
            "result": "success"
        }).encode("utf-8")
        assert testing_objects["transactions"]["vet bill"].tofrom == "Doc O'Daniels"

    def test_non_existant_transaction_change_to_from(self, client, mocker):
        """
        Tests updating an transaction's cows
        """
        mocker.patch("routes.api.api_transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = None
        response = client.post("/api/transactions/change_to_from", data=json.dumps({
            "transaction_id": 46546,
            "to_from": "1970-05-14"
        }))
        assert response.status_code == 404
        assert response.data == b'{"result": "failure", "reason": "Transaction not found"}'


class TestChangeTransactionPrice:
    """
    Tests modifying the price of an transaction
    """

    def test_change_transaction_price(self, client, mocker, testing_objects):
        """
        Tests changing the price of an transaction successfully
        """
        assert testing_objects["transactions"]["vet bill"].price != -256.00
        mocker.patch("routes.api.api_transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["vet bill"]
        response = client.post("/api/transactions/change_price", data=json.dumps({
            "transaction_id": 2,
            "price": -256.00
        }))
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "change_price",
            "result": "success"
        }).encode("utf-8")
        assert testing_objects["transactions"]["vet bill"].price == -256

    def test_non_existant_transaction_change_price(self, client, mocker):
        """
        Tests updating an transaction's cows
        """
        mocker.patch("routes.api.api_transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = None
        response = client.post("/api/transactions/change_price", data=json.dumps({
            "transaction_id": 46546,
            "price": -256.00
        }))
        assert response.status_code == 404
        assert response.data == b'{"result": "failure", "reason": "Transaction not found"}'


class TestArchive:
    """
    This class is responsible for testing archiving a transaction.
    """

    def test_archive_unarchived_transaction(self, client, mocker, testing_objects):
        """
        Tests taking a transaction that's not archived and archiving it.
        """
        mocker.patch("routes.api.api_transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["vet bill"]
        assert testing_objects["transactions"]["vet bill"].isArchived is False

        response = client.post("/api/transactions/archive", data=json.dumps({
            "transaction_id": 2
        }))

        assert testing_objects["transactions"]["vet bill"].isArchived is True
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "archive_transaction",
            "result": "success"
        }) .encode("utf-8")

    def test_archive_archived_transaction(self, client, mocker, testing_objects):
        """
        Tests taking a transaction that is archived and trying to archive it again.
        """
        mocker.patch("routes.api.api_transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["transfer"]

        response = client.post("/api/transactions/archive", data=json.dumps({
            "transaction_id": 3
        }))
        assert testing_objects["transactions"]["transfer"].isArchived is True
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "archive_transaction",
            "result": "success"
        }) .encode("utf-8")

    def test_archive_nonexistant_transaction(self, client, mocker):
        """
        Tests trying to archive a transaction that doesn't exist.
        """
        mocker.patch("routes.api.api_transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = None

        response = client.post("/api/transactions/archive", data=json.dumps({
            "transaction_id": 2642
        }))
        assert response.status_code == 404
        assert response.data == b'{"result": "failure", "reason": "Transaction not found"}'


class TestUnarchive:
    """
    This class is responsible for testing archiving a transaction.
    """

    def test_unarchive_unarchived_transaction(self, client, mocker, testing_objects):
        """
        Tests taking a transaction that's not archived and unarchiving it.
        """
        mocker.patch("routes.api.api_transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["vet bill"]
        assert testing_objects["transactions"]["vet bill"].isArchived is False

        response = client.post("/api/transactions/unarchive", data=json.dumps({
            "transaction_id": 2
        }))
        assert testing_objects["transactions"]["vet bill"].isArchived is False
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "unarchive_transaction",
            "result": "success"
        }) .encode("utf-8")

    def test_unarchive_archived_transaction(self, client, mocker, testing_objects):
        """
        Tests taking a transaction that is archived and trying to unarchive it
        """
        mocker.patch("routes.api.api_transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["transfer"]
        assert testing_objects["transactions"]["transfer"].isArchived is True

        response = client.post("/api/transactions/unarchive", data=json.dumps({
            "transaction_id": 3
        }))
        assert testing_objects["transactions"]["transfer"].isArchived is False
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "unarchive_transaction",
            "result": "success"
        }) .encode("utf-8")

    def test_unarchive_nonexistant_transaction(self, client, mocker):
        """
        Tests trying to unarchive a transaction that doesn't exist.
        """
        mocker.patch("routes.api.api_transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = None

        response = client.post("/api/transactions/unarchive", data=json.dumps({
            "transaction_id": 6546
        }))
        assert response.status_code == 404
        assert response.data == b'{"result": "failure", "reason": "Transaction not found"}'


class TestDeleteTransaction:
    """
    Tests the deletion of an transaction
    """

    def test_delete_transaction(self, client, mocker, testing_objects):
        """
        Tests deleting an transaction
        """
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_db = mocker.patch("routes.api.api_transactions.db")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["vet bill"]

        response = client.post("/api/transactions/delete", data=json.dumps({
            "transaction_id": 2
        }))

        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "delete_transaction",
            "result": "success"
        }).encode("utf-8")
        mock_db.session.delete.assert_called_once()
        mock_db.session.commit.assert_called_once()

    def test_delete_nonexistant_transaction(self, client, mocker):
        """
        Tests removing an transaction that doesn't exist
        """
        mock_db = mocker.patch("routes.api.api_transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.api.api_transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = None

        response = client.post("/api/transactions/delete", data=json.dumps({
            "transaction_id": 2
        }))

        assert response.status_code == 404
        mock_db.session.delete.assert_not_called()
        mock_db.session.delete.assert_not_called()


class TestEmptyRequests:
    """
    Tests to make sure the server correctly handles requests with missing information.
    """
    base = "/api/transactions"
    routes = [
        ("/add", "POST"),
        ("/transaction", "GET"),
        ("/get_cows", "GET"),
        ("/update_cows", "POST"),
        ("/change_name", "POST"),
        ("/change_description", "POST"),
        ("/change_to_from", "POST"),
        ("/change_price", "POST"),
        ("/archive", "POST"),
        ("/unarchive", "POST"),
        ("/delete", "POST")
    ]

    @pytest.mark.parametrize("route, method", routes)
    def test_empty_request(self, client, route, method):
        """
        Sends a request to the route with no args.
        """
        response = None
        if method == "POST":
            response = client.post(f"{self.base}{route}")
        elif method == "GET":
            response = client.get(f"{self.base}{route}")
        else:
            raise AssertionError(
                "Test setup incorrectly. Method should be GET or POST")
        assert response.status_code == 400
        assert json.loads(response.data.decode("utf-8"))["result"] == "failure"
        assert "Missing required argument: " in json.loads(response.data.decode("utf-8")
                                                           )["reason"]

    @pytest.mark.parametrize("route, method", routes)
    def test_request_with_invalid_keys(self, client, route, method):
        """
        Sends a request to the route with no args.
        """
        response = None
        if method == "POST":
            response = client.post(f"{self.base}{route}", data=json.dumps(
                {"Favorite Color": "Red"}))
        elif method == "GET":
            response = client.get(f"{self.base}{route}", query_string={
                                  "Favorite Color": "Red"})
        else:
            raise AssertionError(
                "Test setup incorrectly. Method should be GET or POST")
        assert response.status_code == 400
        assert json.loads(response.data.decode("utf-8"))["result"] == "failure"
        assert "Missing required argument: " in json.loads(response.data.decode("utf-8")
                                                           )["reason"]
