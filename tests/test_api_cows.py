"""
Responsible for testing everything in the api_cows module
"""
import json
import pytest

from models.cow import get_cow_from_tag
from helper.sexes import get_sexes


class TestAdd:
    """
    Responsible for testing adding a cow.
    """

    def test_add_no_dam_sire_or_events(self, client, mocker):
        """
        This tests the add cow api under very basic conditions.
        No dam or sire. No calved event for the dam. No born event for the calf

        It should create the cow and add it to the database with all of the correct information.
        """
        mock_cow = mocker.patch("routes.api.api_cows.Cow")
        mock_db = mocker.patch("routes.api.api_cows.db")
        response = client.post("/api/cows/add", data=json.dumps({
            "dam": "Not Applicable",
            "sire": "Not Applicable",
            "tag_number": "947",
            "owner": "Keith Rivers",
            "sex": "Steer",
            "description": "Generated for testing purposes in test_add_no_dam_sire_or_events",
            "born_event": False,
            "calved_event": False
        }))
        mock_cow.assert_called_once_with(
            dam_id=None,
            sire_id=None,
            tag_number="947",
            owner="Keith Rivers",
            sex="Steer",
            description="Generated for testing purposes in test_add_no_dam_sire_or_events"
        )
        mock_db.session.add.assert_called_once()
        mock_db.session.commit.assert_called_once()
        assert response.status_code == 200
        assert response.data == json.dumps(
            {"operation": "add_cow", "result": "success"}).encode("utf-8")

    def test_add_dam_and_sire_no_events(self, app, client, mocker, testing_objects, fake_get_cow_from_tag):
        """
        This tests the add cow api with specified parents.
        No calved event for the dam. No born event for the calf

        It should create the cow and add it to the database with all of the correct information.
        """
        mock_db = mocker.patch("routes.api.api_cows.db")
        mock_cow = mocker.patch("routes.api.api_cows.Cow")
        mocker.patch("routes.api.api_cows.get_cow_from_tag",
                     side_effect=fake_get_cow_from_tag)
        response = client.post("/api/cows/add", data=json.dumps({
            "dam": "567",
            "sire": "Frankenstein",
            "tag_number": "947",
            "owner": "Keith Rivers",
            "sex": "Steer",
            "description": "Generated for testing purposes in test_add_dam_and_sire_no_events",
            "born_event": False,
            "calved_event": False
        }))
        mock_cow.assert_called_once_with(dam_id=1, sire_id=7, tag_number='947', owner='Keith Rivers',
                                         sex='Steer', description='Generated for testing purposes in test_add_dam_and_sire_no_events')
        mock_db.session.add.assert_called_once()
        mock_db.session.commit.assert_called_once()
        assert response.status_code == 200
        assert response.data == json.dumps(
            {"operation": "add_cow", "result": "success"}).encode("utf-8")

    def test_add_dam_sire_and_events(self, client, mocker, testing_objects, fake_get_cow_from_tag):
        """
        Test adding a cow with a dam, sire, and "born" and "calved" events

        It should create the cow and add it to the database with all of the correct information.
        """
        mock_db = mocker.patch("routes.api.api_cows.db")
        mock_cow = mocker.patch("routes.api.api_cows.Cow")
        mock_cow.return_value = "created cow object"
        mock_event = mocker.patch("routes.api.api_cows.Event")
        mocker.patch("routes.api.api_cows.get_cow_from_tag",
                     side_effect=fake_get_cow_from_tag)
        response = client.post("/api/cows/add", data=json.dumps({
            "dam": "567",
            "sire": "Frankenstein",
            "tag_number": "947",
            "owner": "Keith Rivers",
            "sex": "Steer",
            "description": "Generated for testing purposes in test_add_dam_sire_and_events",
            "born_event_enabled": True,
            "calved_event_enabled": True,
            "date": "1970-01-01"
        }))
        mock_cow.assert_called_once_with(dam_id=1, sire_id=7, tag_number='947', owner='Keith Rivers',
                                         sex='Steer', description='Generated for testing purposes in test_add_dam_sire_and_events')

        assert mock_event.call_count == 2
        assert mock_db.session.add.call_count == 3
        mock_db.session.commit.assert_called_once()
        assert response.status_code == 200
        assert response.data == json.dumps(
            {"operation": "add_cow", "result": "success"}).encode("utf-8")

    def test_add_not_applicable_cow(self, client, mocker):
        """
        This method tests adding a cow named "Not Applicable", which shouldn't
        be allowed because that string is used to identify when a dam or sire is
        unspecificied.
        """

        response = client.post("/api/cows/add", data=json.dumps({
            "dam": "Not Applicable",
            "sire": "Not Applicable",
            "tag_number": "Not Applicable",
            "owner": "Keith Rivers",
            "sex": "Steer",
            "description": "Generated for testing purposes in test_add_not_applicable_cow",
            "born_event": False,
            "calved_event": False,
        }))
        assert get_cow_from_tag("Not Applicable") is None
        assert response.status_code == 400
        assert response.data == json.dumps(
            {"result": "failure", "reason": "Forbidden tag number"}
        ).encode("utf-8")


class TestAddParent:
    """
    This class is responsible for all of the tests related to the add parent api
    """

    def test_add_dam(self, client, mocker, fake_get_cow_from_tag, testing_objects):
        """
        This method tests adding a sire to a calf
        """
        mocker.patch("routes.api.api_cows.Cow")
        mocker.patch(
            "routes.api.api_cows.get_cow_from_tag", side_effect=fake_get_cow_from_tag)
        response = client.post("/api/cows/add_parent", data=json.dumps({
            "tag_number": "921",
            "dam": "567",
            "sire": "Not Applicable",
        }))

        assert fake_get_cow_from_tag(
            "921").dam_id == testing_objects["cows"]["567"].cow_id
        assert testing_objects["cows"]["921"].sire_id is None

        assert response.status_code == 200
        assert response.data == json.dumps(
            {"operation": "add_parent", "result": "success"}).encode("utf-8")

    def test_add_sire(self, client, mocker, testing_objects, fake_get_cow_from_tag):
        """
        This method tests adding a sire to a calf
        """
        mocker.patch("routes.api.api_cows.Cow")
        mocker.patch("routes.api.api_cows.get_cow_from_tag",
                     side_effect=fake_get_cow_from_tag)
        response = client.post("/api/cows/add_parent", data=json.dumps({
            "tag_number": "921",
            "dam": "Not Applicable",
            "sire": "Frankenstein",
        }))

        assert testing_objects["cows"]["921"].dam_id is None
        assert fake_get_cow_from_tag(
            "921").sire_id == testing_objects["cows"]["Frankenstein"].cow_id
        assert response.data == json.dumps(
            {"operation": "add_parent", "result": "success"}).encode("utf-8")

    def test_set_both_na(self, client, mocker, testing_objects, fake_get_cow_from_tag):
        """
        This method tests adding a dam to a calf
        """
        mocker.patch("routes.api.api_cows.get_cow_from_tag",
                     return_value=testing_objects["cows"]["922"])
        response = client.post("/api/cows/add_parent", data=json.dumps({
            "tag_number": "922",
            "dam": "Not Applicable",
            "sire": "Not Applicable",
        }))
        assert testing_objects["cows"]["922"].dam_id is None
        assert testing_objects["cows"]["922"].sire_id is None

        assert response.status_code == 200
        assert response.data == json.dumps(
            {"operation": "add_parent", "result": "success"}).encode("utf-8")


class TestGetCow:
    """
    This class is responsible for testing fetching a single cow.
    """

    def test_get_cow(self, app, client, mocker, fake_get_cow_from_tag):
        """
        This method tests basic usage of the get cow api
        """
        mocker.patch("routes.api.api_cows.get_cow_from_tag",
                     side_effect=fake_get_cow_from_tag)
        response = client.get(
            "/api/cows/cow", query_string={"tag_number": "567"})
        assert response.status_code == 200
        print(response.data)
        assert json.loads(response.data) == {'birthdate': '2015-03-12', 'calves': [{'sex': 'Heifer', 'tag_number': '922'}], 'cow_id': 1, 'dam': None, 'deathdate': '2015-03-13', 'description': 'A fake cow created for testing purposes', 'events': [{'date': '1970-01-01', 'event_id': 2, 'name': 'Branded'}, {
            'date': '1970-01-02', 'event_id': 0, 'name': 'Calved'}], 'isArchived': False, 'owner': 'Keith Rivers', 'sex': 'Cow', 'sire': None, 'tag_number': '567', 'transactions': [{'date': '1970-01-01', 'name': 'Cefimoxin', 'price': -1.4, 'transaction_id': 9}, {'date': '1970-01-02', 'name': 'Vet Bill', 'price': -312.98, 'transaction_id': 9}]}

    def test_get_non_existant_cow(self, app, client, mocker):
        """
        This method tests basic usage of the get cow api
        """
        response = client.get(
            "/api/cows/cow", query_string={"tag_number": "Nonexistant"})
        assert response.status_code == 404
        assert response.data == json.dumps({
            "result": "failure", "reason": "Cow not found"}).encode("utf-8")


class TestGetCowList:
    """
    This class is responsible for testing fetching a list of cows
    """

    def test_get_cow_list(self, client, mocker, testing_objects):
        """
        This tests getting a list of cows in a basic example
        """
        mock_cow = mocker.patch("routes.api.api_cows.Cow")
        mock_cow.query.all.return_value = [
            testing_objects["cows"]["922"], ]
        response = client.get("/api/cows/get_list")
        assert response.status_code == 200
        assert response.data == json.dumps(list(map(
            lambda cow: cow.serialize(["tag_number", "isArchived", "sex"]),
            [
                testing_objects["cows"]["922"], ]
        ))).encode("utf-8")

    def test_get_empty_cow_list(self, client, mocker):
        """
        Tests getting a list of events when there are none
        """
        mock_cow_object = mocker.patch("routes.api.api_cows.Cow")
        mock_cow_object.query.all.return_value = []
        response = client.get("/api/cows/get_list")
        assert response.status_code == 200
        assert response.data == json.dumps([]).encode("utf-8")


class TestGetPossibleParents:
    """
    This tests fetching cows that could possibly
    be a dam or sire.
    """

    def test_get_possible_dams(
        self,
        mocker,
        client, testing_objects
    ):
        """
        This tests getting a list of possible dams in a basic example
        """
        mock_cow = mocker.patch("routes.api.api_cows.Cow")
        mock_cow.query.filter_by.return_value = [
            testing_objects["cows"]["567"],
            testing_objects["cows"]["576"], testing_objects["cows"]["921"], testing_objects["cows"]["922"],  testing_objects["cows"]["B̴̯̟̻͌̊̓̄́͛͝ǒ̸̖̱̽͋v̴̧̻͇̼͍̥̲͙͔̓͗͊̍̚͝į̸̗̩̗̔u̸̧̧͉̫̐̇̇̕̕s̴̨̛̱̮̜ͅ ̵̼̤̪̽́D̶̗͊̿̂̆͗͆́̑e̶̥͎͔̮͕̺̻̼̓̕s̵̢̘̦̈̕t̸̝͍̥͇̱̳̮̬͂́͌̽ͅŗ̵̻͇̬͈̝̭̪̎̔͗̎͆̈́̽̕͝o̶̤̫̐y̷͇͕͂͌ę̸̡̨̮̰̘͝r̸̙̼̖̫̭͈͙̖͈̉̐̏͂́͘ ̷̨̎̉͑̈̿̽̍̇̚o̸̦͎̜̣̳̽͝ͅf̷̫͕̍̀̈́̋ ̸̲̐͑̔͊̈̀͆͝W̶͇̠̟͂́̇̃͆́̎o̵̯̝͔̩̔̊r̸̡͚͓͉̒́̉̀̀̉̎̈́l̶̢̧̛̬̤͕͖̣̼̻͊́̉̀̀͑̄̋d̸̻̑̑̃̋̏̈̃͝s̶̟̤͇͖̐̊͑̿̎͗̋͜"]]

        response = client.get("/api/cows/get_possible_parents",
                              query_string={"parent_type": "dam"})

        assert response.status_code == 200
        assert response.data == json.dumps({
            'parent_type': 'dam',
            'parents': ["567", "576", "922"]
        }).encode("utf-8")

    def test_get_possible_sires(
        self,
        mocker,
        client, testing_objects
    ):
        """
        This tests getting a list of possible dams in a basic example
        """
        mock_cow = mocker.patch("routes.api.api_cows.Cow")
        mock_cow.query.filter_by.return_value = [
            testing_objects["cows"]["576"], testing_objects["cows"]["921"], testing_objects["cows"]["922"],  testing_objects["cows"]["Frankenstein"], testing_objects["cows"]["B̴̯̟̻͌̊̓̄́͛͝ǒ̸̖̱̽͋v̴̧̻͇̼͍̥̲͙͔̓͗͊̍̚͝į̸̗̩̗̔u̸̧̧͉̫̐̇̇̕̕s̴̨̛̱̮̜ͅ ̵̼̤̪̽́D̶̗͊̿̂̆͗͆́̑e̶̥͎͔̮͕̺̻̼̓̕s̵̢̘̦̈̕t̸̝͍̥͇̱̳̮̬͂́͌̽ͅŗ̵̻͇̬͈̝̭̪̎̔͗̎͆̈́̽̕͝o̶̤̫̐y̷͇͕͂͌ę̸̡̨̮̰̘͝r̸̙̼̖̫̭͈͙̖͈̉̐̏͂́͘ ̷̨̎̉͑̈̿̽̍̇̚o̸̦͎̜̣̳̽͝ͅf̷̫͕̍̀̈́̋ ̸̲̐͑̔͊̈̀͆͝W̶͇̠̟͂́̇̃͆́̎o̵̯̝͔̩̔̊r̸̡͚͓͉̒́̉̀̀̉̎̈́l̶̢̧̛̬̤͕͖̣̼̻͊́̉̀̀͑̄̋d̸̻̑̑̃̋̏̈̃͝s̶̟̤͇͖̐̊͑̿̎͗̋͜"]]
        response = client.get("/api/cows/get_possible_parents",
                              query_string={"parent_type": "sire"})
        assert response.status_code == 200
        print(json.loads(response.data))
        assert response.data == json.dumps({
            'parent_type': 'sire',
            'parents': ['Frankenstein', 'B̴̯̟̻͌̊̓̄́͛͝ǒ̸̖̱̽͋v̴̧̻͇̼͍̥̲͙͔̓͗͊̍̚͝į̸̗̩̗̔u̸̧̧͉̫̐̇̇̕̕s̴̨̛̱̮̜ͅ ̵̼̤̪̽́D̶̗͊̿̂̆͗͆́̑e̶̥͎͔̮͕̺̻̼̓̕s̵̢̘̦̈̕t̸̝͍̥͇̱̳̮̬͂́͌̽ͅŗ̵̻͇̬͈̝̭̪̎̔͗̎͆̈́̽̕͝o̶̤̫̐y̷͇͕͂͌ę̸̡̨̮̰̘͝r̸̙̼̖̫̭͈͙̖͈̉̐̏͂́͘ ̷̨̎̉͑̈̿̽̍̇̚o̸̦͎̜̣̳̽͝ͅf̷̫͕̍̀̈́̋ ̸̲̐͑̔͊̈̀͆͝W̶͇̠̟͂́̇̃͆́̎o̵̯̝͔̩̔̊r̸̡͚͓͉̒́̉̀̀̉̎̈́l̶̢̧̛̬̤͕͖̣̼̻͊́̉̀̀͑̄̋d̸̻̑̑̃̋̏̈̃͝s̶̟̤͇͖̐̊͑̿̎͗̋͜']
        }).encode("utf-8")

    def test_get_possible_invalid_parent_type(self, mocker, client):
        """
        This tests getting an invalid type of parent
        """
        mocker.patch("routes.api.api_cows.Cow")
        response = client.get("/api/cows/get_possible_parents",
                              query_string={"parent_type": "Eh, whatever"})
        assert response.status_code == 400
        assert response.data == b'{"result": "failure", "reason": "Invalid parent type"}'

    def test_get_nonexistant_possible_sire(self, mocker, client):
        """
        This tests getting an possible sires if none exist.
        """
        mock_cow = mocker.patch("routes.api.api_cows.Cow")
        mock_cow.query.all.return_value = []
        response = client.get("/api/cows/get_possible_parents",
                              query_string={"parent_type": "sire"})
        assert response.status_code == 404
        assert response.data == json.dumps(
            {
                "parent_type": "sire",
                "parents": []
            }
        ).encode("utf-8")

    def test_get_nonexistant_possible_dam(self, mocker, client):
        """
        This tests getting an possible dam if none exist.
        """
        mock_cow = mocker.patch("routes.api.api_cows.Cow")
        mock_cow.query.all.return_value = []
        response = client.get("/api/cows/get_possible_parents",
                              query_string={"parent_type": "dam"})
        assert response.status_code == 404
        assert response.data == json.dumps(
            {
                "parent_type": "dam",
                "parents": []
            }
        ).encode("utf-8")


class TestGetSexList:
    """
    Tests getting the list of possible sexes
    """

    def test_get_sex_list(self, client):
        """
        Tests basic usage of the get sex list api.
        """
        response = client.get("/api/cows/get_sex_list")
        assert response.status_code == 200
        assert json.loads(response.data.decode("utf-8")) == get_sexes("both")


class TestChangeTag:
    """
    Tests changing a cow's tag number
    """

    def test_change_tag(self, client, mocker, testing_objects, fake_get_cow_from_tag):
        """
        Tests changing a cows tag under basic conditions
        """
        mocker.patch("routes.api.api_cows.db")
        mocker.patch("routes.api.api_cows.get_cow_from_tag",
                     return_value=testing_objects["cows"]["921"])

        response = client.post("/api/cows/change_tag", data=json.dumps({
            "old_tag": "921",
            "new_tag": "920"
        }))
        assert testing_objects["cows"]["921"].tag_number == "920"
        assert response.status_code == 200
        assert response.data == json.dumps(
            {"operation": "change_tag", "result": "success"}).encode("utf-8")

    def test_change_tag_old_doesnt_exist(self, client, mocker):
        """
        Tests changing a cows tag when the cow we're trying to change doesn't exist
        """
        mocker.patch("routes.api.api_cows.get_cow_from_tag", return_value=None)

        response = client.post("/api/cows/change_tag", data=json.dumps({
            "old_tag": "The ProfitaBULL",
            "new_tag": "920"
        }))
        assert response.status_code == 404
        assert response.data == b'{"result": "failure", "reason": "Old tag doesn\'t exist"}'

    def test_change_tag_with_weird_unicode(self, client, mocker, testing_objects,  fake_get_cow_from_tag):
        """
        Tests changing the tag number to something weird
        """
        mocker.patch("routes.api.api_cows.get_cow_from_tag",
                     side_effect=fake_get_cow_from_tag)

        response = client.post("/api/cows/change_tag", data=json.dumps({
            "old_tag": "921",
            "new_tag": "♠♥♦♣"
        }))
        assert testing_objects["cows"]["921"].tag_number == "♠♥♦♣"
        assert response.status_code == 200
        assert response.data == json.dumps(
            {"operation": "change_tag", "result": "success"}).encode("utf-8")


class TestChangeDescription:
    """
    Tests changing the description of a cow
    """

    def test_change_description(self, client, mocker, fake_get_cow_from_tag):
        """
        Tests changing the description under basic conditions
        """
        mocker.patch("routes.api.api_cows.get_cow_from_tag",
                     side_effect=fake_get_cow_from_tag)

        response = client.post("/api/cows/change_description", data=json.dumps({
            "tag_number": "921",
            "description": "A generic steer calf, but with an updated description!"
        }))
        assert fake_get_cow_from_tag(
            "921").description == "A generic steer calf, but with an updated description!"
        assert response.status_code == 200
        assert response.data == json.dumps(
            {"operation": "change_description", "result": "success"}).encode("utf-8")

    def test_change_description_with_weird_unicode(self, client, mocker, fake_get_cow_from_tag):
        """
        Tests changing the description to something weird
        """
        mocker.patch("routes.api.api_cows.get_cow_from_tag",
                     side_effect=fake_get_cow_from_tag)

        response = client.post("/api/cows/change_description", data=json.dumps({
            "tag_number": "921",
            "description": "Unicode stuff: 😀😁😂🤣😃"
        }))
        assert fake_get_cow_from_tag(
            "921").description == "Unicode stuff: 😀😁😂🤣😃"
        assert response.status_code == 200
        assert response.data == json.dumps(
            {"operation": "change_description", "result": "success"}).encode("utf-8")

    def test_change_description_tag_doesnt_exist(self, client, mocker):
        """
        Tests changing a cows description when that cow doesn't exist
        """
        mocker.patch("routes.api.api_cows.get_cow_from_tag", return_value=None)

        response = client.post("/api/cows/change_description", data=json.dumps({
            "tag_number": "The ProfitaBULL",
            "description": "A magical creature akin to a unicorn"
        }))

        assert response.status_code == 404
        print(response.data)
        assert response.data == json.dumps(

            {"result": "failure", "reason": "Cow not found"}
        ).encode("utf-8")


class TestChangeSex:
    """
    Tests changing the sex of a cow
    (as in singlar for cattle, not as in a female one of the aforementioned)
    """

    def test_change_sex(self, client, mocker, testing_objects, fake_get_cow_from_tag):
        """
        Tests changing the sex under basic conditions
        """
        mocker.patch("routes.api.api_cows.get_cow_from_tag",
                     return_value=testing_objects["cows"]["922"])
        # Sanity check in case test data changes:
        assert testing_objects["cows"]["922"].sex != "Steer"

        response = client.post("/api/cows/change_sex", data=json.dumps({
            "tag_number": "922",
            "sex": "Steer"
        }))

        assert testing_objects["cows"]["922"].sex == "Steer"
        assert response.status_code == 200
        assert response.data == json.dumps(
            {"operation": "change_sex", "result": "success"}).encode("utf-8")

    def test_change_sex_tag_doesnt_exist(self, client, mocker):
        """
        Tests changing a cows sex when that cow doesn't exist
        """
        mocker.patch("routes.api.api_cows.get_cow_from_tag", return_value=None)
        response = client.post("/api/cows/change_sex", data=json.dumps({
            "tag_number": "The ProfitaBULL",
            "sex": "Heifer"
        }))
        assert response.status_code == 404
        print(response.data)
        assert response.data == b'{"result": "failure", "reason": "Cow not found"}'

    def test_change_sex_to_invalid_sex(self, client, mocker, testing_objects, fake_get_cow_from_tag):
        """
        Tests trying to change the sex to an invalid sex
        """
        mocker.patch("routes.api.api_cows.get_cow_from_tag",
                     side_effect=fake_get_cow_from_tag)
        assert testing_objects["cows"]["922"].sex == "Heifer"
        response = client.post("/api/cows/change_sex", data=json.dumps({
            "tag_number": "922",
            "sex": "Eh, whatever"
        }))
        assert testing_objects["cows"]["922"].sex == "Heifer"
        assert response.status_code == 400
        assert response.data == b'{"result": "failure", "reason": "Invalid sex"}'


class TestTransferOwnership:
    """
    This tests everything to do with transferring ownership via the api
    """

    def test_transfer_ownership(self, client, mocker, testing_objects):
        """
        Tests transferring ownership under basic conditions
        """
        mocker.patch("routes.api.api_cows.get_cow_from_tag",
                     return_value=testing_objects["cows"]["921"])
        mock_db = mocker.patch("routes.api.api_cows.db")
        mock_event = mocker.patch("routes.api.api_cows.Event")
        mock_transaction = mocker.patch("routes.api.api_cows.Transaction")

        response = client.post("/api/cows/transfer_ownership", data=json.dumps({
            "tag_number": "921",
            "new_owner": "Sofia Olsen",
            "price": 1563,
            "date": "1970-01-01",
            "description": "Transfer 921 from Keith Rivers to Sofia Olsen"
        }))
        assert mock_db.session.add.call_count == 2
        mock_event.assert_called_once()
        mock_transaction.assert_called_once()
        assert response.status_code == 200
        assert response.data == json.dumps(
            {"operation": "transfer_ownership", "result": "success"}).encode("utf-8")

    def test_transfer_ownership_of_nonexistant_cow(self, client, mocker):
        """
        Tests transferring ownership for a cow that doesn't exist
        """
        mocker.patch("routes.api.api_cows.get_cow_from_tag", return_value=None)
        mocker.patch("routes.api.api_cows.db")
        mocker.patch("routes.api.api_cows.Event")
        mocker.patch("routes.api.api_cows.Transaction")

        response = client.post("/api/cows/transfer_ownership", data=json.dumps({
            "tag_number": "",
            "new_owner": "New Owner",
            "price": 19.09,
            "date": "1970-01-01",
            "description": "Description"
        }))
        assert response.status_code == 404
        assert response.data == b'{"result": "failure", "reason": "Cow not found"}'


class TestArchive:
    """
    This class is responsible for testing archiving a cow.
    """

    def test_archive_unarchived_cow(self, client, mocker, testing_objects, fake_get_cow_from_tag):
        """
        Tests taking a cow that's not archived and archiving it.
        """
        mocker.patch("routes.api.api_cows.get_cow_from_tag",
                     return_value=testing_objects["cows"]["921"])
        assert testing_objects["cows"]["921"].isArchived is False

        response = client.post("/api/cows/archive", data=json.dumps({
            "tag_number": "921"
        }))

        assert testing_objects["cows"]["921"].isArchived is True
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "archive_cow",
            "result": "success"
        }) .encode("utf-8")

    def test_archive_archived_cow(self, client, mocker, testing_objects, fake_get_cow_from_tag):
        """
        Tests taking a cow that is archived and trying to archive it again.
        """
        mocker.patch(
            "routes.api.api_cows.get_cow_from_tag", side_effect=fake_get_cow_from_tag)

        response = client.post("/api/cows/archive", data=json.dumps({
            "tag_number": "215"
        }))
        assert testing_objects["cows"]["215"].isArchived is True
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "archive_cow",
            "result": "success"
        }) .encode("utf-8")

    def test_archive_nonexistant_cow(self, client, mocker):
        """
        Tests trying to archive a cow that doesn't exist.
        """
        mocker.patch("routes.api.api_cows.get_cow_from_tag", return_value=None)

        response = client.post("/api/cows/archive", data=json.dumps({
            "tag_number": "The ProfitaBULL"
        }))
        assert response.status_code == 404
        assert response.data == b'{"result": "failure", "reason": "Cow not found"}'


class TestUnarchive:
    """
    This class is responsible for testing archiving a cow.
    """

    def test_unarchive_unarchived_cow(self, client, mocker, testing_objects):
        """
        Tests taking a cow that's not archived and unarchiving it.
        """
        assert testing_objects["cows"]["921"].isArchived is False
        mocker.patch("routes.api.api_cows.get_cow_from_tag",
                     return_value=testing_objects["cows"]["921"])

        response = client.post("/api/cows/unarchive", data=json.dumps({
            "tag_number": "921"
        }))
        assert testing_objects["cows"]["921"].isArchived is False
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "unarchive_cow",
            "result": "success"
        }) .encode("utf-8")

    def test_unarchive_archived_cow(self, client, mocker, testing_objects):
        """
        Tests taking a cow that is archived and trying to unarchive it
        """
        mocker.patch("routes.api.api_cows.get_cow_from_tag",
                     return_value=testing_objects["cows"]["215"])
        assert testing_objects["cows"]["215"].isArchived is True

        response = client.post("/api/cows/unarchive", data=json.dumps({
            "tag_number": "215"
        }))
        assert testing_objects["cows"]["215"].isArchived is False
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "unarchive_cow",
            "result": "success"
        }) .encode("utf-8")

    def test_unarchive_nonexistant_cow(self, client, mocker):
        """
        Tests trying to unarchive a cow that doesn't exist.
        """
        mocker.patch("routes.api.api_cows.get_cow_from_tag", return_value=None)

        response = client.post("/api/cows/unarchive", data=json.dumps({
            "tag_number": "The ProfitaBULL"
        }))
        assert response.status_code == 404
        assert response.data == b'{"result": "failure", "reason": "Cow not found"}'


class TestDelete:
    """
    Tests the api deletion of a cow
    """

    def test_delete_cow(self, client, testing_objects,  mocker):
        """
        Tests the deletion of a cow under basic conditions
        """
        mock_db = mocker.patch("routes.api.api_cows.db")
        mocker.patch("routes.api.api_cows.get_cow_from_tag",
                     return_value=testing_objects["cows"]["921"])

        response = client.post("/api/cows/delete", data=json.dumps({
            "tag_number": "921"
        }))

        mock_db.session.delete.assert_called_once()
        mock_db.session.commit.assert_called_once()
        assert response.status_code == 200
        assert response.data == json.dumps({
            "operation": "delete_cow",
            "result": "success"
        }).encode("utf-8")

    def test_delete_non_existant_cow(self, client, mocker):
        """
        Tests the deletion of a cow under basic conditions
        """
        mocker.patch("routes.api.api_cows.db")
        mock_cow = mocker.patch("routes.api.api_cows.get_cow_from_tag")
        mock_cow.return_value = None

        response = client.post("/api/cows/delete", data=json.dumps({
            "tag_number": "The ProfitaBULL"
        }))

        assert response.status_code == 404
        assert response.data == b'{"result": "failure", "reason": "Cow not found"}'


class TestEmptyRequests:
    """
    Tests to make sure the server correctly handles requests with missing information.
    """
    base = "/api/cows"
    routes = [
        ("/add", "POST"),
        ("/add_parent", "POST"),
        ("/cow", "GET"),
        # ("/get_list", "GET"),
        ("/get_possible_parents", "GET"),
        # ("/get_sex_list", "GET"),
        ("/change_tag", "POST"),
        ("/change_description", "POST"),
        ("/change_sex", "POST"),
        ("/transfer_ownership", "POST"),
        ("/archive", "POST"),
        ("/unarchive", "POST"),
        ("/delete", "POST")
    ]

    @pytest.mark.parametrize("route, method", routes)
    def test_empty_request(self, client, route, method):
        """
        Sends a request to the route with no args.
        """
        response = None
        if method == "POST":
            response = client.post(f"{self.base}{route}")
        elif method == "GET":
            response = client.get(f"{self.base}{route}")
        else:
            raise AssertionError(
                "Test setup incorrectly. Method should be GET or POST")
        assert response.status_code == 400
        assert json.loads(response.data.decode("utf-8"))["result"] == "failure"
        assert "Missing required argument:" in json.loads(response.data.decode("utf-8")
                                                          )["reason"]

    @pytest.mark.parametrize("route, method", routes)
    def test_request_with_invalid_keys(self, client, route, method):
        """
        Sends a request to the route with no args.
        """
        response = None
        if method == "POST":
            response = client.post(f"{self.base}{route}", data=json.dumps(
                {"Favorite Color": "Red"}))
        elif method == "GET":
            response = client.get(f"{self.base}{route}", query_string={
                                  "Favorite Color": "Red"})
        else:
            raise AssertionError(
                "Test setup incorrectly. Method should be GET or POST")
        assert response.status_code == 400
        assert json.loads(response.data.decode("utf-8"))["result"] == "failure"
        assert "Missing required argument:" in json.loads(response.data.decode("utf-8")
                                                          )["reason"]
