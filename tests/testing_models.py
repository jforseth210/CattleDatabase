from models.transaction import serialize_transaction
from models.event import serialize_event
from models.cow import serialize_cow


class FakeCow:
    id_counter = 1

    def __init__(self, tag_number, owner=None, sex=None, description=None, is_archived=False,
                 birthdate=None, deathdate=None, cause_of_death=None):
        self.cow_id = FakeCow.id_counter
        FakeCow.id_counter += 1

        self.dam = None
        self.dam_id = None
        self.sire = None
        self.sire_id = None

        self.calves = []
        self.events = []
        self.transactions = []

        self.tag_number = tag_number
        self.owner = owner
        self.sex = sex
        self.description = description
        self.isArchived = is_archived
        self.birthdate = birthdate
        self.deathdate = deathdate
        self.cause_of_death = cause_of_death

    def __getitem__(self, key):
        return getattr(self, key)

    def add_calf(self, calf):
        self.calves.append(calf)

    def get_birthdate(self):
        return self.birthdate

    def get_deathdate(self):
        return self.deathdate

    def get_calves(self):
        return self.calves

    def set_dam(self, dam):
        self.dam = dam
        self.dam_id = dam.cow_id

    def get_dam(self):
        return self.dam

    def set_sire(self, sire):
        self.sire = sire
        self.sire_id = sire.cow_id

    def get_sire(self):
        return self.sire

    def get_cause_of_death(self):
        return self.cause_of_death

    def add_event(self, event):
        self.events.append(event)

    def add_transaction(self, transaction):
        self.transactions.append(transaction)

    def get_transactions(self):
        return self.transactions

    def serialize(self, fields):
        return serialize_cow(self, fields)


class FakeEvent:
    id_counter = 1

    def __init__(self, name=None, date=None, isArchived=False, description=None):
        self.event_id = FakeEvent.id_counter
        FakeEvent.id_counter += 1

        self.cows = []
        self.transactions = []

        self.name = name
        self.date = date
        self.isArchived = isArchived
        self.description = description

    def add_cow(self, cow):
        self.cows.append(cow)

    def add_transaction(self, transaction):
        self.transactions.append(transaction)

    def serialize(self, fields):
        return serialize_event(self, fields)


class FakeTransaction:
    id_counter = 1

    def __init__(self, name=None, price=None, description=None, tofrom=None, date=None, isArchived=False):
        self.transaction_id = FakeCow.id_counter
        FakeEvent.id_counter += 1

        self.event = None
        self.cows = []

        self.name = name
        self.price = price
        self.description = description
        self.tofrom = tofrom
        self.date = date
        self.isArchived = isArchived

    def get_formatted_price(self):
        return f"${self.price:,.2f}"

    def set_event(self, event):
        self.event = event

    def add_cow(self, cow):
        self.cows.append(cow)

    def serialize(self, fields):
        return serialize_transaction(self, fields)
class FakeUser:
    id_counter = 1
    def __init__(self, username, hashed_password):
        self.user_id = FakeUser.id_counter
        FakeUser.id_counter += 1
        self.username = username
        self.hashed_password = hashed_password

    def __str__(self):
        return self.username


def create_testing_objects():
    FakeCow.id_counter = 0
    FakeEvent.id_counter = 0
    FakeTransaction.id_counter = 0
    fake_cows = {
        "215": FakeCow(
            tag_number="215",
            owner="Auction",
            sex="Cow",
            description="A generic archived cow",
            is_archived=True
        ),
        "567": FakeCow(
            tag_number="567",
            owner="Keith Rivers",
            sex="Cow",
            description="A fake cow created for testing purposes",
            birthdate="2015-03-12",
            deathdate="2015-03-13",
            cause_of_death="Global thermonuclear war"
        ),
        "576": FakeCow(
            tag_number="576",
            owner="Sophia Olsen",
            sex="Cow",

        ),
        "633": FakeCow(tag_number="633", sex="Steer"),
        "641": FakeCow(tag_number="641", sex="Steer"),
        "921": FakeCow(
            tag_number="921",
            owner="Keith Rivers",
            sex="Steer",
            description="A generic steer calf",
            birthdate="2019-01-18"
        ),
        "922": FakeCow(
            tag_number="922",
            owner="Keith Rivers",
            sex="Heifer",
            birthdate="2019-02-15",
        ),
        "Frankenstein": FakeCow(
            tag_number="Frankenstein",
            owner="N/A",
            sex="Bull (AI)"
        ),


        "B̴̯̟̻͌̊̓̄́͛͝ǒ̸̖̱̽͋v̴̧̻͇̼͍̥̲͙͔̓͗͊̍̚͝į̸̗̩̗̔u̸̧̧͉̫̐̇̇̕̕s̴̨̛̱̮̜ͅ ̵̼̤̪̽́D̶̗͊̿̂̆͗͆́̑e̶̥͎͔̮͕̺̻̼̓̕s̵̢̘̦̈̕t̸̝͍̥͇̱̳̮̬͂́͌̽ͅŗ̵̻͇̬͈̝̭̪̎̔͗̎͆̈́̽̕͝o̶̤̫̐y̷͇͕͂͌ę̸̡̨̮̰̘͝r̸̙̼̖̫̭͈͙̖͈̉̐̏͂́͘ ̷̨̎̉͑̈̿̽̍̇̚o̸̦͎̜̣̳̽͝ͅf̷̫͕̍̀̈́̋ ̸̲̐͑̔͊̈̀͆͝W̶͇̠̟͂́̇̃͆́̎o̵̯̝͔̩̔̊r̸̡͚͓͉̒́̉̀̀̉̎̈́l̶̢̧̛̬̤͕͖̣̼̻͊́̉̀̀͑̄̋d̸̻̑̑̃̋̏̈̃͝s̶̟̤͇͖̐̊͑̿̎͗̋͜": FakeCow(



            tag_number="B̴̯̟̻͌̊̓̄́͛͝ǒ̸̖̱̽͋v̴̧̻͇̼͍̥̲͙͔̓͗͊̍̚͝į̸̗̩̗̔u̸̧̧͉̫̐̇̇̕̕s̴̨̛̱̮̜ͅ ̵̼̤̪̽́D̶̗͊̿̂̆͗͆́̑e̶̥͎͔̮͕̺̻̼̓̕s̵̢̘̦̈̕t̸̝͍̥͇̱̳̮̬͂́͌̽ͅŗ̵̻͇̬͈̝̭̪̎̔͗̎͆̈́̽̕͝o̶̤̫̐y̷͇͕͂͌ę̸̡̨̮̰̘͝r̸̙̼̖̫̭͈͙̖͈̉̐̏͂́͘ ̷̨̎̉͑̈̿̽̍̇̚o̸̦͎̜̣̳̽͝ͅf̷̫͕̍̀̈́̋ ̸̲̐͑̔͊̈̀͆͝W̶͇̠̟͂́̇̃͆́̎o̵̯̝͔̩̔̊r̸̡͚͓͉̒́̉̀̀̉̎̈́l̶̢̧̛̬̤͕͖̣̼̻͊́̉̀̀͑̄̋d̸̻̑̑̃̋̏̈̃͝s̶̟̤͇͖̐̊͑̿̎͗̋͜",


            sex="Bull",
            owner="Cthulu"
        )
    }
    fake_events = {
        "calved": FakeEvent(
            date="1970-01-02",
            name="Calved",
            description="567 gave birth to 922"
        ),
        "born": FakeEvent(
            date="1970-01-01",
            name="Born",
        ),
        "branded": FakeEvent(
            name="Branded",
            date="1970-01-01",
            description="A fake branding for testing purposes. In January..."
        ),
        "archived": FakeEvent(
            isArchived=True
        )
    }

    fake_transactions = {
        "medication": FakeTransaction(
            name="Cefimoxin",
            description="The finest quality snake oil on the market",
            tofrom="Paymore Veterinary Supplies",
            price=-1.40
        ),
        "vet bill": FakeTransaction(
            name="Vet Bill",
            description="A very expensive transaction",
            tofrom="Doc Hirshorn",
            date="1970-01-02",
            price=-312.98
        ),
        "transfer": FakeTransaction(
            name="Transfer",
            description="Sophia Olsen sold 1234 to Keith Rivers",
            isArchived=True
        )
    }
    fake_users = {
        "Keith Rivers": FakeUser(
            username="Keith Rivers",
            hashed_password= ("pbkdf2:sha256:260000$6bN8wco5CG04kxy4$a8b2cdb56409a1fb0c91541e"
                                    "4472b26948db5bfbfd0170f36d87d2f64b6d9fef")
        ),
        "Sophia Olsen": FakeUser(
            username= "Sophia Olsen",
            # Hash of "password":
            hashed_password= ("pbkdf2:sha256:260000$6bN8wco5CG04kxy4$a8b2cdb56409a1fb0c91541e"
                                    "4472b26948db5bfbfd0170f36d87d2f64b6d9fef")
        )
    }   
    fake_events["branded"].add_cow(fake_cows["921"])
    fake_events["branded"].add_transaction(fake_transactions["medication"])
    fake_transactions["medication"].set_event(fake_events["branded"])
    fake_cows["921"].add_event(fake_events["branded"])
    fake_transactions["vet bill"].add_cow(fake_cows["633"])
    fake_transactions["vet bill"].add_cow(fake_cows["641"])
    fake_transactions["vet bill"].set_event(fake_events["calved"])

    fake_events["branded"].add_cow(fake_cows["922"])
    fake_cows["922"].add_event(fake_events["branded"])
    fake_cows["576"].add_event(fake_events["born"])
    fake_cows["567"].add_event(fake_events["branded"])
    fake_cows["567"].add_event(fake_events["calved"])
    fake_cows["567"].add_calf(fake_cows["922"])
    fake_cows["567"].add_transaction(fake_transactions["medication"])
    fake_cows["567"].add_transaction(fake_transactions["vet bill"])
    fake_cows["922"].set_dam(fake_cows["567"])
    fake_cows["921"].set_dam(fake_cows["567"])

    fake_cows["921"].set_sire(fake_cows["B̴̯̟̻͌̊̓̄́͛͝ǒ̸̖̱̽͋v̴̧̻͇̼͍̥̲͙͔̓͗͊̍̚͝į̸̗̩̗̔u̸̧̧͉̫̐̇̇̕̕s̴̨̛̱̮̜ͅ ̵̼̤̪̽́D̶̗͊̿̂̆͗͆́̑e̶̥͎͔̮͕̺̻̼̓̕s̵̢̘̦̈̕t̸̝͍̥͇̱̳̮̬͂́͌̽ͅŗ̵̻͇̬͈̝̭̪̎̔͗̎͆̈́̽̕͝o̶̤̫̐y̷͇͕͂͌ę̸̡̨̮̰̘͝r̸̙̼̖̫̭͈͙̖͈̉̐̏͂́͘ ̷̨̎̉͑̈̿̽̍̇̚o̸̦͎̜̣̳̽͝ͅf̷̫͕̍̀̈́̋ ̸̲̐͑̔͊̈̀͆͝W̶͇̠̟͂́̇̃͆́̎o̵̯̝͔̩̔̊r̸̡͚͓͉̒́̉̀̀̉̎̈́l̶̢̧̛̬̤͕͖̣̼̻͊́̉̀̀͑̄̋d̸̻̑̑̃̋̏̈̃͝s̶̟̤͇͖̐̊͑̿̎͗̋͜"])
    return {
        "cows": fake_cows,
        "events": fake_events,
        "transactions": fake_transactions,
        "users": fake_users
    }
