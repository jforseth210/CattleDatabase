"""
Responsible for testing everything in the events module
"""
from sqlalchemy.exc import IntegrityError


class TestAddEvent:
    """
    Responsible for testing adding an event.
    """

    def test_add(self, client, mocker, testing_objects):
        """
        This tests the add event route under very basic conditions.

        It should create the event and add it to the database with all of the correct information.
        """
        mock_event = mocker.patch("routes.web.events.Event")
        mock_db = mocker.patch("routes.web.events.db")
        mocker.patch("routes.web.events.get_cow_from_tag",
                     side_effect=[testing_objects["cows"]["576"], testing_objects["cows"]["567"]])
        response = client.post(
            "/events/add",
            data={
                "cows": [
                    "576",
                    "567"
                ],
                "name": "Branded",
                "date": "1970-01-01",
                "description": "Branded calves"
            },
            headers={"Referer": "/events"},
            follow_redirects=True
        )
        mock_event.assert_called_once_with(
            cows=[testing_objects["cows"]["576"],
                  testing_objects["cows"]["567"]],
            name="Branded",
            date="1970-01-01",
            description="Branded calves"
        )
        mock_db.session.add.assert_called_once()
        mock_db.session.commit.assert_called_once()
        assert response.status_code == 200

    def test_add_event_with_no_cows(self, client, mocker):
        """
        Tests creating an event with no cows
        """
        mock_event = mocker.patch("routes.web.events.Event")
        mock_db = mocker.patch("routes.web.events.db")
        response = client.post(
            "/events/add",
            data={
                "cows": [],
                "name": "Branded",
                "date": "1970-01-01",
                "description": "Branded calves"
            },
            headers={"Referer": "/events"},
            follow_redirects=True
        )
        mock_event.assert_called_once_with(
            cows=[],
            name="Branded",
            date="1970-01-01",
            description="Branded calves"
        )
        mock_db.session.add.assert_called_once()
        mock_db.session.commit.assert_called_once()
        assert response.status_code == 200


class TestGetEvent:
    """
    Tests fetching an event
    """

    def test_get_event(self, client, mocker, testing_objects):
        """
        Tests fetching an event
        """
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["branded"]

        response = client.get("/events/event/2")
        assert response.status_code == 200

    def test_get_event_no_transaction_events_or_calves(self, client, mocker, testing_objects):
        """
        Tests fetching an event
        """
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["calved"]

        response = client.get("/events/event/1")
        assert response.status_code == 200

    def test_get_nonexistant_event(self, client, mocker):
        """
        Tests trying to retrieve an event_id without a corresponding event
        """
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = None

        response = client.get(
            "/events/event/1",
            headers={"Referer": "/events"},
            follow_redirects=True
        )
        assert response.status_code == 404


class TestGetEventList:
    """
    Tests fetching a list of events
    """

    def test_get_event_list(self, client, mocker, testing_objects):
        """
        Tests getting a list of events
        """
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.all.return_value = [
            testing_objects["events"]["branded"], testing_objects["events"]["calved"]]
        response = client.get("/events/")
        assert response.status_code == 200

    def test_get_empty_event_list(self, client, mocker):
        """
        Tests getting a list of events when there are none
        """
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.all.return_value = []
        response = client.get("/events/")
        assert response.status_code == 200


class TestEventUpdateCows:
    """
    Tests updating the cows associated with an event
    """

    def test_event_update_all_cows(self, client, mocker, testing_objects):
        """
        Tests updating an event's cows
        """
        mocker.patch("routes.web.events.db")
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["calved"]
        mocker.patch("routes.web.events.get_cow_from_tag",
                     side_effect=[testing_objects["cows"]["567"], testing_objects["cows"]["576"]])
        response = client.post(
            "/events/update_cows",
            data={
                "event_id": 2,
                "all_cows": ["567", "576"],
            },
            headers={"Referer": "/events"},
            follow_redirects=True
        )
        assert response.status_code == 200
        assert testing_objects["events"]["calved"].cows == [
            testing_objects["cows"]["567"], testing_objects["cows"]["576"]]

    def test_event_add_cows(self, client, mocker, testing_objects, fake_get_cow_from_tag):
        """
        Tests updating a cow to an event
        """
        mocker.patch("routes.web.events.db")
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["branded"]
        mocker.patch("routes.web.events.get_cow_from_tag",
                     side_effect=fake_get_cow_from_tag)
        original_num_cows = len(testing_objects["events"]["branded"].cows)
        assert testing_objects["cows"]["633"] not in testing_objects["events"]["branded"].cows
        response = client.post(
            "/events/update_cows",
            data={
                "event_id": 2,
                "new_cow": "633",
            },
            headers={"Referer": "/events"},
            follow_redirects=True
        )
        new_num_cows = len(testing_objects["events"]["branded"].cows)
        assert response.status_code == 200
        assert original_num_cows + 1 == new_num_cows
        assert testing_objects["cows"]["633"] in testing_objects["events"]["branded"].cows

    def test_non_existant_event_update_cows(self, client, mocker, testing_objects):
        """
        Tests trying to modify an event that doesn't exist
        """
        mocker.patch("routes.web.events.db")
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = None
        mocker.patch("routes.web.events.get_cow_from_tag",
                     side_effect=[testing_objects["cows"]["567"], testing_objects["cows"]["576"]])
        response = client.post("/events/update_cows", data={
            "event_id": 46546,
            "all_cows": ["567", "576"]
        })
        assert response.status_code == 404

    def test_event_update_to_no_cows(self, client, mocker, testing_objects):
        """
        Tests removing all cows from an event
        """
        mocker.patch("routes.web.events.db")
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["calved"]
        mocker.patch("routes.web.events.get_cow_from_tag", return_value=None)
        response = client.post(
            "/events/update_cows",
            data={
                "event_id": 2,
                "all_cows": ""
            },
            headers={"Referer": "/events"},
            follow_redirects=True
        )
        assert response.status_code == 200
        assert testing_objects["events"]["calved"].cows == []


class TestChangeEventName:
    """
    Tests modifying the name of an event
    """

    def test_change_event_name(self, client, mocker, testing_objects):
        """
        Tests changing the name of an event successfully
        """
        assert testing_objects["events"]["calved"].name != "Incremented number of offspring"
        mocker.patch("routes.web.events.db")
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["calved"]
        response = client.post(
            "/events/change_name",
            data={
                "event_id": 2,
                "name": "Incremented number of offspring"
            },
            headers={"Referer": "/events"},
            follow_redirects=True
        )
        assert response.status_code == 200
        assert testing_objects["events"]["calved"].name == "Incremented number of offspring"

    def test_non_existant_event_change_name(self, client, mocker, testing_objects):
        """
        Tests trying to modify an nonexistant event
        """
        mocker.patch("routes.web.events.db")
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = None
        response = client.post("/events/change_name", data={
            "event_id": 46546,
            "name": "Incremented number of offspring"
        })
        assert response.status_code == 404


class TestChangeEventDescription:
    """
    Tests modifying the description of an event
    """

    def test_change_event_description(self, client, mocker, testing_objects):
        """
        Tests changing the description of an event successfully
        """
        assert testing_objects["events"]["calved"].description != "A description describing the event being described"
        mocker.patch("routes.web.events.db")
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["calved"]
        response = client.post(
            "/events/change_description",
            data={
                "event_id": 2,
                "description": "A description describing the event being described"
            },
            headers={"Referer": "/events"},
        )
        assert response.status_code == 302
        assert testing_objects["events"]["calved"].description == "A description describing the event being described"

    def test_non_existant_event_change_description(self, client, mocker, testing_objects):
        """
        Tests updating an event's cows
        """
        mocker.patch("routes.web.events.db")
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = None
        response = client.post(
            "/events/change_description",
            data={
                "event_id": 46546,
                "description": "A description describing the event being described"
            },
            headers={"Referer": "/events"})
        assert response.status_code == 404


class TestChangeEventDate:
    """
    Tests modifying the date of an event
    """

    def test_change_event_date(self, client, mocker, testing_objects):
        """
        Tests changing the date of an event successfully
        """
        assert testing_objects["events"]["calved"].date != "1970-05-14"
        mocker.patch("routes.web.events.db")
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["calved"]
        response = client.post(
            "/events/change_date",
            data={
                "event_id": 2,
                "date": "1970-05-14"
            },
            headers={"Referer": "/events"},
            follow_redirects=True
        )
        assert response.status_code == 200
        assert testing_objects["events"]["calved"].date == "1970-05-14"

    def test_non_existant_event_change_date(self, client, mocker, testing_objects):
        """
        Tests updating an event's cows
        """
        mocker.patch("routes.web.events.db")
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = None
        response = client.post("/events/change_date", data={
            "event_id": 46546,
            "date": "1970-05-14"
        })
        assert response.status_code == 404


class TestArchive:
    """
    This class is responsible for testing archiving a event.
    """

    def test_archive_unarchived_event(self, client, mocker, testing_objects):
        """
        Tests taking a event that's not archived and archiving it.
        """
        mocker.patch("routes.web.events.db")
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["calved"]
        assert testing_objects["events"]["calved"].isArchived is False

        response = client.get("/events/archive", query_string={
            "event_id": 2
        }, follow_redirects=True
        )

        assert testing_objects["events"]["calved"].isArchived is True
        assert response.status_code == 200

    def test_archive_archived_event(self, client, mocker, testing_objects):
        """
        Tests taking a event that is archived and trying to archive it again.
        """
        mocker.patch("routes.web.events.db")
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["archived"]

        response = client.get("/events/archive", data={
            "event_id": 3
        }, follow_redirects=True)
        assert testing_objects["events"]["archived"].isArchived is True
        assert response.status_code == 200

    def test_archive_nonexistant_event(self, client, mocker):
        """
        Tests trying to archive a event that doesn't exist.
        """
        mocker.patch("routes.web.events.db")
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = None

        response = client.get("/events/archive", data={
            "event_id": 2642
        }, follow_redirects=True)
        assert response.status_code == 404


class TestUnarchive:
    """
    This class is responsible for testing archiving a event.
    """

    def test_unarchive_unarchived_event(self, client, mocker, testing_objects):
        """
        Tests taking a event that's not archived and unarchiving it.
        """
        mocker.patch("routes.web.events.db")
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["calved"]
        assert testing_objects["events"]["calved"].isArchived is False

        response = client.get("/events/unarchive", query_string={
            "event_id": 2
        },
            headers={"Referer": "/events"}, follow_redirects=True)
        assert testing_objects["events"]["calved"].isArchived is False
        assert response.status_code == 200

    def test_unarchive_archived_event(self, client, mocker, testing_objects):
        """
        Tests taking a event that is archived and trying to unarchive it
        """
        mocker.patch("routes.web.events.db")
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["archived"]
        assert testing_objects["events"]["archived"].isArchived is True

        response = client.get("/events/unarchive", query_string={
            "event_id": 3
        }, headers={"Referer": "/events"}, follow_redirects=True)
        assert testing_objects["events"]["archived"].isArchived is False
        assert response.status_code == 200

    def test_unarchive_nonexistant_event(self, client, mocker):
        """
        Tests trying to unarchive a event that doesn't exist.
        """
        mocker.patch("routes.web.events.db")
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = None

        response = client.get("/events/unarchive", query_string={
            "event_id": 6546
        })
        assert response.status_code == 404


class TestDeleteEvent:
    """
    Tests the deletion of an event
    """

    def test_delete_event(self, client, mocker, testing_objects):
        """
        Tests deleting an event
        """
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_db = mocker.patch("routes.web.events.db")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["calved"]

        response = client.post("/events/delete", data={
            "event_id": 2
        }, headers={"Referer": "/events"}, follow_redirects=True)

        assert response.status_code == 200
        mock_db.session.delete.assert_called_once()
        mock_db.session.commit.assert_called_once()

    def test_delete_event_with_transactions(self, client, mocker, testing_objects):
        """
        Tests removing an event with existing transactions
        """
        mock_db = mocker.patch("routes.web.events.db")
        mock_db.session.commit.side_effect = IntegrityError(
            # I don't know what the arguments are
            # supposed to be. This works:
            mocker.MagicMock(),
            mocker.MagicMock(),
            mocker.MagicMock()
        )
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["calved"]

        response = client.post("/events/delete", data={
            "event_id": 2
        }, headers={"Referer": "/events"}, follow_redirects=True)

        assert b"Please delete all transactions from this event first." in response.data

    def test_delete_nonexistant_event(self, client, mocker):
        """
        Tests removing an event that doesn't exist
        """
        mock_db = mocker.patch("routes.web.events.db")
        mock_event_object = mocker.patch("routes.web.events.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = None

        response = client.post("/events/delete", data={
            "event_id": 2
        })

        assert response.status_code == 404
        mock_db.session.delete.assert_not_called()
        mock_db.session.delete.assert_not_called()
