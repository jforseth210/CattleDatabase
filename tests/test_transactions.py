"""
Responsible for testing everything in the transactions module
"""


class TestAddTransaction:
    """
    Responsible for testing adding an transaction.
    """

    def test_add(self, client, mocker, testing_objects):
        """
        This tests the add transaction route under very basic conditions.

        It should create the transaction and add it to the database with all of the correct information.
        """
        mock_transaction = mocker.patch("routes.web.transactions.Transaction")
        mock_db = mocker.patch("routes.web.transactions.db")
        mock_event_object = mocker.patch("routes.web.transactions.Event")
        mock_event_object.query.filter_by.return_value.first.return_value = testing_objects[
            "events"]["branded"]
        response = client.post(
            "/transactions/add",
            data={
                "event_id": 2,
                "name": "Predstataarol",
                "description": "An expense",
                "tofrom": "Sweeney Supply Co. Inc. Ltd.",
                "price": -1.22
            },
            headers={"Referer": "/transactions"},
            follow_redirects=True
        )
        mock_transaction.assert_called_once_with(
            event_id=testing_objects["events"]["branded"].event_id,
            cows=testing_objects["events"]["branded"].cows,
            name="Predstataarol",
            description="An expense",
            tofrom="Sweeney Supply Co. Inc. Ltd.",
            price=-1.22
        )
        mock_db.session.add.assert_called_once()
        mock_db.session.commit.assert_called_once()
        assert response.status_code == 200


class TestGetTransaction:
    """
    Tests fetching an transaction
    """

    def test_get_transaction(self, client, mocker, testing_objects):
        """
        Tests fetching an transaction
        """
        mock_transaction_object = mocker.patch(
            "routes.web.transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["vet bill"]

        response = client.get(
            "/transactions/transaction/2",
            headers={"Referer": "/transactions"},
            follow_redirects=True
        )
        assert response.status_code == 200

    def test_get_nonexistant_transaction(self, client, mocker):
        """
        Tests trying to retrieve an transaction_id without a corresponding transaction
        """
        mock_transaction_object = mocker.patch(
            "routes.web.transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = None

        response = client.get(
            "/transactions/transaction/1465",
            headers={"Referer": "/transactions"},
            follow_redirects=True
        )
        assert response.status_code == 404


class TestGetTransactionList:
    def test_get_empty_transaction_list(self, client, mocker):
        """
        Tests getting a list of transactions when there are none
        """
        mock_transaction_object = mocker.patch(
            "routes.web.transactions.Transaction")
        mock_transaction_object.query.all.return_value = []
        response = client.get(
            "/transactions",
            follow_redirects=True
        )
        assert response.status_code == 200
        assert b"clickable-tr" not in response.data


class TestTransactionUpdateCows:
    """
    Tests updating the cows associated with an transaction
    """

    def test_transaction_update_cows(self, client, mocker, testing_objects):
        """
        Tests updating an transaction's cows
        """
        mocker.patch("routes.web.transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.web.transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["vet bill"]
        mocker.patch("routes.web.transactions.get_cow_from_tag",
                     side_effect=[testing_objects["cows"]["567"], testing_objects["cows"]["576"], testing_objects["cows"]["567"], testing_objects["cows"]["576"]])
        response = client.post(
            "/transactions/update_cows",
            data={
                "transaction_id": 2,
                "all_cows": ["567", "576"]
            },
            headers={"Referer": "/transactions"},
            follow_redirects=True

        )
        assert response.status_code == 200
        assert testing_objects["transactions"]["vet bill"].cows == [
            testing_objects["cows"]["567"], testing_objects["cows"]["576"]]
        assert testing_objects["cows"]["567"] in testing_objects["transactions"]["vet bill"].event.cows
        assert testing_objects["cows"]["576"] in testing_objects["transactions"]["vet bill"].event.cows

    def test_non_existant_transaction_update_cows(self, client, mocker, testing_objects):
        """
        Tests trying to modify an transaction that doesn't exist
        """
        mocker.patch("routes.web.transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.web.transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = None
        mocker.patch("routes.web.transactions.get_cow_from_tag",
                     side_effect=[testing_objects["cows"]["567"], testing_objects["cows"]["576"]])
        response = client.post(
            "/transactions/update_cows",
            data={
                "transaction_id": 46546,
                "all_cows": ["567", "576"]
            },
            headers={"Referer": "/transactions"},
            follow_redirects=True
        )
        assert response.status_code == 404

    def test_transaction_update_to_no_cows(self, client, mocker, testing_objects):
        """
        Tests removing all cows from an transaction
        """
        mocker.patch("routes.web.transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.web.transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["vet bill"]
        mocker.patch("routes.web.transactions.get_cow_from_tag",
                     return_value=None)
        response = client.post(
            "/transactions/update_cows",
            data={
                "transaction_id": 2,
                "all_cows": ""
            },
            headers={"Referer": "/transactions"},
            follow_redirects=True
        )
        assert response.status_code == 200
        assert testing_objects["transactions"]["vet bill"].cows == []


class TestChangeTransactionName:
    """
    Tests modifying the name of an transaction
    """

    def test_change_transaction_name(self, client, mocker, testing_objects):
        """
        Tests changing the name of an transaction successfully
        """
        assert testing_objects["transactions"]["vet bill"].name != "Incremented number of offspring"
        mocker.patch("routes.web.transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.web.transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["vet bill"]
        response = client.post(
            "/transactions/change_name",
            data={
                "transaction_id": 2,
                "name": "Incremented number of offspring"
            },
            headers={"Referer": "/transactions"},
            follow_redirects=True
        )
        assert response.status_code == 200
        assert testing_objects["transactions"]["vet bill"].name == "Incremented number of offspring"

    def test_non_existant_transaction_change_name(self, client, mocker):
        """
        Tests trying to modify an nonexistant transaction
        """
        mocker.patch("routes.web.transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.web.transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = None
        response = client.post("/transactions/change_name", data={
            "transaction_id": 46546,
            "name": "Incremented number of offspring"
        })
        assert response.status_code == 404


class TestChangeTransactionDescription:
    """
    Tests modifying the description of an transaction
    """

    def test_change_transaction_description(self, client, mocker, testing_objects):
        """
        Tests changing the description of an transaction successfully
        """
        assert testing_objects["transactions"]["vet bill"].description != "A description describing the transaction being described"
        mocker.patch("routes.web.transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.web.transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["vet bill"]
        response = client.post(
            "/transactions/change_description",
            data={
                "transaction_id": 2,
                "description": "A description describing the transaction being described"
            },
            headers={"Referer": "/transactions"},
            follow_redirects=True
        )
        assert response.status_code == 200
        assert testing_objects["transactions"]["vet bill"].description == "A description describing the transaction being described"

    def test_non_existant_transaction_change_description(self, client, mocker):
        """
        Tests updating an transaction's cows
        """
        mocker.patch("routes.web.transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.web.transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = None
        response = client.post("/transactions/change_description", data={
            "transaction_id": 46546,
            "description": "A description describing the transaction being described"
        })
        assert response.status_code == 404


class TestChangeTransactionToFrom:
    """
    Tests modifying the to/from of an transaction
    """

    def test_change_transaction_to_from(self, client, mocker, testing_objects):
        """
        Tests changing the to/from of an transaction successfully
        """
        assert testing_objects["transactions"]["vet bill"].tofrom != "Doc O'Daniels"
        mocker.patch("routes.web.transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.web.transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["vet bill"]
        response = client.post(
            "/transactions/change_to_from",
            data={
                "transaction_id": 2,
                "tofrom": "Doc O'Daniels"
            },
            headers={"Referer": "/transactions"},
            follow_redirects=True
        )
        assert response.status_code == 200
        assert testing_objects["transactions"]["vet bill"].tofrom == "Doc O'Daniels"

    def test_non_existant_transaction_change_to_from(self, client, mocker):
        """
        Tests updating an transaction's cows
        """
        mocker.patch("routes.web.transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.web.transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = None
        response = client.post("/transactions/change_to_from", data={
            "transaction_id": 46546,
            "tofrom": "1970-05-14"
        })
        assert response.status_code == 404


class TestChangeTransactionPrice:
    """
    Tests modifying the price of an transaction
    """

    def test_change_transaction_price(self, client, mocker, testing_objects):
        """
        Tests changing the price of an transaction successfully
        """
        assert testing_objects["transactions"]["vet bill"].price != -256.00
        mocker.patch("routes.web.transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.web.transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["vet bill"]
        response = client.post(
            "/transactions/change_price", data={
                "transaction_id": 2,
                "price": -256.00
            },
            headers={"Referer": "/transactions"},
            follow_redirects=True
        )
        assert response.status_code == 200
        assert testing_objects["transactions"]["vet bill"].price == -256

    def test_non_existant_transaction_change_price(self, client, mocker):
        """
        Tests updating an transaction's cows
        """
        mocker.patch("routes.web.transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.web.transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = None
        response = client.post(
            "/transactions/change_price",
            data={
                "transaction_id": 46546,
                "price": -256.00
            }
        )
        assert response.status_code == 404


class TestArchive:
    """
    This class is responsible for testing archiving a transaction.
    """

    def test_archive_unarchived_transaction(self, client, mocker, testing_objects):
        """
        Tests taking a transaction that's not archived and archiving it.
        """
        mocker.patch("routes.web.transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.web.transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["vet bill"]
        assert testing_objects["transactions"]["vet bill"].isArchived is False

        response = client.get(
            "/transactions/archive",
            query_string={
                "transaction_id": 2
            },
            headers={"Referer": "/transactions"},
            follow_redirects=True
        )

        assert response.status_code == 200
        assert testing_objects["transactions"]["vet bill"].isArchived is True

    def test_archive_archived_transaction(self, client, mocker, testing_objects):
        """
        Tests taking a transaction that is archived and trying to archive it again.
        """
        mocker.patch("routes.web.transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.web.transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["transfer"]

        response = client.get(
            "/transactions/archive",
            query_string={
                "transaction_id": 3
            },
            headers={"Referer": "/transactions"},
            follow_redirects=True
        )
        assert testing_objects["transactions"]["transfer"].isArchived is True
        assert response.status_code == 200

    def test_archive_nonexistant_transaction(self, client, mocker):
        """
        Tests trying to archive a transaction that doesn't exist.
        """
        mocker.patch("routes.web.transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.web.transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = None

        response = client.get(
            "/transactions/archive",
            query_string={
                "transaction_id": 2642
            },
            headers={"Referer": "/transactions"},
            follow_redirects=True
        )
        assert response.status_code == 404


class TestUnarchive:
    """
    This class is responsible for testing archiving a transaction.
    """

    def test_unarchive_unarchived_transaction(self, client, mocker, testing_objects):
        """
        Tests taking a transaction that's not archived and unarchiving it.
        """
        mocker.patch("routes.web.transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.web.transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["vet bill"]
        assert testing_objects["transactions"]["vet bill"].isArchived is False

        response = client.get(
            "/transactions/unarchive",
            query_string={
                "transaction_id": 2
            },
            headers={"Referer": "/transactions"},
            follow_redirects=True
        )
        assert testing_objects["transactions"]["vet bill"].isArchived is False
        assert response.status_code == 200

    def test_unarchive_archived_transaction(self, client, mocker, testing_objects):
        """
        Tests taking a transaction that is archived and trying to unarchive it
        """
        mocker.patch("routes.web.transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.web.transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["transfer"]
        assert testing_objects["transactions"]["transfer"].isArchived is True

        response = client.get(
            "/transactions/unarchive",
            query_string={
                "transaction_id": 3
            },
            headers={"Referer": "/transactions"},
            follow_redirects=True
        )
        assert testing_objects["transactions"]["transfer"].isArchived is False
        assert response.status_code == 200

    def test_archive_nonexistant_transaction(self, client, mocker):
        """
        Tests trying to unarchive a transaction that doesn't exist.
        """
        mocker.patch("routes.web.transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.web.transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = None

        response = client.get(
            "/transactions/unarchive",
            query_string={
                "transaction_id": 6546
            },
            headers={"Referer": "/transactions"},
            follow_redirects=True
        )
        assert response.status_code == 404


class TestDeleteTransaction:
    """
    Tests the deletion of an transaction
    """

    def test_delete_transaction(self, client, mocker, testing_objects):
        """
        Tests deleting an transaction
        """
        mock_transaction_object = mocker.patch(
            "routes.web.transactions.Transaction")
        mock_db = mocker.patch("routes.web.transactions.db")
        mock_transaction_object.query.filter_by.return_value.first.return_value = testing_objects[
            "transactions"]["vet bill"]

        response = client.post(
            "/transactions/delete",
            data={
                "transaction_id": 2
            },
            follow_redirects=True
        )

        assert response.status_code == 200
        mock_db.session.delete.assert_called_once()
        mock_db.session.commit.assert_called_once()

    def test_delete_nonexistant_transaction(self, client, mocker):
        """
        Tests removing an transaction that doesn't exist
        """
        mock_db = mocker.patch("routes.web.transactions.db")
        mock_transaction_object = mocker.patch(
            "routes.web.transactions.Transaction")
        mock_transaction_object.query.filter_by.return_value.first.return_value = None

        response = client.post("/transactions/delete", data={
            "transaction_id": 2
        })

        assert response.status_code == 404
        mock_db.session.delete.assert_not_called()
        mock_db.session.delete.assert_not_called()
