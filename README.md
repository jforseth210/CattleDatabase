# CattleDatabase

©2022 Justin Forseth. All Rights Reserved

This is a program I designed in order to manage my beef herd. It is capable of keeping track of herd ancestry, events like branding and shipping, ownership, transactions, and more, all from the comfort of your own computer.

## How does it work?

You choose a "host computer" to run your database. It stores the herd information locally on your computer and makes them accessible via a web interface. This can be accessed from the host computer even without an internet connection. When connected, it is available to any device on your home internet or (optionally, if your connection supports it) from anywhere with a connection.

If you're interested in the techy details: It's built using SQLite3 and Flask-SQLAlchemy for the database, Flask for the backend, and a web interface built using Bootstrap.

## How do I run it?

Visit [cattledb.jforseth.com](https://cattledb.jforseth.com), and download the appropriate file for your computer (CattleDB works on Windows and Linux. MacOS isn't currently supported).

Run the installer (if you're using Windows) and open the program.

NOTE: Certain browsers, operating systems, and antivirus programs may caution you against running CattleDB. That's because it's brand-new. Most security software hasn't had a chance to analyze it for threats yet. If you're unsure, you're encouraged to scan CattleDB with your antivirus of choice. If you have any questions, feel free to contact me for more information.

A page with further setup instructions should open in your web browser. Follow the onscreen instructions to get started using CattleDB!
