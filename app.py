"""
This is the entry point for the main program.
Gets everything set up and runs the server.
"""
from routes.web.web_installer import installer
from routes.web.transactions import transactions
from routes.web.settings import settings
from routes.web.search import search
from routes.web.events import events
from routes.web.cows import cows
from routes.web.calendar_page import calendar
from routes.api.api import api
from models.database import db
from helper.sexes import get_sexes
from helper.setup_utils import check_for_updates, is_fully_installed, PORT
from helper.login_checker import login_checker
from helper.cli_installer import cli_installer
from werkzeug.middleware.proxy_fix import ProxyFix
from flask_migrate import Migrate
from flask_simplelogin import SimpleLogin, login_required, is_logged_in, get_username
from flask_inertia import Inertia
from flask import Flask, render_template, redirect, request, get_flashed_messages
import webbrowser
import traceback as tb
import platform
import os
import logging.handlers
import json
import argparse
import sys
sys.path.append(".")


LOG_LEVEL = logging.DEBUG
LOG_FILE = "logs/CattleDB.log"
os.makedirs(os.path.dirname(LOG_FILE), exist_ok=True)
MAX_BYTES = 1 * 1024 * 1024
BACKUP_COUNT = 3

logger = logging.getLogger()
logger.handlers.clear()
logger.setLevel(LOG_LEVEL)

# Create a console handler
console_handler = logging.StreamHandler()
console_handler.setLevel(LOG_LEVEL)
logger.addHandler(console_handler)

# Create a file handler
file_handler = logging.handlers.RotatingFileHandler(
    LOG_FILE, maxBytes=MAX_BYTES, backupCount=BACKUP_COUNT)
file_handler.setLevel(LOG_LEVEL)
logger.addHandler(file_handler)

# Create a formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
console_handler.setFormatter(formatter)
file_handler.setFormatter(formatter)

try:
    # Only imported to test if it's installed
    # pylint: disable=unused-import
    import miniupnpc

    # Trying to get this installed on Windows is agnonizing
    # This might help: https://github.com/miniupnp/miniupnp/issues/159
except ImportError:
    if platform.system() != "Windows":
        logger.warning(
            "Failed to find some libraries (miniupnpc). "
            "CattleDB should work fine without it, but if you run "
            " into issues, mention this when contacting support."
        )

# Create a secret key if none exists
try:
    logger.debug("Reading secret key")
    with open("sensitive_data.json", "r", encoding="utf-8") as f:
        SECRET_KEY = json.loads(f.read())["SECRET_KEY"]
    logger.debug("Secret key set")
except FileNotFoundError:
    logger.debug("No secret key found. Generating one...")
    SECRET_KEY = str(os.urandom(64))
    logger.debug("Secret key set")
    logger.debug("Writing secret key to disk")
    with open("sensitive_data.json", "w", encoding="utf-8") as f:
        f.write(json.dumps({"SECRET_KEY": SECRET_KEY}))
    logger.debug("Secret key written")

# Configure flask app

base_directory = os.path.abspath(os.path.dirname(__file__))
db_path = os.path.join(base_directory, 'cattle.db')


class ProdConfig:
    SQLALCHEMY_DATABASE_URI = f"sqlite:////{db_path}"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = SECRET_KEY
    INERTIA_TEMPLATE = "inertia_base.html"


def create_app(app_config=ProdConfig()):
    """
    Creates a CattleDB app
    """

    # Something to do with loading in templates and static folders
    # once compiled into an executable.
    # Probably copy-pasted. Not touching it.
    ROOT_DIR = os.path.abspath(os.path.dirname(__file__))
    logger.debug("Root dir: %s", ROOT_DIR)
    template_folder = os.path.join(ROOT_DIR, "templates")
    static_folder = os.path.join(ROOT_DIR, "static")
    app = Flask(__name__, template_folder=template_folder,
                static_folder=static_folder)
    logger.debug("Initializing inertia")
    inertia = Inertia(app)
    logger.debug("Done initializing inertia")
    app.config.from_object(app_config)
    # Load in all of the blueprints
    logger.debug("Registering blueprints")
    app.register_blueprint(installer, url_prefix="/install")
    app.register_blueprint(settings, url_prefix="/settings")
    app.register_blueprint(search, url_prefix="/search")
    app.register_blueprint(api, url_prefix="/api")
    app.register_blueprint(cows, url_prefix="/cows")
    app.register_blueprint(events, url_prefix="/events")
    app.register_blueprint(transactions, url_prefix="/transactions")
    app.register_blueprint(calendar, url_prefix="/calendar")
    logger.debug("Done registering blueprints")

    # Initialize the db
    db.init_app(app)
    migrate = Migrate(app, db)

    app.jinja_env.globals["get_sexes"] = get_sexes

    # Messages related to flask_simplelogin
    messages = {
        "login_success": "",
        "login_failure": "Login Failed",
        "is_logged_in": "",
        "logout": "Logged Out",
        "login_required": "",
        "access_denied": "You don't have access to this page",
        "auth_error": "Something went wrong： {0}",
    }

    logger.debug("Initializing simplelogin")
    simplelogin = SimpleLogin(
        app, login_checker=login_checker, messages=messages)
    logger.debug("Done initializing simplelogin")

    update_available, version, latest_version = check_for_updates()
    inertia.share("update_available", update_available)
    inertia.share("version", version)
    inertia.share("latest_version", latest_version)
    inertia.share("logged_in", is_logged_in)
    inertia.share("username", get_username)
    inertia.share("messages", get_flashed_messages)

    if os.environ.get("REVERSE_PROXY") == "true":
        app.wsgi_app = ProxyFix(
            app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_prefix=1
        )

    @app.context_processor
    def inject_version():
        """
        Provide all templates with version information.
        """
        return dict(
            update_available=update_available,
            version=version,
            latest_version=latest_version,
        )

    @app.before_request
    def log_route():
        logger.info(
            f"{request.access_route[0]}: {request.path}")
        # if request.args.to_dict():
        #    logger.debug(f"Args: {request.args.to_dict()}")
        # if request.form.to_dict():
        #    form_dict = request.form.to_dict()
        #    if "password" in form_dict:
        #        form_dict["password"] = "<REDACTED>"
        #    logger.debug(f"Form: {form_dict}")

    @app.after_request
    def log_response(response):
        logger.debug(
            f"{request.access_route[0]}:"
            f"{request.path}. Done. Response: {response.status}")
        return response

    @app.errorhandler(400)
    def bad_request_error(error):
        """
        Display a custom 400 page.
        """
        logger.warning("400 BAD REQUEST: "+error.description)
        return render_template("400.html", description=error.description), 400

    @app.errorhandler(404)
    def not_found_error(error):
        """
        Display a custom 404 page.
        """
        logger.warning("404 not found")
        return render_template("404.html", description=error.description), 404

    @app.errorhandler(500)
    def internal_error(error):
        """
        Display a custom 500 error page.
        """
        logger.error("500 error", exc_info=error)
        db.session.rollback()
        trace = tb.format_exception(
            None, error.original_exception, error.original_exception.__traceback__
        )

        return render_template("500.html", trace=trace), 500

    @app.route("/")
    @login_required
    def home():
        """
        The root route of the project, redirects to /cows
        """
        return redirect("/cows")

    return app


if __name__ == "__main__":
    from waitress import serve
    logging.getLogger("waitress").setLevel(logging.ERROR)

    app = create_app()

    parser = argparse.ArgumentParser()
    # Start the server without opening browser
    parser.add_argument(
        "--headless",
        action="store_true",
        help="Run the server without opening it in browser",
    )

    parser.add_argument(
        "--use-cli-setup",
        dest="use_cli_setup",
        action="store_true",
        help="Use the CLI setup instead of the web installer",
    )
    parser.add_argument(
        "--use-flask-server",
        dest="use_flask_server",
        action="store_true",
        help="Use flask dev server instead of waitress",
    )
    args = parser.parse_args()
    app.debug = True

    print("Welcome to")
    print()
    print(r" ____              __    __    ___           ____    ____")
    print(r"/\  _`\           /\ \__/\ \__/\_ \         /\  _`\ /\  _`\ ")
    print(r"\ \ \/\_\     __  \ \ ,_\ \ ,_\//\ \      __\ \ \/\ \ \ \ \ \ ")
    print(r" \ \ \/_/_  /'__`\ \ \ \/\ \ \/ \ \ \   /'__`\ \ \ \ \ \  _ <'")
    print(r"  \ \ \ \ \/\ \ \.\_\ \ \_\ \ \_ \_\ \_/\  __/\ \ \_\ \ \ \ \ \ ")
    print(r"   \ \____/\ \__/.\_\\ \__\\ \__\/\____\ \____\\ \____/\ \____/")
    print(r"    \/___/  \/__/\/_/ \/__/ \/__/\/____/\/____/ \/___/  \/___/")
    print(r"")
    print()
    print("Herd Management for the 21st Century")
    update_available, version, new_version = check_for_updates()
    if update_available:
        print(
            f"Update Available! You're running {
                version} but {new_version} is available! "
            "Go to cattledb.jforseth.com to download the latest version"
        )

    if not is_fully_installed() and args.use_cli_setup:
        logger.warning(
            "Installation not complete and --use-cli-installer set. Running command line installer")
        cli_installer(args.headless, app, db)
    else:
        print("")
        print(
            "Welcome to CattleDB. Leave this window open and your records will "
            "be accessible from any device."
        )
        print()
        if not args.headless:
            print(
                "The CattleDB installation wizard should be opening in your web "
                "browser now. If it doesn't, just visit:"
            )
        print()
        print("http://localhost:" + str(PORT) + "/install")
        print()
        print("to get started. ")
        if not args.headless:
            webbrowser.open("http://localhost:" + str(PORT) + "/install")

    if args.use_flask_server:
        logger.debug("Using flask debug server")
        app.run(host="0.0.0.0", port=PORT, debug=True)
    else:
        logger.debug("Using waitress")
        serve(app, host="0.0.0.0", port=PORT)
