# The list of possible sexes, fertile/infertile, male/female, etc.
# used throughout the application.


def get_sexes(male_female, possible_parents=None):
    """
    Constants for different kinds of cattle.
    """
    possible_male = ["Bull (AI)", "Bull"]
    impossible_male = ["Steer"]

    possible_female = ["Cow", "Heifer", "Heifer (Replacement)"]
    impossible_female = ["Heifer (Market)", "Free-Martin"]
    output = []
    if male_female == "male":
        if possible_parents is True:
            output = possible_male
        elif possible_parents is False:
            output = impossible_male
        else:
            output = possible_male + impossible_male
    elif male_female == "female":
        if possible_parents is True:
            output = possible_female
        elif possible_parents is False:
            output = impossible_female
        else:
            output = possible_female + impossible_female
    else:
        output = possible_female + impossible_female + possible_male + impossible_male
    return output
