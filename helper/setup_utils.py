"""
Helper functions to deal with installing CattleDB
"""
import os
import json
import platform
import subprocess
import re
import socket
import logging
from version_parser import Version, VersionType
import requests
from models.user import User
try:
    import miniupnpc
except ImportError:
    pass
logger = logging.getLogger(__name__)
# Cow in base 10 :)
PORT = 31523
UPNP_DESCRIPTION = "CattleDB"
OFFLINE_MESSAGE = "<CURRENTLY OFFLINE>"
CURRENT_VERSION = Version("0.1.7")


def is_fully_installed():
    """
    Checks whether or not CattleDB has been completely installed.
    """
    if not os.path.exists('cattle.db'):
        return False
    try:
        with open("config.json", "r", encoding="utf-8") as file:
            config = json.loads(file.read())
    except (FileNotFoundError, UnicodeDecodeError):
        config = {}

    if config.get("using_wan", None):
        return True
    logger.debug("Not fully installed")
    return False


def get_private_ip():
    """
    Get's the devices private IPv4 address.
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # connect to the server on local computer
    sock.connect(("8.8.8.8", 80))
    private_ip = sock.getsockname()[0]
    sock.close()
    logger.debug(f"Private IP: {private_ip}")
    return private_ip


def get_public_ip():
    """
    Get's the device's public IPv4 address.
    """
    public_ip = requests.get("https://www.wikipedia.org",
                             timeout=10).headers["X-Client-IP"]
    logger.debug(f"Private IP: {public_ip}")
    return public_ip


def get_online():
    """
    Determine if connected to the internet
    """
    try:
        # see if we can resolve the host name -- tells us if there is
        # a DNS listening
        host = socket.gethostbyname("www.wikipedia.org")
        # connect to the host -- tells us if the host is actually
        # reachable
        sock = socket.create_connection((host, 80), 2)
        sock.close()
        return True
    except socket.gaierror:
        logger.warning("Offline")
        return False


def get_network_ssid():
    """
    Get's the name of the currently connected network.
    """
    # Windows SSID detection
    if platform.system() == "Windows":
        logger.debug("Using windows ssid detection")
        return subprocess.check_output(
            "powershell.exe (get-netconnectionProfile).Name", shell=True).strip().decode("UTF-8")

    # iwgetid detection
    logger.debug("Using iwgetid ssid detection")
    with subprocess.Popen(
            'iwgetid',
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
    ) as subprocess_result:
        subprocess_output = subprocess_result.communicate()[
            0], subprocess_result.returncode
    if subprocess_output[0] != b"":
        return re.search(r'"(.*?)"', subprocess_output[0].decode('utf-8')).group(0).replace("\"", "")

    # nmcli detectection
    logger.warning("Unable to determine ssid via iwgetid")
    with subprocess.Popen('nmcli -t -f name connection show --active',
                          shell=True,
                          stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE) as subprocess_result:
        subprocess_output = subprocess_result.communicate()[
            0], subprocess_result.returncode
        return subprocess_output[0].decode('utf-8').split("\n")[0]

    # detection failed
    if subprocess_output[0] == b"":
        logger.warning("Unable to determine ssid via nmcli")
        return "your host computer's current network"


def upnp_wizard_installed():
    """
    Check if UPnP Wizard has been installed
    """
    return os.path.exists("C:/Program Files (x86)/UPnP Wizard/UPnPWizardC.exe")


def add_upnp_rule():
    """
    Try to add a UPnP rule using miniupnpc on Linux, and UPnP Wizard on Windows
    """
    try:
        if platform.system() == "Windows":
            logger.debug("Using upnp wiz")
            if not upnp_wizard_installed():
                logger.warning("Attempting to add rule without wizard")
                return False
            command = (r'"C:\Program Files (x86)\UPnP Wizard\UPnPWizardC.exe"'
                       r' -add {} -ip {} -intport {} -extport {} -protocol {} -legacy')
            subprocess.run(command.format(
                UPNP_DESCRIPTION, get_private_ip(), PORT, PORT, "TCP"))
            return True
        logger.debug("Using miniupnpc")
        upnp = miniupnpc.UPnP()
        upnp.discoverdelay = 10
        upnp.discover()
        upnp.selectigd()
        # addportmapping(
        #   external-port,
        #   protocol,
        #   internal-host,
        #   internal-port,
        #   description,
        #   remote-host
        # )
        upnp.addportmapping(
            PORT,
            'TCP',
            upnp.lanaddr,
            PORT,
            UPNP_DESCRIPTION,
            '')
    except Exception as exception:
        logger.error("Failed to add upnp rule", exc_info=exception)
        return False
    return True


def check_for_upnp_rule():
    """
    Check if UPnP rule for CattleDB exists
    """
    if platform.system() == "Windows":
        logger.debug("using upnp wiz")
        if not upnp_wizard_installed():
            logger.warning("Attempting to check for rule without wizard")
            return False
        rules = str(subprocess.check_output(
            r'"C:\Program Files (x86)\UPnP Wizard\UPnPWizardC.exe" -legacy -list'))
        return UPNP_DESCRIPTION in rules
    try:
        logger.debug("Using miniupnpc")
        upnp = miniupnpc.UPnP()
        upnp.discoverdelay = 200
        upnp.discover()
        upnp.selectigd()

        rule = 1
        i = 0
        while rule:
            rule = upnp.getgenericportmapping(i)
            i += 1
            if rule:
                port, _, (to_addr, _), _, _, _, _ = rule
                if port == PORT and to_addr == get_private_ip():
                    return True
    except Exception as exception:
        logging.error(exc_info=exception)
        return False


def get_using_wan():
    """
    Check config.json to see if user wants to (and is able to) use online mode
    """

    with open("config.json", "r", encoding="utf-8") as file:
        return json.loads(file.read()).get("using_wan")


def get_users():
    """
    Get the usernames and hashed passwords of all users.
    """
    return User.query.all()


def get_usernames():
    """
    Get the usernames of all users
    """
    return [user.username for user in get_users()]


def check_for_updates():
    """
    Check whether or not there are updates available
    """
    logger.debug("Checking for updates")
    version = CURRENT_VERSION.get_typed_version(VersionType.STRIPPED_VERSION)
    try:
        versions = json.loads(requests.get(
            "https://cattledb.jforseth.com/versions.json", timeout=10).content)
        server_versions = versions["versions"]['server']
        latest_version = server_versions[0][0]
        latest_version = Version(latest_version)

        if latest_version > CURRENT_VERSION:
            new_version = latest_version.get_typed_version(
                VersionType.STRIPPED_VERSION)
            logger.debug("Done checking for updates, new version available")
            return True, version, new_version
        logger.debug("Done checking for updates, no new version available")
        return False, version, latest_version.get_typed_version(VersionType.STRIPPED_VERSION)
    except Exception:
        logger.error(
            "Unable to check for updates. Is cattledb.jforseth.com down?")
        return False, version, "?.?.?"
