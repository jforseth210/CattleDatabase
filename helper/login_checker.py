"""
Function for validating logins
"""
import json
import logging
from models.user import User
from werkzeug.security import check_password_hash
logger = logging.getLogger(__name__)


def login_checker(provided_user):
    """
    Validates whether the given login is correct.

    Input:
        {
            "username": String
            "password": String
        }

    Output:
        Boolean
    """
    user = User.query.filter_by(username=provided_user["username"]).first()
    if not user:
        return False
    return check_password_hash(user.hashed_password, provided_user["password"])