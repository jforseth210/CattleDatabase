"""
A command line installer for CattleDB.
Use of the web installer is preferrable, but
this should still allow for full installation of CattleDB
"""
import os
import time
import json
import webbrowser
from getpass import getpass
import werkzeug.security
from helper.setup_utils import (
    check_for_upnp_rule,
    add_upnp_rule,
    get_network_ssid,
    upnp_wizard_installed,
    PORT
)


def cli_installer(headless, app, database):
    """
    The starting point for the command line installer
    """
    print("There are just a couple steps to get started:")
    print()
    # If cattle.db missing, create or import it.
    if not os.path.exists("cattle.db"):
        print("The database hasn't been created yet. Would you like to:")
        print("1) Create a new database")
        print("2) Import an existing database")
        print()
        print("(If you aren't sure, choose 1)")
        response = ""
        # Repeat prompt until valid response.
        while response not in ["1", "2"]:
            response = input("Please type \"1\" or \"2\": ")
            # Create database
            if response == "1":
                with app.app_context():
                    database.create_all()
                    database.session.commit()
                print("Database created!")
            # Wait for database to be imported
            elif response == "2":
                print("Please copy your \"cattle.db\" file to this folder...")
                while not os.path.exists("cattle.db"):
                    time.sleep(1)
                print()
                print("Database imported!")
                print()
    # Create or import config if missing
    if not os.path.exists("config.json"):
        print("The configuration file hasn't been created yet. Would you like to:")
        print("1) Create a new configuration")
        print("2) Import an old configuration")
        print()
        print("(If you aren't sure, choose 1)")
        response = ""
        # Repeat prompt until valid response
        while response not in ["1", "2"]:
            response = input("Please type \"1\" or \"2\": ")
            # Create config
            if response == "1":
                generate_config()
            # Wait for config to be imported
            elif response == "2":
                print("Please copy your \"config.json\" file to this folder...")
                while not os.path.exists("config.json"):
                    time.sleep(1)
                print("Configuration imported!")
    with open("config.json", "r", encoding="utf-8") as file:
        config = json.loads(file.read())
    if not config.get("users"):
        create_users()
    if not config.get("using_wan"):
        prompt_wan_lan()
    print()
    print("Good to go!")
    print("Visit:")
    print()
    print("http://localhost:"+str(PORT)+"/install")
    print()
    print("to get started. ")
    if not headless:
        webbrowser.open("http://localhost:"+str(PORT)+"/install")


def generate_config():
    """
    The portion of the installer that deals with the creation of a new config file.
    """
    print("Creating a new configuration: ")
    print()
    # Prompt for usernames, passwords
    users = create_users()
    # Prompt for online mode
    using_wan = prompt_wan_lan()
    if using_wan:
        print()
        print("Setting up online access... this may take a few seconds")
        print()
        # See if UPnp rule exists
        if check_for_upnp_rule():
            print("Already set up. (We didn't do anything)")
        # It doesn't, try to add it
        elif add_upnp_rule():
            print()
            print("Your records are now accessible online")
            print()
        # It didn't work, tell the user, and set to local mode in config.json
        else:
            print()
            print(
                "Whoops! Something went wrong. "
                "We couldn't automatically configure your connection.")
            print(
                f"Your records will still be accessible on {get_network_ssid()}")
            print("It may still be possible to set up online access manually!")
            print("Contact the developer for more information.")
            print()
            using_wan = False

    # Write the config file
    config_dict = {
        "users": users,
        "using_wan": using_wan
    }
    config_json = json.dumps(config_dict, indent=4)
    with open("config.json", "w", encoding="utf-8") as file:
        file.write(config_json)
    print()
    print("Configuration created!")
    print()


def create_users():
    """
    The portion of the config that deals with the creation of new users.
    """
    print("Please create a username.")
    print("This will be used to log in and will be used")
    print("to distinguish between your cows, and cows")
    print("owned by others, like the one's you've sold.")

    # Prompt for comma separated list of usernames
    print("You can create multiple users using commas and spaces (\", \") like so:")
    print("Jesse James, Billy the Kid, Butch Cassidy")
    print("")
    user_strings = input("Enter your name(s): ").split(", ")
    print()
    print("You'll now be prompted to choose a password. (The cursor will not move)")
    print()
    users = []
    # Prompt for a password for each username entered
    for username in user_strings:
        password = getpass(f"Enter a password for {username}: ")
        hashed_password = werkzeug.security.generate_password_hash(password)
        users.append(
            {"username": username, "hashed_password": hashed_password})
    print()
    print("Users created")
    print()
    return users


def prompt_wan_lan():
    """
    Explain online and local modes, and prompt for a choice
    """
    print()
    print("Would you like to make you cattle records accessible online?")
    print()
    print("Choosing \"yes\" will configure your internet connection to allow you")
    print("to access your records from anywhere. However, it will be less secure")
    print()
    print("Choosing \"no\" will be more secure, however, you'll only be able to")
    print(f"access your records from this network ({get_network_ssid()}).")
    print()
    response = input("Access cattle records online? (yes/no): ")
    return response.lower() in ["yes", "y"]


def prompt_for_upnp_wizard():
    """
    Ask the user to install UPnP Wizard on Windows.
    """
    print("We ran into a small problem.")
    print("We need to install an additional program to automatically configure your network.")
    print("Don't worry, all you need to do is go to:")
    print("https://www.xldevelopment.net/upnpwiz.php")
    print("And download and install the UPnP wizard.")
    # Wait for wizard to be installed
    while not upnp_wizard_installed():
        if input("Press ENTER to continue after UPnP wizard is installed or "
                 "press \"c\" to switch to local mode.") == "C":
            break
