"""
This module is responsible for all API calls related to the
fetching and modification of transactions.
"""
import json
import logging

from flask import Blueprint, abort
from flask_simplelogin import login_required

from helper.api_helper_functions import request_data_parser
from models.database import db
from models.cow import get_cow_from_tag
from models.event import Event
from models.transaction import Transaction

logger = logging.getLogger(__name__)

api_transactions = Blueprint(
    'api_transactions', __name__, template_folder='templates')


@api_transactions.route("/add", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def add_transaction_api_transactions(event_id, name, description, to_from, price):
    """
    Submission route for new transactions
    """
    event = Event.query.filter_by(
        event_id=event_id).first()
    if not event:
        abort(404, "Event not found")
    transaction = Transaction(
        event=event,
        cows=event.cows,
        name=name,
        tofrom=to_from,
        price=price,
        description=description
    )
    db.session.add(transaction)
    db.session.commit()

    return json.dumps({
        "operation": "add_transaction",
        "result": "success"
    })


@api_transactions.route("/transaction", methods=["GET"])
@login_required(basic=True)
@request_data_parser
def show_transaction_api_transactions(transaction_id):
    """
    Route for fetching detailed information about a transaction
    """
    transaction = Transaction.query.filter_by(
        transaction_id=transaction_id).first()
    if not transaction:
        abort(404, "Transaction not found")
    return transaction.serialize([
        "transaction_id",
        "name",
        "date",
        "price",
        "tofrom",
        "description",
        "isArchived",
        {"cows": ["tag_number", "sex"]},
        {"event": ["event_id", "name", "date"]}
    ])


@api_transactions.route("/get_list", methods=["GET"])
@login_required(basic=True)
def get_transaction_list():
    """
    Returns a list of transactions with basic information

    Input:
        None

    Output:
        [
            {
                "id": Int
                "name": String
                "cows": [
                    String (tag number)
                ],
                "is_archived": Boolean
                "date": String (ISO date)
                "price": Number
                "formatted_price": String
            }
        ]
    """
    transactions = Transaction.query.all()
    serial_transactions = list(map(
        lambda transaction: transaction.serialize([
            "transaction_id",
            "name",
            "isArchived",
            "date",
            "price",
            {"cows": {"tag_number"}}
        ]),
        transactions
    ))
    serial_transactions.sort(key=lambda a: a["date"], reverse=True)
    return json.dumps(serial_transactions)


@api_transactions.route("/get_cows", methods=["GET"])
@login_required(basic=True)
@request_data_parser
def get_cows_api_transactions(transaction_id):
    """
    Fetch the cows associated with a particular transaction
    """
    transaction = Transaction.query.filter_by(
        transaction_id=transaction_id).first()
    if not transaction:
        abort(404, "Transaction not found")
    return json.dumps(transaction.serialize([{"cows": ["tag_number", "sex"]}]))


@api_transactions.route("/update_cows", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def update_cows_api_transactions(transaction_id, cows):
    """
    Submission route for modifying the cows associated with a transactions.

    Should recieve a list of ALL cows that were involved,
    which overwrites the existing list.
    """
    transaction = Transaction.query.filter_by(
        transaction_id=transaction_id).first()
    if not transaction:
        abort(404, "Transaction not found")
    transaction.cows = [get_cow_from_tag(
        tag_number) for tag_number in cows]

    # If we add a cow to a transaction, it should be
    # added to the parent event too
    for tag_number in cows:
        cow = get_cow_from_tag(tag_number)
        if cow not in transaction.event.cows:
            transaction.event.cows.append(cow)

    db.session.commit()
    return json.dumps({
        "operation": "update_cows",
        "result": "success"
    })


@api_transactions.route("/change_name", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def change_name_api_transactions(transaction_id, name):
    """
    Submission api for changing the name of a transaction
    """
    transaction = Transaction.query.filter_by(
        transaction_id=transaction_id).first()
    if not transaction:
        abort(404, "Transaction not found")
    transaction.name = name
    db.session.commit()
    return json.dumps({
        "operation": "change_name",
        "result": "success"
    })


@api_transactions.route("/change_description", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def change_description_api_transactions(transaction_id, description):
    """
    Submission api for changing the description of a transaction
    """
    transaction = Transaction.query.filter_by(
        transaction_id=transaction_id).first()
    if not transaction:
        abort(404, "Transaction not found")
    transaction.description = description
    db.session.commit()
    return json.dumps({
        "operation": "change_description",
        "result": "success"
    })


@api_transactions.route("/change_to_from", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def change_to_from_api_transactions(transaction_id, to_from):
    """
    Submission api for changing the to/from field of a transaction
    """
    transaction = Transaction.query.filter_by(
        transaction_id=transaction_id).first()
    if not transaction:
        abort(404, "Transaction not found")
    transaction.tofrom = to_from
    db.session.commit()
    return json.dumps({
        "operation": "change_to_from",
        "result": "success"
    })


@api_transactions.route("/change_price", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def change_price_api_transactions(transaction_id, price):
    """
    Submission api for changing the price of a transaction
    """
    transaction = Transaction.query.filter_by(
        transaction_id=transaction_id).first()
    if not transaction:
        abort(404, "Transaction not found")
    transaction.price = price
    db.session.commit()
    return json.dumps({
        "operation": "change_price",
        "result": "success"
    })


@api_transactions.route("/archive", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def api_transactions_archive(transaction_id):
    """
    Submission route for archiving an transaction
    """
    transaction = Transaction.query.filter_by(
        transaction_id=transaction_id).first()
    if not transaction:
        abort(404, "Transaction not found")
    transaction.isArchived = True
    db.session.commit()
    return json.dumps({
        "operation": "archive_transaction",
        "result": "success"
    })


@api_transactions.route("/unarchive", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def api_transactions_unarchive(transaction_id):
    """
    Submission route for unarchiving an transaction
    """
    transaction = Transaction.query.filter_by(
        transaction_id=transaction_id).first()
    if not transaction:
        abort(404, "Transaction not found")
    transaction.isArchived = False
    db.session.commit()
    return json.dumps({
        "operation": "unarchive_transaction",
        "result": "success"
    })


@api_transactions.route("/delete", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def api_transactions_delete_transaction(transaction_id):
    """
    Transaction deletion route
    """
    transaction = Transaction.query.filter_by(
        transaction_id=transaction_id).first()
    if not transaction:
        abort(404, "Transaction not found")
    db.session.delete(transaction)
    db.session.commit()
    return json.dumps({
        "operation": "delete_transaction",
        "result": "success"
    })
