"""
This module is responsible for all API calls related to the
fetching and modification of cows.
"""
import json
import logging

import arrow
from arrow.parser import ParserError
from flask import Blueprint, abort
from flask_simplelogin import login_required

from helper.api_helper_functions import request_data_parser
from helper.sexes import get_sexes
from models.cow import Cow
from models.database import db
from models.cow import get_cow_from_tag
from models.event import Event
from models.transaction import Transaction

logger = logging.getLogger(__name__)

api_cows = Blueprint('api_cows', __name__, template_folder='templates')


@api_cows.route("/add", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def add_cow(tag_number, owner, sex, description, dam="Not Applicable", sire="Not Applicable", born_event_enabled=False, calved_event_enabled=False, date=None):
    """
    This route is responsible for creating new
    cows and for adding calves to cows.

    TODO: Error and provide feedback setting a dam/sire that doesn't exist
    TODO: Better handling of N/A dams and sires
    """
    # Don't let anyone name a cow this.
    if tag_number in ["N/A", "NA", "Not Applicable"]:
        logger.warning("Attempted to add cow with invalid tag number")
        abort(400, "Forbidden tag number")
    # Don't overwrite existing cows
    if get_cow_from_tag(tag_number):
        logger.warning("Cow exists")
        abort(400, "Cow exists")
    # Don't allow invalid sexes
    if sex not in get_sexes("both"):
        abort(400, "Invalid sex")

    if dam == "Not Applicable":
        # "Not Applicable" for no dam
        logger.debug("No dam")
        dam_id = None
    elif get_cow_from_tag(dam) is None:
        # Dam provided, but could not be found in database
        abort(404, "Dam not found")
    else:
        # Set dam if provided
        dam_id = get_cow_from_tag(dam).cow_id

    if sire == "Not Applicable":
        # "Not Applicable" for no sire
        logger.debug("No sire")
        sire_id = None
    elif get_cow_from_tag(sire) is None:
        # Sire provided, but could not be found in database
        abort(404, "Sire not found")
    else:
        # Set sire if provided
        sire_id = get_cow_from_tag(sire).cow_id

    cow = Cow(
        dam_id=dam_id,
        sire_id=sire_id,
        tag_number=tag_number,
        owner=owner,
        sex=sex,
        description=description
    )
    logger.debug("Cow created")

    if (born_event_enabled or calved_event_enabled) and not date:
        logger.warning("Date is required if event is enabled")
        abort(400, "Date is required if event is enabled")

    # Create a "born" event for new calves, if requested
    if born_event_enabled:
        born_event = Event(
            date=date,
            name="Born",
            description=f"{dam} gave birth to {tag_number}",
            cows=[cow]
        )
        db.session.add(born_event)
        logger.debug("Born event added")

    # Create a "calved" event for the cow, if requested
    if calved_event_enabled:
        calved_event = Event(
            date=date,
            name="Calved",
            description=f"{dam} gave birth to {tag_number}",
            cows=[get_cow_from_tag(dam)])
        db.session.add(calved_event)
        logger.debug("Calved event added")

    db.session.add(cow)
    logger.debug("Cow added")
    db.session.commit()
    return json.dumps({
        "operation": "add_cow",
        "result": "success"
    })


@api_cows.route("/add_parent", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def add_parent(tag_number, dam, sire):
    """
    This route is responsible for modifying the
    dam and sire of an existing cow.

    TODO: Document expected N/A behavior
    """
    cow = get_cow_from_tag(tag_number)
    if not cow:
        abort(404, "Cow not found")

    if dam not in ["N/A", "NA", "Not Applicable"]:
        logger.debug(f"Adding dam to {cow.tag_number}")
        cow.dam_id = get_cow_from_tag(dam).cow_id
    else:
        logger.debug(f"Removing dam from {cow.tag_number}")
        cow.dam_id = None

    if sire not in ["N/A", "NA", "Not Applicable"]:
        logger.debug(f"Adding sire to {cow.tag_number}")
        cow.sire_id = get_cow_from_tag(sire).cow_id
    else:
        logger.debug(f"Removing sire from {cow.tag_number}")
        cow.sire_id = None

    db.session.commit()
    return json.dumps({
        "operation": "add_parent",
        "result": "success"
    })


@api_cows.route("/cow", methods=["GET"])
@login_required(basic=True)
@request_data_parser
def show_cow(tag_number):
    """
    This route is responsible for providing all of the details
    for a particular cow.
    """

    cow = get_cow_from_tag(tag_number)
    if not cow:
        abort(404, "Cow not found")
    logger.debug(f"Retrieved cow {cow.tag_number}")
    return cow.serialize([
        "tag_number",
        "cow_id",
        "owner",
        "birthdate",
        "deathdate",
        {"dam": ["tag_number"]},
        {"sire": ["tag_number"]},
        "sex",
        "isArchived",
        "description",
        {"calves": [
            "tag_number",
            "sex"
        ]},
        {"events": [
            "event_id",
            "name",
            "date"
        ]},
        {"transactions": [
            "transaction_id",
            "name",
            "date",
            "price"
        ]}
    ])


@api_cows.route("/get_list", methods=["GET"])
@login_required(basic=True)
def get_cow_list():
    """
    This route provides a list of all the cows in the herd.

    TODO: Rewrite this to use dictionaries
    TODO: Lazy loading?
    """
    cows = Cow.query.all()
    return json.dumps(list(map(
        lambda cow: cow.serialize(["tag_number", "isArchived", "sex"]),
        cows
    )))


@api_cows.route("/get_possible_parents", methods=["GET"])
@login_required(basic=True)
@request_data_parser
def get_possible_parents(parent_type):
    """
    This route provides the tag number of every cow
    in the herd that could potentially be a dam or sire.
    """
    cows = Cow.query.filter_by(isArchived=False)
    if parent_type == "sire":
        parent_sex = "male"
    elif parent_type == "dam":
        parent_sex = "female"
    else:
        logger.warning("Recieved unrecognized sex")
        abort(400, "Invalid parent type")
    parents = [
        cow.tag_number for cow in cows if cow.sex in get_sexes(parent_sex, True)]
    if len(parents) > 0:
        status_code = 200
    else:
        logger.warning(f"No possible {parent_type}s found")
        status_code = 404
    return json.dumps({
        "parent_type": parent_type,
        "parents": parents}), status_code


@api_cows.route("/get_sex_list", methods=["GET"])
@login_required(basic=True)
def get_sex_list():
    """
    This route provides the list of possible sexes as specified in models.py
    """
    return json.dumps(get_sexes("both"))


@api_cows.route("/set_birthdate", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def set_birthdate(tag_number, birthdate):
    """
    Submission route to set the birthdate of a cow, regardless of whether a born event exists
    """
    # Look up the cow
    cow = get_cow_from_tag(tag_number)
    if not cow:
        logger.warning(f"Cow {tag_number} not found")
        abort(404, "Cow with the given tag number does not exist")
    try:
        arrow.get(birthdate, "YYYY-MM-DD")
    except ParserError:
        abort(400, "Invalid date. (Expecting yyyy-mm-dd)")

    # Find born event
    for event in cow.events:
        if event.name == "Born":
            # Remove born event if blank birthdate set
            if birthdate == "":
                logger.debug(f"Deleting birthdate for {tag_number}")
                db.session.delete(event)
                db.session.commit()
                return json.dumps({
                    "operation": "set_birthdate",
                    "result": "success"
                }), 200
            # Update born event if birthdate provided
            else:
                logger.debug(f"Updating birthdate for {tag_number}")
                event.date = birthdate
                db.session.commit()
                return json.dumps({
                    "operation": "set_birthdate",
                    "result": "success"
                }), 200
    # Create born event if none exists
    logger.debug(f"Creating a new born event for {tag_number}")
    born_event = Event(
        date=birthdate,
        name="Born",
        description=f"{tag_number} was born",
        cows=[cow]
    )
    db.session.add(born_event)
    db.session.commit()

    return json.dumps({
        "operation": "set_birthdate",
        "result": "success"
    }), 200


@api_cows.route("/change_tag", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def change_tag(old_tag, new_tag):
    """
    Submission route for updating the tag number of an
    existing cow.
    """
    cow = get_cow_from_tag(old_tag)

    if not cow:
        abort(404, "Old tag doesn't exist")
    cow.tag_number = new_tag
    db.session.commit()
    return json.dumps({
        "operation": "change_tag",
        "result": "success"
    })


@api_cows.route("/change_description", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def change_description(tag_number, description):
    """
    Submission route for updating the description of an
    existing cow.
    """
    cow = get_cow_from_tag(tag_number)
    if not cow:
        abort(404, "Cow not found")
    cow.description = description
    db.session.commit()
    return json.dumps({
        "operation": "change_description",
        "result": "success"
    })


@api_cows.route("/change_sex", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def change_sex(tag_number, sex):
    """
    Submission route for updating the sex of an
    existing cow.
    """
    cow = get_cow_from_tag(tag_number)
    if not cow:
        abort(404, "Cow not found")
    if sex not in get_sexes("both"):
        logger.warning("Recieved unrecognized sex")
        abort(400, "Invalid sex")
    cow.sex = sex
    db.session.commit()
    return json.dumps({
        "operation": "change_sex",
        "result": "success"
    })


@api_cows.route("/transfer_ownership", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def transfer_ownership(tag_number, new_owner, price, date, description):
    """
    Submission route for transferring an existing cow to a new
    owner.
    """
    # Change the cows owner
    cow = get_cow_from_tag(tag_number)
    if not cow:
        abort(404, "Cow not found")

    # Create a "sold" transaction
    logger.debug("Creating sold transaction")
    sale_transaction = Transaction(
        name="Sold",
        description=f"{cow.owner} sold {cow.tag_number}: {description}",
        price=price, tofrom=new_owner, cows=[
            cow]
    )

    # Create a "transfer" event and attach the transaction
    logger.debug("Creating transfer event")
    sale_event = Event(
        date=date,
        name="Transfer",
        description=f"Transfer {cow.tag_number} from {cow.owner}"
        f" to {new_owner}:\n"
        f"{description}",
        cows=[cow],
        transactions=[sale_transaction]
    )

    logger.debug("Updating owner")
    cow.owner = new_owner

    db.session.add(sale_event)
    db.session.add(sale_transaction)
    db.session.commit()
    return json.dumps(
        {"operation": "transfer_ownership", "result": "success"}).encode("utf-8")


@api_cows.route("/record_death_loss", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def record_death_loss(tag_number, date, cause):
    """
    Submission route to record the death loss of a cow
    """
    cow = get_cow_from_tag(tag_number)
    if not cow:
        abort(404, "Cow not found")
    logger.debug("Archiving cow")
    cow.isArchived = True
    # Create a "transfer" event and attach the transaction
    logger.debug("Creating death event")
    death_event = Event(
        date=date,
        name="Died",
        description=f"{cow.tag_number} died:\n"
        f"{cause}",
        cows=[cow]
    )
    db.session.add(death_event)
    db.session.commit()
    return json.dumps(
        {"operation": "record_death_loss", "result": "success"}).encode("utf-8")


@api_cows.route("/archive", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def archive(tag_number):
    cow = get_cow_from_tag(tag_number)
    if not cow:
        abort(404, "Cow not found")

    cow.isArchived = True
    db.session.commit()
    return json.dumps({
        "operation": "archive_cow",
        "result": "success"
    })


@api_cows.route("/unarchive", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def unarchive(tag_number):
    """
    Submission route for unarchiving an archived cow.
    """

    cow = get_cow_from_tag(tag_number)
    if not cow:
        abort(404, "Cow not found")
    cow.isArchived = False
    db.session.commit()
    return json.dumps({
        "operation": "unarchive_cow",
        "result": "success"
    })


@api_cows.route("/delete", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def delete_cow(tag_number):
    """
    Route to delete an existing cow permanently
    """
    cow = get_cow_from_tag(tag_number)
    if not cow:
        abort(404, "Cow not found")
    db.session.delete(cow)
    db.session.commit()
    return json.dumps({
        "operation": "delete_cow",
        "result": "success"
    })
