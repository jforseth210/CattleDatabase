"""
This module is responsible for all api calls
that don't directly relate to cows, events, or
transactions.
"""
import json

from flask import Blueprint
from flask_simplelogin import login_required
from helper.setup_utils import get_private_ip, get_public_ip
from routes.api.api_cows import api_cows
from routes.api.api_events import api_events
from routes.api.api_transactions import api_transactions
import logging

logger = logging.getLogger(__name__)

api = Blueprint('api', __name__, template_folder='templates')


@api.errorhandler(400)
def bad_request_error(error):
    """
    Display a custom 400 page.
    """
    logger.warning(f"400 bad request: {error.description}")
    return json.dumps({
        "result": "failure",
        "reason": error.description
    }), 400


@api.errorhandler(404)
def not_found_error(error):
    """
    Display a custom 404 page.
    """
    logger.debug(error.description)
    return json.dumps({
        "result": "failure",
        "reason": error.description
    }), 404


@api.route("/test_credentials", methods=["POST"])
@login_required(basic=True)
def test_credentials():
    """
    Checks the auth header and returns true if login is good.

    Input:
        None

    Output:
        {
            "succeeded": True
        }

        Or redirection to login page if unsuccessful
    """
    return json.dumps({"operation": "test_credentials", "succeeded": "True"})


@api.route("/get_server_info", methods=["GET"])
def get_server_info():
    """
    Api for fetching basic address information about the server

    Input:
        None

    Output:
        {
            "LAN_address": String (IPv4 Address)
            "WAN_address": String (IPv4 Address)
        }
    """
    return json.dumps({
        "LAN_address": get_private_ip(),
        "WAN_address": get_public_ip()
    })


api.register_blueprint(api_cows, url_prefix="/cows")
api.register_blueprint(api_events, url_prefix="/events")
api.register_blueprint(api_transactions, url_prefix="/transactions")
