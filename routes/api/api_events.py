"""
This module is responsible for all API calls related to the
fetching and modification of events.
"""
import json
import logging

from flask import Blueprint, abort
from flask_simplelogin import login_required
from sqlalchemy.exc import IntegrityError

from helper.api_helper_functions import request_data_parser
from models.database import db
from models.cow import get_cow_from_tag
from models.event import Event

logger = logging.getLogger(__name__)
api_events = Blueprint('api_events', __name__, template_folder='templates')


@api_events.route("/add", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def add_event_api_events(cows, name, date, description):
    """
    Submission route for creating new events
    """
    event = Event(
        cows=[get_cow_from_tag(cow) for cow in cows],
        name=name,
        date=date,
        description=description
    )
    db.session.add(event)
    db.session.commit()

    return json.dumps({
        "operation": "add_event",
        "result": "success"
    })


@api_events.route("/event", methods=["GET"])
@login_required(basic=True)
@request_data_parser
def show_event_api_events(event_id):
    """
    Route for fetching all of the information about a particular event.
    """
    event = Event.query.filter_by(event_id=event_id).first()
    transactions = []
    if not event:
        abort(404, "Event not found")
    for transaction in event.transactions:
        transactions.append({
            "id": transaction.transaction_id,
            "name": transaction.name,
            "price": transaction.price,
            "formatted_price": transaction.get_formatted_price(),
        })
    return event.serialize([
        "event_id",
        "date",
        "description",
        "name",
        "isArchived",
        {"cows": ["tag_number", "sex"]},
        {"transactions": ["transaction_id", "name", "price"]}
    ])


@api_events.route("/get_list", methods=["GET"])
@login_required(basic=True)
def get_event_list():
    """
    Route for fetching a basic list of events
    """
    events = Event.query.all()
    serial_events = list(map(
        lambda event: event.serialize([
            "event_id",
            "name",
            "isArchived",
            "date"
        ]),
        events
    ))
    serial_events.sort(key=lambda a: a["date"], reverse=True)
    return json.dumps(serial_events)


@api_events.route("/get_cows", methods=["GET"])
@login_required(basic=True)
@request_data_parser
def get_cows_api_events(event_id):
    """
    Route for fetching information about an event's cows.
    """
    event = Event.query.filter_by(event_id=event_id).first()
    if not event:
        abort(404, "Event not found")
    if event.cows:
        return json.dumps([cow.serialize(["tag_number", "sex"]) for cow in event.cows])
    else:
        logger.debug("Event has no cows")
        return []


@api_events.route("/update_cows", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def update_cows_api_events(event_id, cows, mode="overwrite"):
    """
    Submission route for changing which cows were incolved in an event.

    By default, api should recieve a list of ALL cows that were involved,
    which overwrites the existing list.

    In append mode, provided cows are added to existing cows
    """
    event = Event.query.filter_by(event_id=event_id).first()
    if not event:
        abort(404, "Event not found")

    # Overwrite mode replace all cows
    if mode == "overwrite":
        logger.debug("Updating cows using overwrite mode")
        event.cows = [get_cow_from_tag(tag_number)
                      for tag_number in cows]

    # Append mode add to cows (and dedupe)
    elif mode == "append":
        logger.debug("Updating cows using append mode")
        for tag_number in cows:
            cow = get_cow_from_tag(tag_number)
            if cow not in event.cows:
                event.cows.append(get_cow_from_tag(tag_number))

    db.session.commit()
    return json.dumps({
        "operation": "update_cows",
        "result": "success"
    })


@api_events.route("/change_name", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def change_name_api_events(event_id, name):
    """
    Submission route for changing the name of an event
    """
    event = Event.query.filter_by(
        event_id=event_id).first()
    if not event:
        return "{}", 404
    event.name = name
    db.session.commit()
    return json.dumps({
        "operation": "change_name",
        "result": "success"
    })


@api_events.route("/change_description", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def change_description_api_events(event_id, description):
    """
    Submission route for changing the description of an event
    """
    event = Event.query.filter_by(
        event_id=event_id).first()
    if not event:
        return "{}", 404
    event.description = description
    db.session.commit()
    return json.dumps({
        "operation": "change_description",
        "result": "success"
    })


@api_events.route("/change_date", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def change_date_api_events(event_id, date):
    """
    Submission route for changing the date of an event
    """
    event = Event.query.filter_by(event_id=event_id).first()
    if event:
        event.date = date
        db.session.commit()
        return json.dumps({
            "operation": "change_date",
            "result": "success"
        })
    abort(404, "Event not found")


@api_events.route("/archive", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def api_events_archive(event_id):
    """
    Submission route for archiving an event
    """
    event = Event.query.filter_by(event_id=event_id).first()
    if not event:
        abort(404, "Event not found")
    event.isArchived = True
    db.session.commit()
    return json.dumps({
        "operation": "archive_event",
        "result": "success"
    })


@api_events.route("/unarchive", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def api_events_unarchive(event_id):
    """
    Submission route for unarchiving an event
    """
    event = Event.query.filter_by(event_id=event_id).first()
    if not event:
        abort(404, "Event not found")
    event.isArchived = False
    db.session.commit()
    return json.dumps({
        "operation": "unarchive_event",
        "result": "success"
    })


@api_events.route("/delete", methods=["POST"])
@login_required(basic=True)
@request_data_parser
def api_events_delete_event(event_id):
    """
    Route to permanently delete an event.
    """
    event = Event.query.filter_by(event_id=event_id).first()
    if not event:
        abort(404, "Event not found")
    db.session.delete(event)
    try:
        db.session.commit()

    # This error is thrown when there are still transactions
    # attached to the event. Tell the client to deal with it.
    except IntegrityError:
        abort(400, "Transactions still exist")
    return json.dumps({
        "operation": "delete_event",
        "result": "success"
    })
