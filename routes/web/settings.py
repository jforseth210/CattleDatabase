"""
Module for handling all of the routes related to CattleDB settings and
"""
import json
import os
import platform
import subprocess
from models.user import User
from models.database import db
try:
    import miniupnpc
except ImportError:
    # For testing purposes, we need something to mock:
    class miniupnpc:
        """
        Dummy class
        """
import werkzeug.security
from send2trash import send2trash
from flask import Blueprint, request, render_template, redirect, flash, send_file
from flask_inertia import render_inertia
from flask_simplelogin import login_required, get_username
from helper.setup_utils import (
    get_network_ssid,
    get_private_ip,
    get_public_ip,
    add_upnp_rule,
    upnp_wizard_installed,
    UPNP_DESCRIPTION,
    PORT
)
from models.cow import Cow
from helper.setup_utils import get_usernames
from helper.login_checker import login_checker
from helper.api_helper_functions import request_data_parser
settings = Blueprint('settings', __name__, template_folder='templates')


@settings.route("/")
@login_required()
def settings_page():
    """
    Route for the main settings screen
    """
    users = get_usernames()
    return render_inertia("Settings", props=dict(ssid=get_network_ssid(), users=users))


@settings.route("/add_user", methods=["POST"])
@login_required()
@request_data_parser
def new_user(username: str, password: str):
    """
    Submission route for the creation of a new user
    """
    message = f"User {username} was added successfully."

    if User.query.filter_by(username=username).first():
        flash(
            f"Username {username} is already taken. Please choose a different username.")
        return redirect("/settings")
    user = User(username=username,
                hashed_password=werkzeug.security.generate_password_hash(password))
    db.session.add(user)
    db.session.commit()
    flash(message)
    return redirect("/settings")


@settings.route("/change_username", methods=["POST"])
@login_required()
@request_data_parser
def change_username(new_username: str, change_cow_ownership: bool = False):
    """
    Submission route to change a username
    """
    username = get_username()
    if User.query.filter_by(username=new_username).first():
        flash(
            f"Username {new_username} is already taken. Please choose a different username.")
        return redirect("/settings")

    if change_cow_ownership:
        cows = Cow.query.filter_by(owner=username)
        for cow in cows:
            cow.owner = new_username
    user = User.query.filter_by(username=username).first()
    user.username = new_username

    db.session.commit()
    flash(f"Username changed to {new_username}")
    return redirect("/logout")


@settings.route("/change_password", methods=["POST"])
@login_required()
@request_data_parser
def change_password(old_password: str, new_password: str, confirm_new_password: str):
    """
    Submission route to change a username
    """
    username = get_username()
    user = User.query.filter_by(username=username).first()
    if new_password != confirm_new_password:
        flash("New passwords do not match!")
        return redirect("/settings")
    if not werkzeug.security.check_password_hash(user.hashed_password, old_password):
        flash("Incorrect old password")
        return redirect("/settings")
    user.hashed_password = werkzeug.security.generate_password_hash(
        new_password)
    flash("Password changed successfully")
    return redirect("/settings")


@settings.route("/check_upnp")
@login_required()
def check_upnp():
    """
    Route to request information about the currently configured UPnP information

    Output:
        {
            "check_successful": Boolean,
            "default_port": Int,
            "rules": [
                {
                    "port": Int,
                    "protocol": "TCP" or "UDP",
                    "toAddr": String (IPv4 Address),
                    "toPort": String (IPv4 Address),
                    "desc": String
                }
            ],
            "ssid": String,
            "host_private_ip_address": String (IPv4 Address),
            "host_public_ip_address": String (IPv4 Address)
        }
        OR
        {
            "check_successful": Boolean,
            "failure_reason": String,
            "rule_exists": None
        }
    """
    if platform.system() == "Windows":
        if not upnp_wizard_installed():
            return json.dumps(
                {
                    "check_successful": False,
                    "failure_reason": "UPnP Wizard not installed",
                    "rule_exists": None
                }
            )
        raw_output = ""
        attempts = 0
        # Sometimes it doesn't work. Give it a few tries before giving up
        while attempts < 5 and raw_output == "":
            try:
                raw_output = subprocess.check_output(
                    r'"C:\Program Files (x86)\UPnP Wizard\UPnPWizardC.exe" -legacy -list'
                ).decode("utf-8").replace("\t", "")
            except subprocess.CalledProcessError:
                print("Failed to check UPnP rules. Trying again....")
            attempts += 1
        if raw_output == "":
            print("Still failing to check UPnP rules. Giving up...")
            print("Try running:")
            print(r'"C:\Program Files (x86)\UPnP Wizard\UPnPWizardC.exe" -legacy -list')
            print("manually in the command prompt.")
            return json.dumps(
                {
                    "check_successful": False,
                    "failure_reason": "UPnP Wizard command failed",
                    "rule_exists": None
                }
            )
        raw_output_lines = raw_output.split("\r\n")
        descriptions = [line.replace("Mapping Description: ", "")
                        for line in raw_output_lines if "Description" in line]
        protocols = [line.split(": ")[1]
                     for line in raw_output_lines if "Protocol" in line]
        to_ports = [line.split(": ")[1]
                    for line in raw_output_lines if "Internal Port" in line]
        ports = [line.split(": ")[1]
                 for line in raw_output_lines if "External Port" in line]
        to_addrs = [line.split(": ")[1]
                    for line in raw_output_lines if "Internal IP" in line]

        rules = []
        rule_zip = zip(ports, protocols, to_addrs, to_ports, descriptions)
        for port, protocol, to_addr, to_port, description in rule_zip:
            if description == UPNP_DESCRIPTION:
                rules.append({
                    "port": port,
                    "protocol": protocol,
                    "toAddr": to_addr,
                    "toPort": to_port,
                    "desc": description
                })

        return json.dumps({
            "check_successful": True,
            "default_port": PORT,
            "rules": rules,
            "ssid": get_network_ssid(),
            "host_private_ip_address": get_private_ip(),
            "host_public_ip_address": get_public_ip()
        })
    upnp = miniupnpc.UPnP()
    upnp.discoverdelay = 200
    upnp.discover()
    upnp.selectigd()

    rule = 1
    i = 0
    rules = []
    while rule:
        rule = upnp.getgenericportmapping(i)
        i += 1
        if rule:
            port, protocol, (to_addr, to_port), desc, _, _, _ = rule
            if desc == UPNP_DESCRIPTION:
                rules.append({
                    "port": port,
                    "protocol": protocol,
                    "toAddr": to_addr,
                    "toPort": to_port,
                    "desc": desc
                })
    return json.dumps({
        "check_successful": True,
        "rules": rules,
        "default_port": PORT,
        "ssid": get_network_ssid(),
        "host_private_ip_address": get_private_ip(),
        "host_public_ip_address": get_public_ip()
    })


@settings.route("/add_upnp")
@login_required()
def add_upnp():
    """
    Route to request the creation of a UPnP rule
    """
    if add_upnp_rule():
        flash("Successfully added UPnP rule")
    else:
        flash("Failed to add UPnP rule")
    return redirect(request.referrer)


@settings.route("/delete_upnp")
@login_required()
def delete_upnp():
    """
    Route to request the deletion of a UPnP rule
    """
    if platform.system() == "Windows":
        if not upnp_wizard_installed():
            return render_template("installer/prompt_upnp_wizard.html")
        command = r'"C:\Program Files (x86)\UPnP Wizard\UPnPWizardC.exe" -legacy -remove {}'
        subprocess.run(command.format(UPNP_DESCRIPTION))
        flash("UPnP rule(s) removed successfully.")
        return redirect(request.referrer)
    upnp = miniupnpc.UPnP()
    upnp.discoverdelay = 200
    upnp.discover()
    upnp.selectigd()

    rule = 1
    i = 0
    while rule:
        rule = upnp.getgenericportmapping(i)
        i += 1
        if rule:
            port, protocol, (_, _), desc, _, _, _ = rule
            if desc == UPNP_DESCRIPTION:
                upnp.deleteportmapping(port, protocol)
    flash("UPnP rule(s) removed successfully.")
    return redirect(request.referrer)


@settings.route("/download_database")
@login_required()
def download_database():
    """
    Route to download the cattle.db file
    """
    if not os.path.exists("cattle.db"):
        flash("Unable to download: cattle.db file is missing!")
        raise FileNotFoundError()
    return send_file("cattle.db")


@settings.route("/download_config")
@login_required()
def download_config():
    """
    Route to download the config.json file
    """
    if not os.path.exists("config.json"):
        flash("Unable to download: config.json file is missing!")
        raise FileNotFoundError()
    return send_file("config.json")


@settings.route("/delete_account", methods=["POST"])
@login_required()
@request_data_parser
def delete_account(password: str):
    """
    Route to request the deletion of the currently logged in account
    """
    if not login_checker({"username": get_username(), "password": password}):
        flash("Invalid Password")
        return redirect("/settings")
    username = get_username()
    user = User.query.filter_by(username=username).first()
    if not user:
        return redirect("/settings")
    db.session.delete(user)
    db.session.commit()
    return redirect("/logout")


@settings.route("/delete_database", methods=["POST"])
@login_required()
@request_data_parser
def delete_database(password: str):
    """
    Route to request the deletion of the cattle.db file
    """
    if not login_checker({"username": get_username(), "password": password}):
        flash("Invalid Password")
        return redirect("/settings")
    send2trash("cattle.db")
    print("Successfully deleted the database")
    print("If this action was unintentional please close this window, restore "
          "the file from your computer's recycle bin, and restart CattleDB.")
    return redirect("/install")


@ settings.route("/delete_config", methods=["POST"])
@ login_required()
@request_data_parser
def delete_config(password: str):
    """
    Route to request the deletion of the config.json file
    """
    username = get_username()
    user = User.query.filter_by(username=username).first()
    if not user:
        return redirect("/settings")
    if not login_checker({"username": get_username(), "password": password}):
        flash("Invalid Password")
        return redirect("/settings")
    send2trash("config.json")
    print("Successfully deleted the config")
    print("If this action was unintentional please close this window, restore the file "
          "from your computer's recycle bin, and restart CattleDB.")
    return redirect("/install")


@ settings.route("/delete_both", methods=["POST"])
@ login_required()
@request_data_parser
def delete_both(password: str):
    """
    Route to request the deletion of the config.json file AND the cattle.db file
    """
    username = get_username()
    user = User.query.filter_by(username=username).first()
    if not user:
        return redirect("/settings")
    if not login_checker({"username": get_username(), "password": password}):
        flash("Invalid Password")
        return redirect("/settings")
    send2trash("config.json")
    print("Successfully deleted the config")
    send2trash("cattle.db")
    print("Successfully deleted the database")
    print("If this action was unintentional please close this window, restore the "
          "file from your computer's recycle bin, and restart CattleDB.")
    return redirect("/install")
