"""
This module is responsible for the calendar page on the site, as well as the ical feed.
"""
import json
import secrets
import logging
import arrow
from ics import Calendar as iCalendar
from ics import Event as iEvent
from flask import Blueprint, make_response, request
from flask_inertia import render_inertia
from flask_simplelogin import login_required
from models.event import Event
logger = logging.getLogger(__name__)
calendar = Blueprint('calendar', __name__, template_folder='templates')


@calendar.route('/')
@login_required
def calendar_page():
    """
    Displays a calender with all of the events and a download link.
    """
    logger.debug("Reading config")
    with open("config.json", "r", encoding="UTF-8") as config_file:
        config = json.loads(config_file.read())
    try:
        calendar_secret = config["calendar_secret"]
    except KeyError:
        logger.debug("Calendar secret not found creating one")
        config["calendar_secret"] = secrets.token_urlsafe(32)
        with open("config.json", "w", encoding="UTF-8") as file:
            file.write(json.dumps(config, indent=4))
        logger.debug("Wrote new calendar secret")

    calendar_secret = config["calendar_secret"]
    return render_inertia(
        'Calendar',
        props={"icalendar_route": f"/calendar/ical/{calendar_secret}.ics"}
    )

# The calendar javascript loads its events from here.


@calendar.route('/events/api')
def event_api():
    """
    This is an api for the calendar, not the main api.

    Input:
        None
    Output:
        {
            "title": String,
                - Event Name: Cow1, Cow2, Cow3
            "start": String (ISO date),
                - Start: The date of the event, the calendar supports multi-day events,
                - CattleDB does not.
            "id": Int
                - The event_id
        }
    """
    start = request.args.get("start")
    end = request.args.get("end")

    first_date = arrow.get(start)
    second_date = arrow.get(end)

    all_events = Event.query.all()
    filtered_events = []
    for event in all_events:
        if not event.date:
            continue
        event_date = arrow.get(event.date)
        if event_date < first_date or event_date > second_date:
            continue
        filtered_events.append(event)
    formatted_events = []
    for event in filtered_events:
        # List of the event's cows
        cow_string = ", ".join(cow.tag_number for cow in event.cows)

        formatted_event = {
            'title': event.name + ": " + cow_string,
            'start': event.date,
            'id': event.event_id
        }
        formatted_events.append(formatted_event)
    return json.dumps(formatted_events)


@calendar.route("/ical/<secret>.ics")
def generate_ics(secret):
    """
    Generates an icalendar file with all of the events.
    """
    with open("config.json", "r", encoding="UTF-8") as config_file:
        config = json.loads(config_file.read())
        calendar_secret = config["calendar_secret"]
    if calendar_secret == secret:
        icalendar = iCalendar()
        events = Event.query.all()
        for event in events:
            if event.date:
                ical_event = iEvent()
                cow_string = ", ".join(cow.tag_number for cow in event.cows)
                ical_event.name = event.name + ": " + cow_string
                ical_event.begin = event.date
                ical_event.make_all_day()
                ical_event.url = f"{request.url_root}events/event/{event.event_id}"
                ical_event.attendees = [cow.tag_number for cow in event.cows]
                icalendar.events.add(ical_event)
            else:
                logger.warning(f"Event #{event.event_id} has no date")
        output = make_response(str(icalendar))
        output.headers["Content-Disposition"] = "attachment; filename=CattleDB.ics"
        return output
    return "", 404
