"""
A web-based installation wizard for CattleDB
"""
import os
import json
import platform
import logging
import sqlite3
import sqlalchemy
from flask import Blueprint, render_template, request, redirect, flash, current_app
from models.user import User
from models.cow import Cow
import werkzeug.security
from models.database import db
from helper.api_helper_functions import request_data_parser
from helper.setup_utils import (
    get_private_ip,
    get_public_ip,
    get_network_ssid,
    upnp_wizard_installed,
    check_for_upnp_rule,
    add_upnp_rule,
    PORT
)
logger = logging.getLogger(__name__)
installer = Blueprint('installer', __name__, template_folder='templates')


@installer.route("/")
def installer_start():
    """
    The main installation route. Figures out where we are in the setup
    process and sends the user to the next step.
    """
    
    script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
    rel_path = "../../cattle.db"
    abs_file_path = os.path.join(script_dir, rel_path)
    if not os.path.exists(abs_file_path):
        return render_template("installer/setup_db.html", port=PORT)

    config = {}
    try:
        logger.debug("Reading config")
        script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
        rel_path = "../../config.json"
        abs_file_path = os.path.join(script_dir, rel_path)
        with open(abs_file_path, "r", encoding="utf-8") as file:
            config = json.loads(file.read())
    except FileNotFoundError:
        # logger.debug("Config not found")
        # logger.debug("Creating new config")
        # with open("config.json", "w", encoding="utf-8") as file:
        #    file.write(json.dumps({}))
        return render_template("installer/setup_config.html", port=PORT)
    except UnicodeDecodeError:
        flash(
            "The \"config.json\" file is not valid JSON. Please upload a valid config file.")
        return render_template("installer/setup_config.html", port=PORT)

    try:
        if not User.query.first():
            logger.debug("No users")
            return render_template("installer/create_user.html", port=PORT, allow_skip=False)
    except sqlalchemy.exc.OperationalError:
        return render_template("installer/setup_db.html", port=PORT)

    if config.get("using_wan", None) is None:
        logger.debug("Setting wan/lan")
        return render_template("installer/prompt_wan_lan.html", port=PORT, ssid=get_network_ssid())
    return redirect("/install/show_server_info")


@installer.route("/licenses")
def licenses():
    return render_template("installer/licenses.html")


@installer.route("/import_database", methods=["GET", "POST"])
def import_database():
    """
    A submission route for importing an existing CattleDB database.
    """
    if request.method == "GET":
        return redirect("/install")
    sent_file = request.files["file"]
    if sent_file.filename != "cattle.db":
        spreadsheet_extensions = ["csv", "xlsx", "numbers", "_xls", "xls", "iif", "123", "ods",
                                  "xlsb", "tsv", "et", "mpj", "wk4", "ms", "wk1", "xlsm", "sta",
                                  "pmd", "tdl", "wks", "xl", "sxc", "jnb", "cwk", "dif", "wr1",
                                  "excel", "ecsv", "pmdx", "wb3", "nmbtemplate", "xlt", "vc",
                                  "wk3", "slk", "ks", "pxl", "jzz", "xltx", "fods", "wq1", "ksp",
                                  "sdc", "aws", "qpw", "xmlx", "silk", "wk!", "ots", "xls_", "wb1",
                                  "wb2", "xcel", "mtp", "skv", "spw", "stc", "xltm", "fm3", "dex",
                                  "mtx", "wkz", "wr!", "csvx", "uos", "wkq", "qvo", "mws", "xslb",
                                  "xlxml", "pmv", "xlmx", "wq!", "wq2", "nam", "xs4", "wg2",
                                  "sylk", "wb0", "lgx", "bwb", "xls4", "$$s", "xls5", "wk2",
                                  "xls8", "xlsmhtml", "ett", "wj3", "wku", "wzk", "wki", "otw",
                                  "rs1", "xls3", "wrk", "spr", "wke", "vts", "wk5", "sxls", "fm1",
                                  "pln", "ess", "sht", "xmap", "stw", "imp", "lcw", "smx", "sxlsx",
                                  "tc6", "xlsxe", "cwss", "wks", "wq0", "wb?", "atf", "ssf", "ogw",
                                  "valu", "gsw", "lss", "tcd", "xlw_", "sky", "fp", "tii", "ag",
                                  "syk", "numbers-tef", "mod", "wat", "fj3", "css", "vcx", "wkb",
                                  "ens", "col", "wss", "as", "wg1", "wks", "xqt", "xs5", "pln",
                                  "spr", "awss", "pxd", "ast", "pt", "siag", "dwk", "xss", "sls3",
                                  "xlw4", "mar", "xlw5", "faff", "t", "sls8", "sls5", "pmvx",
                                  "sls4", "xlw3", "pmbak", "swss"]
        flash(
            f"Database must be a valid CattleDB database (\"cattle.db\"). \"{sent_file.filename}\" "
            f"is not allowed. If you're sure \"{sent_file.filename}\" is a valid database (generated "
            "by CattleDB), rename it to \"cattle.db\" and try again. Otherwise, click \"Create a "
            "New Database\" to continue."
        )
        for extension in spreadsheet_extensions:
            if extension in sent_file.filename.split(".")[-1]:
                logger.warning("Recieved spreadsheet instead of database")
                flash("Sorry, spreadsheets are not allowed!")
                return redirect("/install")
        logger.warning("Did not recieve valid cattledb database")
        return redirect("/install")

    try:
        Cow.query.first()
    except (sqlite3.DatabaseError, sqlite3.OperationalError, sqlalchemy.exc.OperationalError):
        flash("The uploaded \"cattle.db\" is not a valid CattleDB database. Please upload a valid database file. If this is an old database (generated by CattleDB), please contact support@jforseth.com for assistance updating it.")
        return redirect("/install")

    sent_file.save("cattle.db")
    return redirect("/install")


@ installer.route("/create_database")
def create_database():
    """
    Route to request the creation of a new CattleDB database.
    """
    logger.debug("Creating new database")
    script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
    rel_path = "../../cattle.db"
    abs_file_path = os.path.join(script_dir, rel_path)

    with open(abs_file_path, "w", encoding="utf-8") as file:
        file.write("")
    with current_app.app_context():
        db.create_all()
        db.session.commit()
    return redirect("/install")


@ installer.route("/import_config", methods=["GET", "POST"])
def import_configuration():
    """
    A submission route for importing an existing CattleDB config.
    """
    if request.method == "GET":
        return redirect("/install")
    sent_file = request.files["file"]
    if sent_file.filename != "config.json":
        logger.warning("Didn't recieve valid config.json")
        script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
        rel_path = "../../cattle.db"
        abs_file_path = os.path.join(script_dir, rel_path)
        if os.path.exists(abs_file_path):
            logger.warning("Removing config.json")
            os.remove(abs_file_path)

        flash(
            f"Config file must be a valid CattleDB config (\"config.json\"). "
            f"{sent_file.filename} is not allowed. If you're sure {sent_file.filename} "
            "is a valid configuration (generated by CattleDB), rename it to \"config.json\" "
            "and try again. Otherwise, click \"Create a New Configuration\" to continue."
        )

        return redirect("/install")
    logger.debug("Saving config.json")

    script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
    rel_path = "../../config.json"
    abs_file_path = os.path.join(script_dir, rel_path)

    sent_file.save(abs_file_path)
    return redirect("/install")


@ installer.route("/create_config")
def create_configuration():
    """
    Route to request the creation of a new CattleDB config.
    """
    logger.debug("Creating new config")
    
    script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
    rel_path = "../../config.json"
    abs_file_path = os.path.join(script_dir, rel_path)
    with open(abs_file_path, "w", encoding="utf-8") as file:
        file.write(json.dumps({}))
    return redirect("/install")


@ installer.route("/create_credentials", methods=["GET", "POST"])
@request_data_parser
def create_credentials(username: str = None, password: str = None, confirm_password: str = None):
    """
    Submission route to create a new user.
    """
    allow_skip = User.query.first() is not None
    if request.method == "GET":
        return render_template("installer/create_user.html", port=PORT, allow_skip=allow_skip)

    if username is None or username == "":
        flash("Username cannot be blank. Please try again.")
        return render_template("installer/create_user.html", port=PORT, allow_skip=allow_skip)

    if User.query.filter_by(username=username).first():
        flash(
            f"Username {username} has already been created. Please choose a different username.")
        return render_template("installer/create_user.html", port=PORT, allow_skip=allow_skip)

    if password is None or password == "":
        flash("Password cannot be blank. Please try again.")
        return render_template("installer/create_user.html", port=PORT, allow_skip=allow_skip)

    if password != confirm_password:
        logger.warning("Passwords don't match")
        flash("Passwords do not match! Please try again.")
        return redirect("/install")
    if User.query.filter_by(username=username).first():
        logger.warning("User already exists")
        flash(
            f"Username {username} has already been created. Please choose a different username.")
        return redirect("/install/create_credentials")
    user = User(username=username,
                hashed_password=werkzeug.security.generate_password_hash(password))
    db.session.add(user)
    db.session.commit()
    return render_template("installer/prompt_create_another_user.html")


@ installer.route("/setup_online_mode")
def setup_online_mode():
    """
    Route which determines what is needed to set up online mode,
    tries to set it up if possible, and lets the user know if
    it succeeded.
    """
    if not upnp_wizard_installed() and platform.system() == "Windows":
        logger.debug("Prompting for wizard")
        return render_template("installer/prompt_upnp_wizard.html")
    if check_for_upnp_rule():
        return render_template("installer/upnp_already_configured.html", port=PORT)

    if add_upnp_rule():
        logger.debug("Added upnp rule")
        script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
        rel_path = "../../config.json"
        abs_file_path = os.path.join(script_dir, rel_path)
        with open(abs_file_path, "r", encoding="utf-8") as file:
            config = json.loads(file.read())
        config["using_wan"] = True
        with open(abs_file_path, "w", encoding="utf-8") as file:
            file.write(json.dumps(config, indent=4))
        return render_template("installer/upnp_success.html")
    logger.debug("upnp failed")
    return render_template("installer/upnp_failure.html")


@ installer.route("/setup_offline_mode")
def setup_offline_mode():
    """
    Route used to indicate that the user doesn't want
    or isn't able to use online mode and that this should
    be written to the config.
    """
    script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
    rel_path = "../../config.json"
    abs_file_path = os.path.join(script_dir, rel_path)
    with open(abs_file_path, "r", encoding="utf-8") as file:
        config = json.loads(file.read())
    config["using_wan"] = False
    with open(abs_file_path, "w", encoding="utf-8") as file:
        file.write(json.dumps(config, indent=4))
    return redirect("/install")


@ installer.route("/show_server_info")
def show_server_info():
    """
    The final route of the installer shown when everything is complete.
    Shows server address information and provides a link to the main
    CattleDB page.
    """
    script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
    rel_path = "../../config.json"
    abs_file_path = os.path.join(script_dir, rel_path)
    with open(abs_file_path, "r", encoding="utf-8") as file:
        config = json.loads(file.read())
    return render_template("installer/show_server_info.html",
                           using_wan=config["using_wan"],
                           ssid=get_network_ssid(),
                           public_ip="http://" + get_public_ip() + ":" + str(PORT),
                           private_ip="http://" + get_private_ip() + ":" + str(PORT)
                           )
