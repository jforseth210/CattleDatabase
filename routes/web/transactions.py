"""
This module is responsible for all routes relating to transaction on the web interface.
"""
import json
import logging

from flask import Blueprint, request, redirect, abort
from flask_inertia import render_inertia
from flask_simplelogin import login_required

from helper.api_helper_functions import request_data_parser
from models.database import db
from models.cow import get_cow_from_tag
from models.event import Event
from models.transaction import Transaction

transactions = Blueprint("transactions", __name__, template_folder="templates")


logger = logging.getLogger(__name__)

# Create


@transactions.route("/add", methods=["POST"])
@login_required
@request_data_parser
def new_transaction(event_id: int, price: float, name: str, tofrom: str, description: str, cows: list=None):
    """
    Submission route for new transactions
    """
    print(cows)
    event = Event.query.filter_by(event_id=event_id).first()

    if not cows:
        cows = event.cows
    
    cows = [get_cow_from_tag(cow) for cow in cows]

    new_transaction_object = Transaction(
        price=price,
        name=name,
        description=description,
        event_id=event_id,
        tofrom=tofrom,
        cows=cows,
    )

    db.session.add(new_transaction_object)
    db.session.commit()
    return redirect(request.referrer + "#transactions")


@transactions.route("/")
@login_required
def send_transactions_page():
    """
    Route for the events page (the table with all the events)
    """
    return render_inertia("Transactions")


@transactions.route("/page/<page>")
@login_required()
@request_data_parser
def list_transactions(page, field: str, direction: str, include_archived: bool):
    if direction != "asc" and direction != "desc":
        abort(400)
    page = int(page)
    # ChatGPT generated a lot of this...

    # Set the number of items per page
    per_page = 25

    # Calculate the offset based on the page number
    offset = page * per_page
    if field == "date":
        query = Transaction.query.join(Event).order_by(
            getattr(Event.date, direction)())
    elif field == "name":
        query = db.session.query(Transaction).order_by(
            getattr(Transaction.name, direction)()
        )
    elif field == "tofrom":
        query = db.session.query(Transaction).order_by(
            getattr(Transaction.tofrom, direction)()
        )
    elif field == "price":
        query = db.session.query(Transaction).order_by(
            getattr(Transaction.price, direction)()
        )
    elif field == "description":
        query = db.session.query(Transaction).order_by(
            getattr(Transaction.description, direction)()
        )

    if not include_archived:
        query = query.filter(Transaction.isArchived == False)

    transaction_list = query.offset(offset).limit(per_page).all()
    desired_fields = [
        "transaction_id",
        "isArchived",
        {"event": ["date"]},
        "tofrom",
        "name",
        "description",
        {"cows": ["tag_number"]},
        "price",
    ]
    serializable_transactions = list(
        map(lambda transaction: transaction.serialize(
            desired_fields), transaction_list)
    )
    return json.dumps(serializable_transactions)


@transactions.route("/transaction/<transaction_id>")
@login_required
def show_transaction(transaction_id):
    """
    The individual transaction information page
    """
    transaction = Transaction.query.filter_by(
        transaction_id=transaction_id).first()
    if not transaction:
        logger.debug(f"Transaction does not exist")
        abort(404)
    return render_inertia(
        "Transaction",
        props={
            "transaction": transaction.serialize(
                [
                    "transaction_id",
                    "isArchived",
                    "description",
                    "price",
                    "tofrom",
                    "name",
                    {
                        "cows": [
                            "tag_number",
                            "birthdate",
                            "owner",
                            "sex",
                            {"sire": ["tag_number"]},
                            {"dam": ["tag_number"]},
                            {"calves": ["tag_number"]},
                            "description",
                        ]
                    },
                    {"event": ["event_id", "date", "name", "description"]},
                ]
            )
        },
    )


# Update


@transactions.route("/archive")
@login_required
def transaction_archive():
    """
    Submission to archive a transaction

    Input:
        {
            "transaction_id": Int
        }

    Output:
        The list of transactions
    """
    transaction_id = request.args.get("transaction_id")
    transaction = Transaction.query.filter_by(
        transaction_id=transaction_id).first()
    if not transaction:
        logger.debug(f"Transaction does not exist")
        abort(404)
    transaction.isArchived = True
    db.session.commit()
    return redirect("/transactions")


@transactions.route("/unarchive")
@login_required
def transaction_unarchive():
    """
    Submission to unarchive a transaction

    Input:
        {
            "transaction_id": Int
        }

    Output:
        Wherever the user came from
    """
    transaction_id = request.args.get("transaction_id")
    transaction = Transaction.query.filter_by(
        transaction_id=transaction_id).first()
    if not transaction:
        logger.debug(f"Transaction does not exist")
        abort(404)
    transaction.isArchived = False
    db.session.commit()
    return redirect(request.referrer)


@transactions.route("/update_cows", methods=["POST"])
@login_required
@request_data_parser
def transaction_add_remove_cows(transaction_id: int, new_cow: str = None, all_cows: list = None):
    """
    This function is used to update the transation's cows.

    Output:
        Wherever the user came from
    """
    if "" in all_cows:
        all_cows.remove("")
    transaction_id = request.form.get("transaction_id")
    transaction = Transaction.query.filter_by(
        transaction_id=transaction_id).first()
    if not transaction:
        logger.debug(f"Transaction does not exist")
        abort(404)
    if all_cows is not None:
        transaction.cows = [get_cow_from_tag(cow) for cow in all_cows]
        for tag_number in all_cows:
            cow = get_cow_from_tag(tag_number)
            if cow not in transaction.event.cows:
                transaction.event.cows.append(cow)
    elif new_cow is not None:
        transaction.cows.append(get_cow_from_tag(new_cow))
        if get_cow_from_tag(new_cow) not in transaction.event.cows:
            transaction.event.cows.append(get_cow_from_tag(new_cow))
    db.session.commit()
    return redirect(request.referrer)


@transactions.route("/change_price", methods=["POST"])
@login_required
@request_data_parser
def transaction_change_price(transaction_id: int, price: float):
    """
    Submission route for changing the price of an transaction.
    """
    transaction = Transaction.query.filter_by(
        transaction_id=transaction_id).first()
    if not transaction:
        logger.debug(f"Transaction does not exist")
        abort(404)
    transaction.price = price
    db.session.commit()
    return redirect(request.referrer)


@transactions.route("/change_description", methods=["POST"])
@login_required
@request_data_parser
def transaction_change_description(transaction_id: int, description: str):
    """
    Submission route for changing the description of a transaction.
    """
    transaction = Transaction.query.filter_by(
        transaction_id=transaction_id).first()
    if not transaction:
        logger.debug(f"Transaction does not exist")
        abort(404)
    transaction.description = description
    db.session.commit()
    return redirect(request.referrer)


@transactions.route("/change_name", methods=["POST"])
@login_required
@request_data_parser
def transaction_change_name(transaction_id: int, name: str):
    """
    Submission route for changing the name of an event
    """
    transaction = Transaction.query.filter_by(
        transaction_id=transaction_id).first()
    if not transaction:
        logger.debug(f"Transaction does not exist")
        abort(404)
    transaction.name = name
    db.session.commit()
    return redirect(request.referrer)


@transactions.route("/change_to_from", methods=["POST"])
@login_required
@request_data_parser
def transaction_change_to_from(transaction_id: int, tofrom: str):
    """
    Submission route for changing  the to/from field of a transaction
    """

    transaction = Transaction.query.filter_by(
        transaction_id=transaction_id).first()
    if not transaction:
        logger.debug(f"Transaction does not exist")
        abort(404)
    transaction.tofrom = tofrom
    db.session.commit()
    return redirect(request.referrer)


# Delete
@transactions.route("/delete", methods=["POST"])
@login_required
@request_data_parser
def delete_transaction(transaction_id: int):
    """
    Route to delete a transaction permanently
    """
    transaction = Transaction.query.filter_by(
        transaction_id=transaction_id).first()
    if not transaction:
        logger.debug(f"Transaction does not exist")
        abort(404)
    db.session.delete(transaction)
    db.session.commit()

    return redirect("/transactions")
