"""
Handles searches
"""
import json
from flask import Blueprint, request
import arrow
from sqlalchemy import or_
from flask_simplelogin import login_required
from flask_inertia import render_inertia
from models.cow import Cow
from models.event import Event
from models.transaction import Transaction
from search_functions import get_unique_values

search = Blueprint("search", __name__, template_folder="templates")


@search.route("/")
@login_required
def search_page():
    """
    This is the main route for the search page
    """
    query = request.args.get("q")
    return render_inertia("Search", props={"query": query})


@search.route("/get_available_filters")
@login_required
def available_filters():
    """
    Find unique values for the search filters.
    For example, if we want to filter by owner, we need to know who the possible owners are.
    """
    return json.dumps(get_unique_values())


@search.route("/search_cows", methods=["POST"])
@login_required
def search_cows():
    """
    Performs the actual search and filtration for cows
    """
    request_json = json.loads(request.get_data())
    query = request_json["query"]
    filters = request_json["filters"]
    results = {}
    cows = Cow.query.filter(
        or_(
            Cow.tag_number.like(f"%{query}%"),
            Cow.sex.like(f"%{query}%"),
            Cow.owner.like(f"%{query}%"),
            Cow.description.like(f"%{query}%"),
        )
    )
    filtered_cows = []
    for cow in cows:
        if len(filters["owners"]) > 0 and cow.owner not in filters["owners"]:
            continue

        if len(filters["sexes"]) > 0 and cow.sex not in filters["sexes"]:
            continue

        if len(filters["tags"]) > 0:
            first_digit = cow.get_first_digit_of_tag()
            matches = False
            for tag in filters["tags"]:
                digit_to_seek = tag.replace("*", "")
                if digit_to_seek == first_digit:
                    matches = True
            if not matches:
                continue

        # Filter by dam
        if filters.get("dam", False):
            dam = cow.get_dam()
            # Cow doesn't have a dam
            if not dam:
                continue
            # Cow has the wrong dam
            if dam.tag_number != filters["dam"]:
                continue

        # Filter by sire
        if filters.get("sire", False):
            sire = cow.get_sire()
            # Cow has no sire
            if not sire:
                continue
            # Cow has the wrong sire
            if sire.tag_number != filters["sire"]:
                continue

        filtered_cows.append(cow)
    results = [cow.to_search_result().to_dict() for cow in filtered_cows]
    return json.dumps(results)


@search.route("/search_events", methods=["POST"])
@login_required
def search_events():
    """
    Performs the actual search and filtration for events
    """
    request_json = json.loads(request.get_data())
    query = request_json["query"]
    filters = request_json["filters"]
    results = {}
    events = Event.query.filter(
        or_(
            Event.date.like(f"%{query}%"),
            Event.name.like(f"%{query}%"),
            Event.description.like(f"%{query}%"),
        )
    )
    filtered_events = []
    for event in events:
        if len(filters["event_names"]) > 0 and event.name not in filters["event_names"]:
            continue
        if len(filters["dates"]) == 2:
            first_date = arrow.get(filters["dates"][0])
            second_date = arrow.get(filters["dates"][1])
            try:
                event_date = arrow.get(event.date)
            except arrow.parser.ParserError:
                continue
            if event_date < first_date or event_date > second_date:
                continue
        filtered_events.append(event)
    results = [event.to_search_result().to_dict() for event in filtered_events]
    return json.dumps(results)


@search.route("/search_transactions", methods=["POST"])
@login_required
def search_transactions():
    """
    Performs the actual search and filtration for transactions
    """
    request_json = json.loads(request.get_data())
    query = request_json["query"]
    filters = request_json["filters"]
    results = {}
    transactions = Transaction.query.filter(
        or_(
            Transaction.price.like(f"%{query}%"),
            Transaction.name.like(f"%{query}%"),
            Transaction.tofrom.like(f"%{query}%"),
            Transaction.description.like(f"%{query}%"),
        )
    )
    filtered_transactions = []
    for transaction in transactions:
        if (
            len(filters["transaction_names"]) > 0
            and transaction.name not in filters["transaction_names"]
        ):
            continue
        if len(filters["tofroms"]) > 0 and transaction.tofrom not in filters["tofroms"]:
            continue
        if len(filters["prices"]) == 2:
            min_price = filters["prices"][0]
            max_price = filters["prices"][1]
            if transaction.price < min_price or transaction.price > max_price:
                continue
        filtered_transactions.append(transaction)
    results = [
        transaction.to_search_result().to_dict()
        for transaction in filtered_transactions
    ]
    return json.dumps(results)
