"""
This module is responsible for all routes relating to events on the web interface.
"""
import json
import logging
from flask import (
    Blueprint,
    request,
    redirect,
    flash,
    abort,
)
from markupsafe import Markup
from flask_simplelogin import login_required
from flask_inertia import render_inertia
from sqlalchemy import func
from sqlalchemy.exc import IntegrityError

from models.event import Event
from models.transaction import Transaction
from models.database import db
from models.cow import get_cow_from_tag
from helper.api_helper_functions import request_data_parser

logger = logging.getLogger(__name__)
events = Blueprint("events", __name__, template_folder="templates")


# Create
@events.route("/add", methods=["POST"])
@login_required
@request_data_parser
def new_event(date: str, name: str, description: str, tag_number: str = None, cows: list[str] = []):
    """
    Submission route for new events
    """
    if cows != [] and tag_number is not None:
        abort(400, "cannot provide cows and tag_number")
    if tag_number:
        cows = [tag_number]
    if "" in cows:
        logging.debug("Removing blank tag number")
        cows.remove("")

    new_event_object = Event(
        date=date,
        name=name,
        description=description,
        cows=[get_cow_from_tag(tag) for tag in cows],
    )

    db.session.add(new_event_object)
    db.session.commit()
    return redirect(request.referrer + "#events")


@ events.route("/")
@ login_required
def send_events_page():
    """
    Route for the events page (the table with all the events)
    """
    # all_events = Event.query.all()
    # cows = Cow.query.all()
    return render_inertia("Events")


@ events.route("/page/<page>")
@ login_required()
@ request_data_parser
def list_events(page: int, field: str, direction: str, include_archived: bool):
    """
    Api for paginated event records from the database.
    field - What to sort by
    direction - asc/desc
    include_archived - "true"/"false"
    """
    if direction not in ('asc', 'desc'):
        abort(400)

    # ChatGPT generated a lot of this...

    # Set the number of items per page
    per_page = 25

    # Calculate the offset based on the page number
    offset = page * per_page
    if field == "date":
        query = db.session.query(Event).order_by(
            getattr(Event.date, direction)())
    elif field == "name":
        query = db.session.query(Event).order_by(
            getattr(Event.name, direction)())
    elif field == "price":
        query = (
            Event.query.outerjoin(Transaction)
            .group_by(Event)
            .order_by(getattr(func.sum(Transaction.price), direction)())
        )
    elif field == "description":
        query = db.session.query(Event).order_by(
            getattr(Event.description, direction)()
        )

    if not include_archived:
        # Pylint lies! This MUST be Cow.isArchive == false
        query = query.filter(Event.isArchived == False)

    event_list = query.offset(offset).limit(per_page).all()
    desired_fields = [
        "event_id",
        "isArchived",
        "date",
        "name",
        "description",
        {"cows": ["tag_number"]},
        {"transactions": ["name", "price"]},
    ]
    serializable_events = list(
        map(lambda event: event.serialize(desired_fields), event_list)
    )

    return json.dumps(serializable_events)


@ events.route("/event/<event_id>")
@ login_required
def show_event(event_id):
    """
    The individual event information page
    """
    event = Event.query.filter_by(event_id=event_id).first()
    if not event:
        return redirect(request.referrer), 404
    return render_inertia(
        "Event",
        props={
            "event": event.serialize(
                [
                    "event_id",
                    "name",
                    "date",
                    "isArchived",
                    "description",
                    {
                        "cows": [
                            "tag_number",
                            "birthdate",
                            "owner",
                            "sex",
                            {"sire": ["tag_number"]},
                            {"dam": ["tag_number"]},
                            {"calves": ["tag_number"]},
                            "description",
                        ]
                    },
                    {
                        "transactions": [
                            "transaction_id",
                            "name",
                            "tofrom",
                            "description",
                            "price",
                        ]
                    },
                ]
            )
        },
    )


@events.route("/exists/<date>")
@login_required
def check_if_date_exists(date):
    """
    Check whether there's already an event on a given date.
    There can be multiple events on one date, but we don't
    want seven "Branded"s on the same day because the user
    didn't check.
    """
    all_events = Event.query.filter_by(date=date).all()
    if all_events:
        return f"Found {len(all_events)} events on {date}: " + ", ".join(
            event.name for event in all_events
        )
    return "No events on this date"


@events.route("/archive")
@login_required
@request_data_parser
def event_archive(event_id: int, no_redirect: bool = False):
    """
    Submission to archive an event
    """
    event = Event.query.filter_by(event_id=event_id).first()
    if not event:
        abort(404)
    event.isArchived = True
    db.session.commit()
    if no_redirect:
        return ""
    return redirect("/events")


@events.route("/unarchive")
@login_required
@request_data_parser
def event_unarchive(event_id: int, no_redirect: bool = False):
    """
    Submission to unarchive an event
    """
    event_id = request.args.get("event_id")
    event = Event.query.filter_by(event_id=event_id).first()
    if not event:
        abort(404)
    event.isArchived = False
    db.session.commit()
    if no_redirect:
        return ""
    return redirect(request.referrer)


@events.route("/update_cows", methods=["POST"])
@login_required
@request_data_parser
def event_add_remove_cows(event_id: int, new_cow: str = None, all_cows: list = []):
    """
    This function is used to update the event's cows.

    Input:
        {
            "new_cow": String (tag number)
                - A single new cow to add
            OR
            "all_cows": [
                String (tag number)
            ]
                - A list of every cow in the event

            "event_id": Int
        }

    Output:
        Wherever the user came from
    """
    if "" in all_cows:
        all_cows.remove("")

    event = Event.query.filter_by(event_id=event_id).first()

    if not event:
        abort(404)

    if all_cows != []:
        # If it's a list of cows, overwrite the old list with the new one
        logging.debug("Overwriting existing event cows")
        event.cows = [get_cow_from_tag(cow) for cow in all_cows]
    elif new_cow is not None:
        # Otherwise, just add the new cow to the list.
        logging.debug("Adding a single new cow")
        event.cows.append(get_cow_from_tag(new_cow))

    db.session.commit()
    return redirect(request.referrer)


@events.route("/change_date", methods=["POST"])
@login_required
@request_data_parser
def event_change_date(event_id: int, date: str):
    """
    Submission route for changing the date of an event
    """

    event = Event.query.filter_by(event_id=event_id).first()
    if not event:
        abort(404)
    event.date = date
    db.session.commit()
    return redirect(request.referrer)


@events.route("/change_description", methods=["POST"])
@login_required
@request_data_parser
def event_change_description(event_id: str, description: str):
    """
    Submission route for changing the description of an event.
    """
    event = Event.query.filter_by(event_id=event_id).first()
    if not event:
        abort(404)
    event.description = description
    db.session.commit()
    return redirect(request.referrer)


@events.route("/change_name", methods=["POST"])
@login_required
@request_data_parser
def event_change_name(event_id: int, name: str):
    """
    Submission route for changing the name of an event.
    """

    event = Event.query.filter_by(event_id=event_id).first()
    if not event:
        abort(404)
    event.name = name
    db.session.commit()
    return redirect(request.referrer)


@events.route("/delete", methods=["POST"])
@login_required
@request_data_parser
def delete_event(event_id: int, no_redirect: bool = False):
    """
    Route to delete an event permanently
    """
    event = Event.query.filter_by(event_id=event_id).first()
    if not event:
        abort(404)
    try:
        db.session.delete(event)
        db.session.commit()
    # This error is thrown when an event has attached transactions.
    except IntegrityError:
        logger.warning("Attempted to delete event with attached transactions")
        db.session.rollback()
        # Make a list of transactions preventing deletion, and display it to the user in a modal.
        # TODO: Fix XSS
        flash(
            Markup(
                "Please delete all transactions from this event first."
                " The following transactions are preventing event deletion: <ul>"
                + "".join(
                    "<li>" + transaction.name + "</li>"
                    for transaction in event.transactions
                )
                + "</ul>"
            )
        )

        if no_redirect:
            return ""
        return redirect(request.referrer)
    if no_redirect:
        return ""
    return redirect("/events")
