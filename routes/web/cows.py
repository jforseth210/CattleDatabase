"""
This module is responsible for all routes relating to cows on the web interface.
"""
import json
import logging
import pprint
from datetime import datetime, timedelta

from flask import render_template, Blueprint, request, redirect, flash, abort
from flask_inertia import render_inertia
from flask_simplelogin import login_required

from helper.api_helper_functions import request_data_parser
from helper.sexes import get_sexes
from models.cow import Cow, get_cow_from_tag
from models.database import db
from models.event import Event
from models.transaction import Transaction

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(width=41, compact=True)
cows = Blueprint("cows", __name__, template_folder="templates")


@cows.route("/add", methods=["POST"])
@login_required
@request_data_parser
def new_cow(tag_number: str, owner: str, sex: str, description: str, born_event_enabled: bool = False, calved_event_enabled: bool = False, date: str = "", dam: Cow = None, sire: Cow = None):
    """
    Submission route for new cows and new calves.
    """
    if tag_number == "Not Applicable":
        logger.warning(
            "Attempted creating cow with forbidden tag number \"Not Applicable\"")
        flash('The tag number "Not Applicable" isn\'t available')
        return redirect(request.referrer), 400

    new_cow_object = Cow(
        dam_id=dam.cow_id if dam else None,
        sire_id=sire.cow_id if sire else None,
        owner=owner,
        sex=sex,
        tag_number=tag_number,
        description=description,
    )

    # Creates a "Born" event for the calf, if requested
    if born_event_enabled:
        logger.debug("Creating born event")
        born_event = Event(
            date=date,
            name="Born",
            description=f"{dam.tag_number} gave birth to {tag_number}",
            cows=[new_cow_object],
        )
        db.session.add(born_event)

    # Creates a "Calved" event for the cow, if requested
    if calved_event_enabled:
        logger.debug("Creating calved event")
        calved_event = Event(
            date=date,
            name="Calved",
            description=f"{dam.tag_number} gave birth to {tag_number}",
            cows=[dam],
        )
        db.session.add(calved_event)

    db.session.add(new_cow_object)
    db.session.commit()
    return redirect(request.referrer)


@cows.route("/add_calving_record", methods=["GET", "POST"])
@login_required
def bulk_new():
    """
    Method for adding a batch of new calves

    Input:
        {
            "owner": String
            "create_nonexistant_cows": "on", "off", or None
            "cow-n": String (tag number)
            "calf-n": String (tag number)
            "sex-n": String
            "dob-n": String (ISO date)
            "description-n": String
        }

    Output:
        {
            "operation": String
            "result": String
        }
    """
    if request.method == "GET":
        return render_template("new_calving_record.html")

    records = request.form.to_dict()
    if records.get("create_nonexistant_cows", None) is not None:
        create_non_existant_cows = records.pop(
            "create_nonexistant_cows") == "on"
    else:
        create_non_existant_cows = False

    records = assemble_record_dict(records)

    for record in records:
        # Check if it's a blank row
        if record != {}:
            create_record(record, create_non_existant_cows)
    db.session.commit()
    return redirect("/cows")


def assemble_record_dict(form_data):
    records = []
    # Take request.form.get("thing-n") -> value and convert to records[{"thing":value}]
    for key, value in form_data.items():
        # Special (not thing-n format) information
        if key != "owner" and key != "create_nonexistant_cows" and value:
            field, index = key.split("-")
            index = int(index)
            if len(records) >= index:
                records.append({})
            records[index][field] = value
    print(records)
    return records


def create_record(record, create_non_existant_cows):
    cow = get_cow_from_tag(record["cow"])
    owner = record["owner"]
    if record.get("sire", None):
        sire = get_cow_from_tag(record["sire"])

    # Create the cow if asked to, otherwise panic.
    if cow is None:
        if not create_non_existant_cows:
            logger.warning(
                "Non-existent cow entered, and create_nonexistant_cows not enabled. Aborting")
            abort(400)
        logger.debug(f"Creating cow: {record['cow']}")
        cow = Cow(sex="Cow", tag_number=record["cow"], owner=owner)
        db.session.add(cow)

    if record.get("sire", None) and sire is None:
        if not create_non_existant_cows:
            logger.warning(
                "Non-existent cow entered, and create_nonexistant_cows not enabled. Aborting")
            abort(400)
        logger.debug(f"Creating sire: {record['sire']}")
        sire = Cow(sex="Bull", tag_number=record["sire"], owner=owner)
        db.session.add(sire)
    # Parse sexes
    if record["sex"].lower() in ["s", "steer"]:
        sex = "Steer"
    elif record["sex"].lower() in ["b", "bull"]:
        sex = "Bull"
    elif record["sex"].lower() in ["h", "heifer"]:
        sex = "Heifer"
    elif record["sex"] in get_sexes("both"):
        logger.debug("Using provided sex")
        sex = record["sex"]
    else:
        logger.warning("Invalid sex recieved")
        abort(400)

    # No calf was provided. (Input validation *should* catch this first)
    if not record.get("calf", None):
        logger.warning("No calf provided! Creating event anyway")
        calved_event = Event(
            date=record["dob"],
            name="Calved",
            description=f"{record['cow']} gave birth to <CALF NOT ENTERED>\n\n"
            f"{record.get('description', '')}",
            cows=[cow],
        )
        db.session.add(calved_event)
        return
    logger.debug("Creating calf")
    logger.debug("Cow ID: %s", cow.cow_id)
    db.session.commit()
    calf = Cow(
        dam_id=cow.cow_id,
        sire_id=sire.cow_id if record.get("sire", None) else None,
        sex=sex,
        tag_number=record["calf"],
        owner=owner,
        description=record.get("description", ""),
    )
    logger.debug("Creating born event")
    born_event = Event(
        date=record["dob"],
        name="Born",
        description=f"{record['cow']} gave birth to {record['calf']}\n\n"
        f"{record.get('description', '')}",
        cows=[calf],
    )
    logger.debug("Creating calved event")
    calved_event = Event(
        date=record["dob"],
        name="Calved",
        description=f"{record['cow']} gave birth to {record['calf']}\n\n"
        f"{record.get('description', '')}",
        cows=[cow],
    )
    db.session.add(calf)
    db.session.add(born_event)
    db.session.add(calved_event)
    db.session.commit()


@cows.route("/")
@login_required
def send_cows_page():
    """
    Route for the cows page (the table with all the cows)
    """
    return render_inertia(component_name="Cows")


@cows.route("/page/<page>")
@login_required
@request_data_parser
def list_cows(page: int, list_type: str, field: str, direction: str, include_archived: bool):
    """
    Api for paginated cow records from the database.
    list_type - "ALL"/"CURRENT_COWS"
    field - What to sort by
    direction - asc/desc
    include_archived - "true"/"false"
    """
    if direction not in ('asc', 'desc'):
        abort(400)
    page = int(page)

    # Set the number of items per page
    per_page = 25

    # Calculate the offset based on the page number
    offset = page * per_page

    # Assume the default sort is by tag number
    query = db.session.query(Cow).order_by(
        getattr(Cow.tag_number, direction)())
    if field == "birthdate":
        born_event = db.session.query(Event).filter_by(name='Born').subquery()

        if direction == "asc":
            order = born_event.c.date.nullsfirst()
        elif direction == "desc":
            order = born_event.c.date.desc().nullslast()

        query = db.session.query(Cow).outerjoin(
            born_event, Cow.events).order_by(order)
        # if direction == "desc":
        #    birthdate_case = case([(Event.name == "Born", 1)], else_=0)
        # else:
        #    birthdate_case = case([(Event.name == "Born", 0)], else_=1)

        #    .outerjoin(calendar, calendar.c.cow_id == Cow.cow_id) \
        #    .outerjoin(Event, calendar.c.event_id == Event.event_id).order_by(birthdate_case.desc(),
        #                                                                      getattr(Event.date, direction)())
    elif field == "sex":
        query = db.session.query(Cow) \
            .order_by(getattr(Cow.sex, direction)())
    elif field == "owner":
        query = db.session.query(Cow) \
            .order_by(getattr(Cow.owner, direction)())
    elif field == "description":
        query = db.session.query(Cow) \
            .order_by(getattr(Cow.description, direction)())
    if list_type == "CURRENT_COWS":
        cutoff_date = datetime.now() - timedelta(days=10*30)
        print(cutoff_date)
        calved_event = db.session.query(
            Event).filter(Event.name == 'Calved').filter(Event.date > cutoff_date).subquery()
        query = query.join(calved_event, Cow.events)
    if not include_archived:
        # Pylint lies! This MUST be Cow.isArchived == false
        query = query.filter(Cow.isArchived == False)
    cow_list = query.offset(offset).limit(per_page).all()
    # if field == "birthdate":
    #    cow_list = list(map(lambda entry: entry[0], cow_list))
    desired_fields = [
        "tag_number",
        "sex",
        "isArchived",
        "birthdate",
        "owner",
        "description",
        {"dam": ["tag_number"]},
        {"sire": ["tag_number"]},
        {"calves": ["tag_number"]},
    ]
    serializable_cows = list(
        map(lambda cow: cow.serialize(desired_fields), cow_list))
    return json.dumps(serializable_cows)


@cows.route("/cow/<tag_number>")
@login_required
def show_cow(tag_number):
    """
    The individual cow information page
    """
    # This is an ugly hack that can be used to access
    # cows with a slash in their tag number
    tag_number = tag_number.replace("SLASH", "/")

    cow = get_cow_from_tag(tag_number)
    if not cow:
        return redirect(request.referrer), 404

    return render_inertia(
        "Cow",
        props=dict(
            cow=cow.serialize(
                [
                    "cow_id",
                    "tag_number",
                    "owner",
                    "birthdate",
                    "deathdate",
                    "causeOfDeath",
                    "sex",
                    "isArchived",
                    "description",
                    {"dam": ["tag_number"]},
                    {"sire": ["tag_number"]},
                    {
                        "calves": [
                            "tag_number",
                            "birthdate",
                            "owner",
                            "sex",
                            "description",
                            {"dam": ["tag_number"]},
                            {"sire": ["tag_number"]},
                            {"calves": ["tag_number"]},
                        ]
                    },
                    {
                        "events": [
                            "event_id",
                            "date",
                            "name",
                            "description",
                            {"cows": ["tag_number"]},
                        ]
                    },
                    {
                        "transactions": [
                            "transaction_id",
                            "tofrom",
                            "name",
                            "description",
                            "price",
                            {"event": ["date"]},
                        ]
                    },
                ]
            ),
        ),
    )


@cows.route('/tree/<tag_number>', methods=['GET'])
def get_family_tree(tag_number):
    cow = get_cow_from_tag(tag_number)

    def build_family_tree(cow,  nodes=None, edges=None, is_ancestor=False, is_descendant=False):
        if nodes is None:
            nodes = []
        if edges is None:
            edges = []

        # Add current cow to nodes
        nodes.append(cow.tag_number)
        if is_ancestor:
            # Add edges for get_dam() and get_sire()
            if cow.get_dam():
                edges.append(
                    {"to": cow.tag_number, "from": cow.get_dam().tag_number})
                build_family_tree(cow.get_dam(), nodes,
                                  edges, is_ancestor=True)
            if cow.get_sire():
                edges.append(
                    {"to": cow.tag_number, "from": cow.get_sire().tag_number})
                build_family_tree(cow.get_sire(), nodes,
                                  edges, is_ancestor=True)
        if is_descendant:
            # Add edges for get_calves()
            for child in cow.get_calves():
                edges.append({"from": cow.tag_number, "to": child.tag_number})
                build_family_tree(child, nodes, edges, is_descendant=True)

        return nodes, edges

    nodes, edges = build_family_tree(cow, is_ancestor=True, is_descendant=True)
    return json.dumps({'nodes': nodes, 'edges': edges})


@cows.route("/exists/<tag_number>")
@login_required
def cow_exists(tag_number):
    """
    Check if a given tag number is already taken
    """
    cow = get_cow_from_tag(tag_number)

    if cow:
        return "True"
    return "False", 404


@cows.route("/change_tag", methods=["POST"])
@login_required
@request_data_parser
def change_tag_number(old_tag, new_tag):
    """
    Submission route to change the tag number of an existing cow
    """
    cow = get_cow_from_tag(old_tag)
    if not cow:
        logger.debug(f"Cow {old_tag} does not exist")
        abort(404)
    cow.tag_number = new_tag
    db.session.commit()
    return redirect("/cows/cow/" + new_tag)


@cows.route("/change_sex", methods=["POST"])
@login_required
@request_data_parser
def change_sex(tag_number, sex):
    """
    Submission route to change the sex of an existing cow.
    """
    if sex not in get_sexes("both"):
        abort(400)
    cow = get_cow_from_tag(tag_number)
    if not cow:
        logger.debug(f"Cow {tag_number} does not exist")
        abort(404)
    cow.sex = sex
    db.session.commit()
    return redirect("/cows/cow/" + tag_number)


@cows.route("/set_birthdate", methods=["POST"])
@login_required
@request_data_parser
def set_birthdate(tag_number, birthdate):
    """
    Submission route to set the birthdate of a cow, regardless of whether a born event exists
    """
    cow = get_cow_from_tag(tag_number)
    if not cow:
        logger.debug(f"Cow {tag_number} does not exist")
        abort(404)
    for event in cow.events:
        if event.name == "Born":
            logger.debug("Updating birthdate")
            event.date = birthdate
            db.session.commit()
            return redirect("/cows/cow/" + tag_number)
    logger.debug("Creating new born event")
    born_event = Event(
        date=birthdate, name="Born", description=f"{tag_number} was born", cows=[cow]
    )
    db.session.add(born_event)
    db.session.commit()
    return redirect("/cows/cow/" + tag_number)


@cows.route("/add_parent", methods=["POST"])
@login_required
@request_data_parser
def cow_add_parent(tag_number, parent_type, parent_tag):
    """
    Submission route for adding a parent to a cow that doesn't have one
    """

    cow = get_cow_from_tag(tag_number)

    if parent_type == "Dam":
        logger.debug("Setting dam")
        cow.dam_id = get_cow_from_tag(parent_tag).cow_id
    elif parent_type == "Sire":
        logger.debug("Setting sire")
        cow.sire_id = get_cow_from_tag(parent_tag).cow_id
    else:
        logger.warning("Invalid parent type")
    db.session.commit()
    return redirect(request.referrer)


@cows.route("/change_description", methods=["POST"])
@login_required
@request_data_parser
def cow_change_description(tag_number, description):
    """
    Submission route for updating the description of a cow
    """
    cow = get_cow_from_tag(tag_number)
    if not cow:
        logger.debug(f"Cow {tag_number} does not exist")
        abort(404)
    cow.description = description
    db.session.commit()
    return redirect(request.referrer)


@cows.route("/archive")
@login_required
@request_data_parser
def cow_archive(tag_number, no_redirect=False):
    """
    Submission to archive a cow
    """
    cow = get_cow_from_tag(tag_number)
    if not cow:
        logger.debug(f"Cow {tag_number} does not exist")
        abort(404)
    cow.isArchived = True
    db.session.commit()
    if no_redirect:
        return ""
    return redirect("/cows")


@cows.route("/unarchive")
@login_required
@request_data_parser
def cow_unarchive(tag_number, no_redirect=False):
    """
    Submission to unarchive a cow
    """
    cow = get_cow_from_tag(tag_number)
    if not cow:
        logger.debug(f"Cow {tag_number} does not exist")
        abort(404)
    cow.isArchived = False
    db.session.commit()
    if no_redirect:
        return ""
    return redirect(request.referrer)


@cows.route("/transfer_ownership", methods=["POST"])
@request_data_parser
@login_required
def transfer_ownership(tag_number: list, new_owner: str, no_redirect: bool = False, transfer_event_enabled: bool = False, archive_cow: bool = False, date="", price: float = 0, description=""):
    """
    Submission to transfer ownership of a cow
    """
    transferred_cows = []
    # TODO: Can't rename arg without changing it in the http request
    tag_numbers = tag_number
    for tag_number in tag_numbers:
        cow = get_cow_from_tag(tag_number)
        if not cow:
            logger.debug(f"Cow {tag_number} does not exist")
            abort(404)
        # Change the cow's owner
        cow.owner = new_owner

        if archive_cow:
            cow.isArchived = True
        transferred_cows.append(cow)
    if transfer_event_enabled:
        # Create a "Sold" transaction
        logger.debug("Creating sold transaction")
        sale_transaction = Transaction(
            name="Sold",
            description=f"{cow.owner} sold {', '.join(tag_numbers)}: {
                description}",
            price=price,
            tofrom=new_owner,
            cows=transferred_cows,
        )

        # Create a "Transfer" event and attach the transaction
        logger.debug("Creating transfer event")
        sale_event = Event(
            date=date,
            name="Transfer",
            description=f"Transfer {', '.join(tag_numbers)} from {
                cow.owner} to {new_owner}:\n"
            f"{description}",
            cows=transferred_cows,
            transactions=[sale_transaction],
        )
        db.session.add(sale_event)
    db.session.commit()
    if no_redirect:
        return ""
    return redirect(request.referrer)


@cows.route("/delete", methods=["POST"])
@request_data_parser
@login_required
def delete_cow(tag_number, no_redirect=False):
    """
    Route to delete a cow permanently
    """
    tag_number = request.form.get("tag_number")
    no_redirect = request.form.get("no_redirect")
    cow = get_cow_from_tag(tag_number)
    if not cow:
        logger.debug(f"Cow {tag_number} does not exist")
        abort(404)
    db.session.delete(cow)
    db.session.commit()

    if no_redirect:
        return ""
    return redirect("/cows")
