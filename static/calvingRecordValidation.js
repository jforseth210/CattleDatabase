function rowIsEmpty(row) {
    let inputsInRow = row.getElementsByTagName("input")
    for (let i = 0; i < inputsInRow.length; i++) {
        const siblingInput = inputsInRow[i];
        if (siblingInput.value != "") {
            return false;
        }
    }
    return true;
}
function setError(input, error) {
    input.parentElement.querySelectorAll(".invalid-feedback")[0].innerHTML = error;
    input.setCustomValidity(error);
}
function setSuccess(input, success) {
    input.parentElement.querySelectorAll(".valid-feedback")[0].innerHTML = success;
    input.setCustomValidity("");
}
function autofillCalf(calf) {
    if (calf.value != "")
        return
    let calves = Array.from(document.getElementsByClassName("calf"));
    try {
        let prevCalf = calves[calves.indexOf(calf) - 1]
        if (Number(prevCalf.value) != NaN){
            calf.value = Number(prevCalf.value) + 1;
        }
    } catch (TypeError) {
        // Can't autofill the first .
    }
    calf.classList.add("autofilled")

}
function clearCalf(calf) {
    if (calf.classList.contains("autofilled")) {
        calf.value = "";
        calf.classList.remove("autofilled");
    }
}
function autofillOwner(owner) {
    if (owner.value != "")
        return
    //let owner = Array.from(document.getElementsByClassName("owner"));
    owner.value = document.getElementById("masterOwner").value
    owner.classList.add("autofilled")
}
function clearOwner(owner) {
    if (owner.classList.contains("autofilled")) {
        owner.value = "";
        owner.classList.remove("autofilled");
    }
}
form = document.getElementById("inputForm");
form.addEventListener('submit', function(event) {
    if (!form.checkValidity()) {
        event.preventDefault()
        event.stopPropagation()
    }
    form.classList.add('was-validated')
}, false)
function revalidateRows() {
    cows = document.getElementsByClassName("cow");
    for (let i = 0; i < cows.length; i++) {
        const cow = cows[i];
        validateRow(cow);
    }
}
function validateRow(input) {
    let row = input.parentElement.parentElement;
    let inputs = row.getElementsByTagName("input");
    if (rowIsEmpty(row))
        return
    for (let i = 0; i < inputs.length; i++) {
        const input = inputs[i];
        if (input.classList.contains("cow")) {
            validateCow(input);
            autofillCalf(row.getElementsByClassName("calf")[0]);
            autofillOwner(row.getElementsByClassName("owner")[0]);
        }
        if (input.classList.contains("sire")) {
            validateSire(input);
        }
        if (input.classList.contains("calf")) {
            validateCalf(input);
        }
        if (input.classList.contains("date")) {
            validateDate(input);
        }
        if (input.classList.contains("sex")) {
            validateSex(input);
        }


    }
    row.classList.add("was-validated")
}
function validateCow(input) {
    if (input.value == "") {
        setError(input, "Please specify a cow");
        return;
    }
    const Http = new XMLHttpRequest();
    const url = "/cows/exists/" + input.value;
    Http.open("GET", url);
    Http.send();
    Http.onreadystatechange = (e) => {
        if (Http.responseText == "True") {
            setSuccess(input, "Cow exists");
        } else if (Http.responseText == "False") {
            if (document.getElementById("create_nonexistant_cows").checked) {
                setSuccess(input, "Cow will be created");
            } else {
                setError(input, "Cow does not exist");
            }
        }
    };
}
function validateSire(input) {
    const rowIndex = Number(input.name.split("-")[1]);
    const Http = new XMLHttpRequest();
    const url = "/cows/exists/" + input.value;
    Http.open("GET", url);
    Http.send();
    Http.onreadystatechange = (e) => {
        if (Http.responseText == "True") {
            document.getElementById(`sireToggle-${rowIndex}`).classList.add("btn-success") 
            document.getElementById(`sireToggle-${rowIndex}`).classList.remove("btn-danger") 
            setSuccess(input, "Bull exists");
        } else if (Http.responseText == "False") {
            if (document.getElementById("create_nonexistant_cows").checked) {
                document.getElementById(`sireToggle-${rowIndex}`).classList.add("btn-success") 
                document.getElementById(`sireToggle-${rowIndex}`).classList.remove("btn-danger") 
                setSuccess(input, "Bull will be created");
            } else {
                console.log(document.getElementById(`sireToggle-${rowIndex}`))
                document.getElementById(`sireToggle-${rowIndex}`).classList.remove("btn-success") 
                document.getElementById(`sireToggle-${rowIndex}`).classList.add("btn-danger") 
                setError(input, "Bull does not exist");
            }
        }
    };
}
function validateCalf(input) {
    if (input.value == "") {
        setError(input, "Please specify a calf");
        return;
    }
    const Http = new XMLHttpRequest();
    const url = "/cows/exists/" + input.value;
    Http.open("GET", url);
    Http.send();
    Http.onreadystatechange = (e) => {
        if (Http.responseText == "True") {
            setError(input, "Calf exists already");
        } else if (Http.responseText == "False") {
            setSuccess(input, "Calf will be added");
        }
    };
}
function validateDate(input) {
    if (input.value == "") {
        setError(input, "Please specify a date of birth");
        return;
    }
    if (new Date().getTime() <= new Date(input.value).getTime()) {
        setError(input, "Future dates are not allowed");
        return;
    }
    setSuccess(input, "Looks good!");

}
function validateSex(input) {
    if (input.value == "") {
        setError(input, "Please specify a sex");
        return;
    }
    if (["h", "heifer", "s", "steer", "b", "bull"].indexOf(input.value.toLowerCase()) == -1) {
        setError(input, "Please enter h, s, or b");
        return;
    }
    setSuccess(input, "Looks good!");
}
function validateOwner(input) {
    if (input.value == "") {
        setError(input, "Please specify an owner");
        return;
    }
    setSuccess(input, "Looks good!");

}
