import sys
import json
sys.path.append('/var/www/my_cows')

activate_this = '/var/www/my_cows/venv/bin/activate_this.py'
#execfile(activate_this, dict(__file__=activate_this))
exec(open(activate_this).read(), dict(__file__=activate_this))
from app import create_app

# Create a secret key if none exists
try:
    with open("sensitive_data.json", "r", encoding="utf-8") as f:
        SECRET_KEY = json.loads(f.read())["SECRET_KEY"]

except FileNotFoundError:
    SECRET_KEY = str(os.urandom(64))
    with open("sensitive_data.json", "w", encoding="utf-8") as f:
        f.write(json.dumps({
            "SECRET_KEY": SECRET_KEY
        }))

application = create_app()

